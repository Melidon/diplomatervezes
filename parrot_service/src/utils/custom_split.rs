use {
    crate::{
        error::Error,
        message_models::{Message, PartialText},
    },
    axum::extract::ws::WebSocket,
    futures_util::{SinkExt, StreamExt, TryStreamExt},
};

pub trait CustomSplit {
    fn custom_split<M: Into<Message>>(
        self,
    ) -> (impl Sender<M, Error>, impl Receiver<PartialText, Error>);
}

impl CustomSplit for WebSocket {
    fn custom_split<M: Into<Message>>(
        self,
    ) -> (impl Sender<M, Error>, impl Receiver<PartialText, Error>) {
        let (sender, receiver) = self.map_err(Error::from).split();
        let sender = sender
            .with(|message: M| Box::pin(async { message.into().try_into().map_err(Error::Serde) }));
        let receiver = receiver.map(|message| PartialText::try_from(message?));
        (sender, receiver)
    }
}

pub trait Sender<T, E>: SinkExt<T, Error = E> + Unpin {}
impl<S, T, E> Sender<T, E> for S where S: SinkExt<T, Error = E> + Unpin {}
pub trait Receiver<T, E>: StreamExt<Item = Result<T, E>> + Unpin {}
impl<R, T, E> Receiver<T, E> for R where R: StreamExt<Item = Result<T, E>> + Unpin {}
