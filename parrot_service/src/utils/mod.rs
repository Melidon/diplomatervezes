pub mod amqp;
pub mod custom_split;

use std::time::{SystemTime, UNIX_EPOCH};

pub fn milliseconds_since_epoch() -> u128 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_millis()
}
