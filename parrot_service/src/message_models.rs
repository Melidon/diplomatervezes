use {
    crate::{error::Error, utils::milliseconds_since_epoch},
    axum::extract::ws,
    serde::{Deserialize, Serialize},
    uuid::Uuid,
};

#[derive(Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PartialText {
    pub text: String,
}

impl TryFrom<ws::Message> for PartialText {
    type Error = Error;

    fn try_from(message: ws::Message) -> Result<Self, Self::Error> {
        let message = message.to_text()?;
        let message = serde_json::from_str(message)?;
        Ok(message)
    }
}

#[derive(Serialize, Clone)]
#[serde(rename_all = "camelCase", tag = "type")]
pub enum Message {
    Text(TextMessage),
}

#[derive(Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct TextMessage {
    pub author: User,
    pub created_at: u128, // milliseconds since epoch
    pub id: Uuid,
    pub text: String,
}

#[derive(Serialize, Clone, Copy)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub id: Uuid,
}

impl TextMessage {
    pub fn new(author: User, text: String) -> Self {
        Self {
            author,
            created_at: milliseconds_since_epoch(),
            id: Uuid::new_v4(),
            text,
        }
    }
}

impl From<TextMessage> for Message {
    fn from(text_message: TextMessage) -> Self {
        Message::Text(text_message)
    }
}

impl TryFrom<Message> for ws::Message {
    type Error = serde_json::Error;

    fn try_from(message: Message) -> Result<Self, Self::Error> {
        let message = serde_json::to_string(&message)?;
        let message = ws::Message::from(message);
        Ok(message)
    }
}
