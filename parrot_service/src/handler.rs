use {
    crate::{
        error::Error,
        message_models::{PartialText, TextMessage, User},
        utils::custom_split::{CustomSplit, Receiver, Sender},
    },
    axum::{
        extract::{Query, WebSocketUpgrade},
        response::Response,
    },
    serde::Deserialize,
    tokio::time::{sleep, Duration},
    tracing::{debug, error, info},
    uuid::Uuid,
};

#[derive(Deserialize)]
pub struct QueryParams {
    user_id: Uuid,
}

pub async fn handler(
    ws: WebSocketUpgrade,
    Query(QueryParams { user_id }): Query<QueryParams>,
) -> Response {
    ws.on_upgrade(move |socket| async move {
        info!("User connected: {}", user_id);
        let (sender, receiver) = socket.custom_split();
        let result = handle_socket(sender, receiver, user_id).await;
        match result {
            Ok(_) => info!("Socket closed"),
            Err(e) => error!("Socket error: {:?}", e),
        }
    })
}

async fn handle_socket(
    mut sender: impl Sender<TextMessage, Error>,
    mut receiver: impl Receiver<PartialText, Error>,
    user_id: Uuid,
) -> Result<(), Error> {
    let user = User { id: user_id };
    let parrot = User { id: Uuid::new_v4() };
    while let Some(message) = receiver.next().await.transpose()? {
        debug!("Received message: {}", message.text);
        let user_message = TextMessage::new(user, message.text.clone());
        sender.send(user_message).await?;
        sleep(Duration::from_secs(1)).await;
        let parrot_message = TextMessage::new(parrot, message.text);
        sender.send(parrot_message).await?;
    }
    Ok(())
}
