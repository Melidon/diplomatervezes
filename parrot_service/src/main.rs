use {
    amqprs::{
        callbacks::{DefaultChannelCallback, DefaultConnectionCallback},
        channel::Channel,
        connection::Connection,
    },
    parrot_service::{heartbeat_consumer::HeartbeatConsumer, utils::amqp::Listener},
    std::{
        env,
        net::{Ipv4Addr, SocketAddrV4},
    },
    tokio::net::TcpListener,
};

async fn connect_to_message_queue() -> (Connection, Channel, Channel) {
    let rabbitmq_uri = env::var("RABBITMQ_URI").expect("Failed to fetch RABBITMQ_URI");
    let connection_arguments = rabbitmq_uri
        .as_str()
        .try_into()
        .expect("RABBITMQ_URI is invalid!");

    let connection = Connection::open(&connection_arguments)
        .await
        .expect("Failed to connect to RabbitMQ!");
    connection
        .register_callback(DefaultConnectionCallback)
        .await
        .expect("Failed to register connection callback!");

    let consume_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    consume_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");

    let publish_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    publish_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");

    (connection, consume_channel, publish_channel)
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();
    // DO NOT RENAME `connection` TO `_connection`!!!
    // It will cause the connection to be dropped.
    // Thus it will be closed and channels won't work.
    #[allow(unused_variables)]
    let (connection, consume_channel, publish_channel) = connect_to_message_queue().await;
    consume_channel
        .on(
            "chat_heartbeat_request",
            HeartbeatConsumer::new(publish_channel).await,
        )
        .await
        .expect("Failed to register consumer!");
    let port = env::var("PORT").map_or(3000, |port| port.parse().expect("PORT is invalid"));
    let listener = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port))
        .await
        .expect("Could not bind TCP listener");
    let app = parrot_service::get_app();
    axum::serve(listener, app).await.expect("Server failed");
}
