#[derive(Debug)]
pub enum Error {
    Axum(axum::Error),
    Serde(serde_json::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Axum(error) => write!(f, "Axum error: {}", error),
            Error::Serde(error) => write!(f, "Serde error: {}", error),
        }
    }
}

impl std::error::Error for Error {}

impl From<axum::Error> for Error {
    fn from(e: axum::Error) -> Self {
        Error::Axum(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::Serde(e)
    }
}
