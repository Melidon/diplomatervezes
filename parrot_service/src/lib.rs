pub mod error;
mod handler;
pub mod heartbeat_consumer;
pub mod message_models;
pub mod utils;

pub use handler::handler;

use {
    axum::{routing::get, Router},
    tower_http::cors::CorsLayer,
};

pub const PATH: &str = "/chats/parrot";

pub fn get_app() -> Router {
    Router::new()
        .route(format!("/api{PATH}").as_str(), get(handler))
        .layer(CorsLayer::very_permissive()) // TODO: Allow less if possible
}
