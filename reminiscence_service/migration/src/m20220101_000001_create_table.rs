use sea_orm_migration::{prelude::*, schema::*};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(QuestionTable::Table)
                    .if_not_exists()
                    .col(pk_uuid(QuestionTable::Id))
                    .col(uuid(QuestionTable::UserId))
                    .col(string(QuestionTable::Question))
                    .col(string(QuestionTable::Answer))
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(QuestionTable::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum QuestionTable {
    Table,
    Id,
    UserId,
    Question,
    Answer,
}
