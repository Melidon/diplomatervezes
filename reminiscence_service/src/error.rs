#[derive(Debug)]
pub enum Error {
    Axum(axum::Error),
    Database(sea_orm::DbErr),
    Serde(serde_json::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Axum(error) => write!(f, "Axum error: {}", error),
            Error::Database(error) => write!(f, "Database error: {}", error),
            Error::Serde(error) => write!(f, "Serde error: {}", error),
        }
    }
}

impl std::error::Error for Error {}

impl From<axum::Error> for Error {
    fn from(e: axum::Error) -> Self {
        Error::Axum(e)
    }
}

impl From<sea_orm::DbErr> for Error {
    fn from(e: sea_orm::DbErr) -> Self {
        Error::Database(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::Serde(e)
    }
}
