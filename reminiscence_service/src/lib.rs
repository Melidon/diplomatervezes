pub mod error;
pub mod handler;
pub mod heartbeat_consumer;
pub mod message_models;
pub mod utils;

pub const PATH: &str = "/chats/reminiscence";

use {
    axum::{routing::get, Extension, Router},
    sea_orm::DatabaseConnection,
    tower_http::cors::CorsLayer,
};

pub fn get_app(db: DatabaseConnection) -> Router {
    Router::new()
        .route(format!("/api{PATH}").as_str(), get(handler::handler))
        .layer(Extension(db))
        .layer(CorsLayer::very_permissive()) // TODO: Allow less if possible
}
