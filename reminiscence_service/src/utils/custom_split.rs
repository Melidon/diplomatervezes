use {
    crate::{
        error::Error,
        message_models::{Message, PartialText},
    },
    axum::extract::ws::WebSocket,
    futures_util::{SinkExt, StreamExt, TryStreamExt},
};

pub trait CustomSplit {
    fn custom_split(
        self,
    ) -> (
        impl Sender<Message, Error = Error>,
        impl Receiver<Item = Result<PartialText, Error>>,
    );
}

impl CustomSplit for WebSocket {
    fn custom_split(
        self,
    ) -> (
        impl Sender<Message, Error = Error>,
        impl Receiver<Item = Result<PartialText, Error>>,
    ) {
        let (sender, receiver) = self.map_err(Error::from).split();
        let sender = sender
            .with(|message: Message| Box::pin(async { message.try_into().map_err(Error::Serde) }));
        let receiver = receiver.map(|message| PartialText::try_from(message?));
        (sender, receiver)
    }
}

pub trait Sender<T>: SinkExt<T> + Unpin {
    fn send_into(&mut self, item: impl Into<T>) -> futures_util::sink::Send<'_, Self, T> {
        self.send(item.into())
    }
}
impl<S, T, E> Sender<T> for S where S: SinkExt<T, Error = E> + Unpin {}
pub trait Receiver: StreamExt + Unpin {}
impl<R, T, E> Receiver for R where R: StreamExt<Item = Result<T, E>> + Unpin {}
