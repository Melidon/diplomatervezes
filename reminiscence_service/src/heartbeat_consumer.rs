use {
    crate::PATH,
    amqprs::{
        channel::{BasicPublishArguments, Channel, ExchangeDeclareArguments, ExchangeType},
        consumer::AsyncConsumer,
        BasicProperties, Deliver,
    },
    async_trait::async_trait,
    serde_json::json,
    tracing::info,
};

pub struct HeartbeatConsumer {
    publish_channel: Channel,
}

#[async_trait]
impl AsyncConsumer for HeartbeatConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        _content: Vec<u8>,
    ) {
        info!("Received heartbeat request");
        let message = json!({
            "name": "Reminiscence",
            "path": PATH,
        });
        info!("Sending heartbeat response: {:?}", message);
        self.publish_channel
            .basic_publish(
                BasicProperties::default(),
                serde_json::to_vec(&message).expect("Failed to serialize message!"),
                BasicPublishArguments::new(EXCHANGE, ""),
            )
            .await
            .expect("Failed to publish message");
    }
}

impl HeartbeatConsumer {
    pub async fn new(publish_channel: Channel) -> Self {
        publish_channel
            .exchange_declare(ExchangeDeclareArguments::of_type(
                EXCHANGE,
                ExchangeType::Fanout,
            ))
            .await
            .expect("Failed to declare exchange");
        Self { publish_channel }
    }
}

const EXCHANGE: &str = "chat_heartbeat_response";
