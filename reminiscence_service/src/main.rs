use {
    amqprs::{
        callbacks::{DefaultChannelCallback, DefaultConnectionCallback},
        channel::Channel,
        connection::Connection,
    },
    migration::{Migrator, MigratorTrait},
    reminiscence_service::{heartbeat_consumer::HeartbeatConsumer, utils::amqp::Listener},
    sea_orm::{Database, DatabaseConnection},
    std::{
        env,
        net::{Ipv4Addr, SocketAddrV4},
    },
    tokio::net::TcpListener,
};

async fn connect_to_amqp() -> (Connection, Channel, Channel) {
    let amqp_uri = env::var("AMQP_URI").expect("AMQP_URI");
    let connection_arguments = amqp_uri
        .as_str()
        .try_into()
        .expect("Failed to parse AMQP_URI");

    let connection = Connection::open(&connection_arguments)
        .await
        .expect("Failed to connect to AMQP server");
    connection
        .register_callback(DefaultConnectionCallback)
        .await
        .expect("Failed to register connection callback");

    let consume_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel");
    consume_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback");

    let publish_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel");
    publish_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback");

    (connection, consume_channel, publish_channel)
}

async fn connect_to_database() -> DatabaseConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL");
    let db = Database::connect(&database_url)
        .await
        .expect("Failed to connect to database");
    Migrator::up(&db, None)
        .await
        .expect("Failed apply all pending migrations");
    db
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();

    // DO NOT RENAME `connection` TO `_connection`!!!
    // It will cause the connection to be dropped.
    // Thus it will be closed and channels won't work.
    #[allow(unused_variables)]
    let (connection, consume_channel, publish_channel) = connect_to_amqp().await;
    consume_channel
        .on(
            "chat_heartbeat_request",
            HeartbeatConsumer::new(publish_channel).await,
        )
        .await
        .expect("Failed to register consumer");

    let port = env::var("PORT").map_or(3000, |port| port.parse().expect("PORT is invalid"));
    let listener = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port))
        .await
        .expect("Could not bind TCP listener");
    let db = connect_to_database().await;
    let app = reminiscence_service::get_app(db);
    axum::serve(listener, app).await.expect("Server failed");
}
