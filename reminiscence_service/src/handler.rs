use {
    crate::{
        error::Error,
        message_models::{Message, PartialText, TextMessage, User},
        utils::custom_split::{CustomSplit, Receiver, Sender},
    },
    axum::{
        extract::{Query, WebSocketUpgrade},
        response::Response,
        Extension,
    },
    entity::{prelude::*, question_table},
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter},
    serde::Deserialize,
    tokio::time::{sleep, Duration},
    tracing::{debug, error, info},
    uuid::Uuid,
};

#[derive(Deserialize)]
pub struct QueryParams {
    user_id: Uuid,
}

pub async fn handler(
    Extension(db): Extension<DatabaseConnection>,
    ws: WebSocketUpgrade,
    Query(QueryParams { user_id }): Query<QueryParams>, // TODO: Get from JWT
) -> Response {
    ws.on_upgrade(move |socket| async move {
        info!("User connected: {}", user_id);
        let (sender, receiver) = socket.custom_split();
        let result = handle_socket(db, sender, receiver, user_id).await;
        match result {
            Ok(message) => info!(message),
            Err(error) => error!("Error: {}", error),
        }
    })
}

async fn handle_socket(
    db: DatabaseConnection,
    mut sender: impl Sender<Message, Error = Error>,
    mut receiver: impl Receiver<Item = Result<PartialText, Error>>,
    user_id: Uuid,
) -> Result<&'static str, Error> {
    let user = User { id: user_id };
    let server = User { id: Uuid::new_v4() };
    let questions = list_questions_for_user(&db, user_id).await?;
    for question in questions {
        // Ask question
        debug!("Sending question: {}", question.question);
        let question_message = TextMessage::new(server, question.question);
        sender.send_into(question_message).await?;

        // Wait for answer
        let Some(answer) = receiver.next().await.transpose()? else {
            return Ok("Socket closed");
        };
        debug!("Received answer: {}", answer.text);
        let user_message = TextMessage::new(user, answer.text.clone());
        sender.send_into(user_message).await?;

        // Wait for a second
        sleep(Duration::from_secs(1)).await;

        // Send response
        let response = if answer.text.trim().to_lowercase() == question.answer.trim().to_lowercase()
        {
            "Yes, that's correct".to_owned()
        } else {
            format!("No, the correct answer is: {}", question.answer)
        };
        let response_message = TextMessage::new(server, response);
        sender.send_into(response_message).await?;

        // Wait for a second
        sleep(Duration::from_secs(1)).await;
    }
    let out_of_questions_message = TextMessage::new(server, "Out of questions".to_owned());
    sender.send_into(out_of_questions_message).await?;
    Ok("Out of questions")
}

async fn list_questions_for_user(
    db: &DatabaseConnection,
    user_id: Uuid,
) -> Result<Vec<question_table::Model>, Error> {
    let questions = QuestionTable::find()
        .filter(question_table::Column::UserId.eq(user_id))
        .all(db)
        .await?;
    Ok(questions)
}
