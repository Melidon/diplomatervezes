# Steps to reproduce migration

1. Run `cargo install sea-orm-cli` (just to make sure).
2. Run `sea-orm-cli migrate init` and add the **migration** crate to the workspace.
4. Write the migration in **./migration/src/m20220101_000001_create_table.rs** based on [the documentation](https://www.sea-ql.org/SeaORM/docs/migration/writing-migration/)
5. Run `sea-orm-cli migrate up`. You need the **DATABASE_URL** (possibly in the **.env** file) to be set and to have a running PostgreSQL server.
6. Run `cargo new entity --lib` (this will add the **entity** crate to the workspace) and run `cargo add sea-orm --package entity`.
7. Run `sea-orm-cli generate entity --lib --output-dir entity/src` to generate the entities.
