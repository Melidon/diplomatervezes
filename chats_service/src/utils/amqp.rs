use {
    amqprs::{
        channel::{
            BasicConsumeArguments, Channel, ExchangeDeclareArguments, ExchangeType,
            QueueBindArguments, QueueDeclareArguments,
        },
        consumer::AsyncConsumer,
        error::Error,
    },
    std::future::Future,
};

pub trait Listener {
    fn on<Consumer>(
        &self,
        event: impl AsRef<str>,
        consumer: Consumer,
    ) -> impl Future<Output = Result<(), Error>>
    where
        Consumer: AsyncConsumer + Send + 'static;
}

impl Listener for Channel {
    async fn on<Consumer>(&self, exchange: impl AsRef<str>, consumer: Consumer) -> Result<(), Error>
    where
        Consumer: AsyncConsumer + Send + 'static,
    {
        // Declare exchange
        self.exchange_declare(ExchangeDeclareArguments::of_type(
            exchange.as_ref(),
            ExchangeType::Fanout,
        ))
        .await?;
        // Declare queue
        let (queue_name, _, _) = self
            .queue_declare(QueueDeclareArguments::exclusive_server_named())
            .await?
            .expect("`no_wait` argument should be `false`");
        // Bind queue to exchange
        self.queue_bind(QueueBindArguments::new(&queue_name, exchange.as_ref(), ""))
            .await?;
        // Start consuming
        self.basic_consume(
            consumer,
            BasicConsumeArguments::new(&queue_name, "")
                .auto_ack(true)
                .finish(),
        )
        .await?;
        Ok(())
    }
}
