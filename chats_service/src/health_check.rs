use {
    crate::models::InternalChat,
    amqprs::{
        channel::{BasicPublishArguments, Channel, ExchangeDeclareArguments, ExchangeType},
        BasicProperties,
    },
    std::sync::Arc,
    tokio::{
        sync::Mutex,
        time::{sleep, Duration, Instant},
    },
    tracing::{debug, info},
};

pub async fn health_check(chats: Arc<Mutex<Vec<InternalChat>>>, publish_channel: Channel) {
    publish_channel
        .exchange_declare(ExchangeDeclareArguments::of_type(
            EXCHANGE,
            ExchangeType::Fanout,
        ))
        .await
        .expect("Failed to declare exchange!");
    let publish_channel = Arc::new(publish_channel);
    loop {
        tokio::spawn({
            let chats = chats.clone();
            let publish_channel = publish_channel.clone();
            async move {
                let health_check_start = Instant::now();
                request_heartbeat(publish_channel).await;
                sleep(PING_TIMEOUT).await;
                remove_dead_chats(chats, health_check_start).await;
            }
        });
        sleep(PING_INTERVAL).await;
    }
}

async fn request_heartbeat(publish_channel: Arc<Channel>) {
    info!("Sending heartbeat request.");
    publish_channel
        .basic_publish(
            BasicProperties::default(),
            Vec::default(),
            BasicPublishArguments::new(EXCHANGE, ""),
        )
        .await
        .expect("Failed to publish message!");
}

async fn remove_dead_chats(chats: Arc<Mutex<Vec<InternalChat>>>, health_check_start: Instant) {
    let mut chats = chats.lock().await;
    debug!("Before removing dead chats: {:?}", chats);
    chats.retain(|chat| health_check_start < chat.registered_at);
    debug!("After removing dead chats: {:?}", chats);
}

const EXCHANGE: &str = "chat_heartbeat_request";
const PING_INTERVAL: Duration = Duration::from_secs(25);
const PING_TIMEOUT: Duration = Duration::from_secs(5);
