use {
    crate::models::{IncomingChat, InternalChat},
    amqprs::{channel::Channel, consumer::AsyncConsumer, BasicProperties, Deliver},
    axum::async_trait,
    std::sync::Arc,
    tokio::sync::Mutex,
    tracing::{error, info},
};

pub struct HeartbeatConsumer {
    chats: Arc<Mutex<Vec<InternalChat>>>,
}

#[async_trait]
impl AsyncConsumer for HeartbeatConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        content: Vec<u8>,
    ) {
        if let Err(e) = self.add_or_update_chat(&content).await {
            error!("Failed to add or update chat: {}", e);
        }
    }
}

impl HeartbeatConsumer {
    pub fn new(chats: Arc<Mutex<Vec<InternalChat>>>) -> Self {
        Self { chats }
    }

    async fn add_or_update_chat(&self, new_chat: &[u8]) -> Result<(), serde_json::error::Error> {
        let new_chat: InternalChat = serde_json::from_slice::<IncomingChat>(new_chat)?.into();
        info!("Received heartbeat response: {:?}", new_chat);
        let mut chats = self.chats.lock().await;
        for chat in chats.iter_mut() {
            if chat.name == new_chat.name {
                *chat = new_chat;
                return Ok(());
            }
        }
        chats.push(new_chat);
        Ok(())
    }
}
