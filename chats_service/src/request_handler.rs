use {
    crate::models::{InternalChat, OutgoingChat},
    axum::{Extension, Json},
    std::sync::Arc,
    tokio::sync::Mutex,
    tracing::debug,
};

pub async fn list_chats(
    Extension(chats): Extension<Arc<Mutex<Vec<InternalChat>>>>,
) -> Json<Vec<OutgoingChat>> {
    let chats = chats
        .lock()
        .await
        .iter()
        .cloned()
        .map(OutgoingChat::from)
        .collect();
    debug!("Listing chats: {:?}", chats);
    Json(chats)
}
