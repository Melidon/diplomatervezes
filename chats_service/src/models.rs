use {
    serde::{Deserialize, Serialize},
    tokio::time::Instant,
};

#[derive(Clone, Debug, Deserialize)]
pub struct IncomingChat {
    name: String,
    path: String,
}

impl From<IncomingChat> for InternalChat {
    fn from(chat: IncomingChat) -> Self {
        InternalChat {
            name: chat.name,
            path: chat.path,
            registered_at: Instant::now(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct InternalChat {
    pub name: String,
    path: String,
    pub registered_at: Instant,
}

#[derive(Clone, Debug, Serialize)]
pub struct OutgoingChat {
    name: String,
    path: String,
}

impl From<InternalChat> for OutgoingChat {
    fn from(chat: InternalChat) -> Self {
        OutgoingChat {
            name: chat.name,
            path: chat.path,
        }
    }
}
