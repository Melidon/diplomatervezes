pub mod health_check;
pub mod heartbeat_consumer;
pub mod models;
pub mod request_handler;
pub mod utils;

use {
    axum::{routing::get, Extension, Router},
    models::InternalChat,
    request_handler::list_chats,
    std::sync::Arc,
    tokio::sync::Mutex,
    tower_http::cors::CorsLayer,
};

pub fn get_app(chats: Arc<Mutex<Vec<InternalChat>>>) -> Router {
    Router::new()
        .route("/api/chats", get(list_chats))
        .layer(Extension(chats))
        .layer(CorsLayer::very_permissive()) // TODO: Allow less if possible
}
