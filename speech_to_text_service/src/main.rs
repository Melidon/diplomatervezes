use {
    amqprs::{
        callbacks::{DefaultChannelCallback, DefaultConnectionCallback},
        channel::Channel,
        connection::Connection,
    },
    sea_orm::{Database, DatabaseConnection},
    speech_to_text_service::{consumers::RecordingCreatedConsumer, utils::amqp::Listener},
    std::env,
    tokio::time::{sleep, Duration},
};

async fn connect_to_database() -> DatabaseConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set!");
    Database::connect(&database_url)
        .await
        .expect("Failed to connect to database!")
}

async fn connect_to_message_queue() -> (Connection, Channel, Channel) {
    let rabbitmq_uri = env::var("RABBITMQ_URI").expect("RABBITMQ_URI must be set!");
    let connection_arguments = rabbitmq_uri
        .as_str()
        .try_into()
        .expect("RABBITMQ_URI is invalid!");
    let connection = Connection::open(&connection_arguments)
        .await
        .expect("Failed to connect to RabbitMQ!");
    connection
        .register_callback(DefaultConnectionCallback)
        .await
        .expect("Failed to register connection callback!");

    let consume_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    consume_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");

    let publish_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    publish_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");

    (connection, consume_channel, publish_channel)
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();
    let db = connect_to_database().await;
    // DO NOT RENAME `connection` TO `_connection`!!!
    // It will cause the connection to be dropped.
    // Thus it will be closed and channels won't work.
    #[allow(unused_variables)]
    let (connection, consume_channel, publish_channel) = connect_to_message_queue().await;
    consume_channel
        .on(
            "recording_created",
            RecordingCreatedConsumer::new(db, publish_channel).await,
        )
        .await
        .expect("Failed to start consuming!");
    sleep(Duration::from_secs(u64::MAX)).await;
}
