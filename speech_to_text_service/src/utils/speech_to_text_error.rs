#[derive(Debug)]
pub enum SpeechToTextError {
    Amqp(amqprs::error::Error),
    Database(sea_orm::DbErr),
    Deepgram,
    NotFound,
    Reqwest(reqwest::Error),
    Serde(serde_json::Error),
    Uuid(uuid::Error),
}

impl std::fmt::Display for SpeechToTextError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Amqp(error) => write!(f, "AMQP error: {error}"),
            Self::Database(error) => write!(f, "Database error: {error}"),
            Self::Deepgram => write!(f, "Deepgram error"),
            Self::NotFound => write!(f, "Not found"),
            Self::Reqwest(error) => write!(f, "Reqwest error: {error}"),
            Self::Serde(error) => write!(f, "Serde error: {error}"),
            Self::Uuid(error) => write!(f, "UUID error: {error}"),
        }
    }
}

impl std::error::Error for SpeechToTextError {}

impl From<amqprs::error::Error> for SpeechToTextError {
    fn from(error: amqprs::error::Error) -> Self {
        Self::Amqp(error)
    }
}

impl From<sea_orm::DbErr> for SpeechToTextError {
    fn from(error: sea_orm::DbErr) -> Self {
        Self::Database(error)
    }
}

impl From<reqwest::Error> for SpeechToTextError {
    fn from(error: reqwest::Error) -> Self {
        Self::Reqwest(error)
    }
}

impl From<serde_json::Error> for SpeechToTextError {
    fn from(error: serde_json::Error) -> Self {
        Self::Serde(error)
    }
}

impl From<uuid::Error> for SpeechToTextError {
    fn from(error: uuid::Error) -> Self {
        Self::Uuid(error)
    }
}
