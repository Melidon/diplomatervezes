//! `SeaORM` Entity, @generated by sea-orm-codegen 1.0.1

use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq)]
#[sea_orm(table_name = "image")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub id: Uuid,
    pub memory_id: Uuid,
    pub name: String,
    #[sea_orm(column_type = "VarBinary(StringLen::None)")]
    pub bytes: Vec<u8>,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::annotation::Entity")]
    Annotation,
    #[sea_orm(has_many = "super::image_text::Entity")]
    ImageText,
    #[sea_orm(
        belongs_to = "super::memory::Entity",
        from = "Column::MemoryId",
        to = "super::memory::Column::Id",
        on_update = "NoAction",
        on_delete = "Cascade"
    )]
    Memory,
}

impl Related<super::annotation::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Annotation.def()
    }
}

impl Related<super::image_text::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ImageText.def()
    }
}

impl Related<super::memory::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Memory.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
