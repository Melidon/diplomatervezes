mod transcript_result;

use {
    self::transcript_result::TranscriptResult,
    crate::{
        entities::{
            recording::Entity as RecordingTable,
            transcript::{ActiveModel as Transcript, Entity as TranscriptTable},
        },
        utils::speech_to_text_error::SpeechToTextError,
    },
    amqprs::{
        channel::{BasicPublishArguments, Channel, ExchangeDeclareArguments, ExchangeType},
        consumer::AsyncConsumer,
        BasicProperties, Deliver,
    },
    file_format::FileFormat,
    reqwest::{
        header::{HeaderMap, AUTHORIZATION, CONTENT_TYPE},
        Client,
    },
    sea_orm::{prelude::async_trait::async_trait, DatabaseConnection, EntityTrait, Set},
    std::{env, sync::LazyLock},
    tracing::{error, info},
    uuid::Uuid,
};

const TRANSCRIPT_CREATED: &str = "transcript_created";

pub struct RecordingCreatedConsumer {
    db: DatabaseConnection,
    publish_channel: Channel,
}

impl RecordingCreatedConsumer {
    pub async fn new(db: DatabaseConnection, publish_channel: Channel) -> Self {
        publish_channel
            .exchange_declare(ExchangeDeclareArguments::of_type(
                TRANSCRIPT_CREATED,
                ExchangeType::Fanout,
            ))
            .await
            .expect("Failed to declare exchange!");
        Self {
            db,
            publish_channel,
        }
    }

    async fn process(&self, content: Vec<u8>) -> Result<(), SpeechToTextError> {
        let recording_id = Uuid::try_from(content)?;
        info!("Transcribing recording with id: {recording_id}");
        let recording = get_recording(&self.db, recording_id).await?;
        let transcript = transcribe(recording).await?;
        let transcript_id = create_transcript(&self.db, recording_id, transcript).await?;
        notify_transcript_created(&self.publish_channel, transcript_id).await?;
        Ok(())
    }
}

#[async_trait]
impl AsyncConsumer for RecordingCreatedConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        content: Vec<u8>,
    ) {
        if let Err(error) = self.process(content).await {
            error!("{:?}", error);
        }
    }
}

async fn get_recording(
    db: &DatabaseConnection,
    recording_id: Uuid,
) -> Result<Vec<u8>, SpeechToTextError> {
    let bytes = RecordingTable::find_by_id(recording_id)
        .one(db)
        .await?
        .ok_or(SpeechToTextError::NotFound)?
        .bytes;
    Ok(bytes)
}

static DEEPGRAM_API_KEY: LazyLock<String> =
    LazyLock::new(|| env::var("DEEPGRAM_API_KEY").expect("DEEPGRAM_API_KEY must be set"));

async fn transcribe(bytes: Vec<u8>) -> Result<String, SpeechToTextError> {
    const PARAMS: [(&str, &str); 2] = [("model", "nova-2"), ("smart_format", "true")];
    let mut headers = HeaderMap::new();
    headers.insert(
        AUTHORIZATION,
        format!("Token {}", DEEPGRAM_API_KEY.as_str())
            .parse()
            .expect("Failed to parse Authorization header"),
    );
    headers.insert(
        CONTENT_TYPE,
        FileFormat::from_bytes(&bytes)
            .media_type()
            .parse()
            .expect("Failed to parse ContentType header"),
    );
    let transcript = Client::new()
        .post("https://api.deepgram.com/v1/listen")
        .query(&PARAMS)
        .headers(headers)
        .body(bytes)
        .send()
        .await?
        .text()
        .await?
        .parse::<TranscriptResult>()?
        .results
        .channels
        .into_iter()
        .next()
        .ok_or(SpeechToTextError::Deepgram)?
        .alternatives
        .into_iter()
        .next()
        .ok_or(SpeechToTextError::Deepgram)?
        .transcript;
    Ok(transcript)
}

async fn create_transcript(
    db: &DatabaseConnection,
    recording_id: Uuid,
    transcript: String,
) -> Result<Uuid, sea_orm::DbErr> {
    let transcript_id = Uuid::new_v4();
    let transcript = Transcript {
        id: Set(transcript_id),
        recording_id: Set(recording_id),
        text: Set(transcript),
        revised: Set(false),
    };
    TranscriptTable::insert(transcript).exec(db).await?;
    Ok(transcript_id)
}

async fn notify_transcript_created(
    publish_channel: &Channel,
    transcript_id: Uuid,
) -> Result<(), amqprs::error::Error> {
    info!("Transcript created with id: {transcript_id}");
    publish_channel
        .basic_publish(
            BasicProperties::default(),
            transcript_id.as_bytes().to_vec(),
            BasicPublishArguments::new(TRANSCRIPT_CREATED, ""),
        )
        .await?;
    Ok(())
}
