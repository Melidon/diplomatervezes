#![allow(dead_code)]

use {
    chrono::{DateTime, Utc},
    serde::Deserialize,
    std::{collections::HashMap, str::FromStr},
    uuid::Uuid,
};

impl FromStr for TranscriptResult {
    type Err = serde_json::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_json::from_str(s)
    }
}

#[derive(Deserialize)]
pub struct TranscriptResult {
    pub metadata: Metadata,
    pub results: Results,
}

#[derive(Deserialize)]
pub struct Metadata {
    pub transaction_key: String,
    pub request_id: Uuid,
    pub sha256: String,
    pub created: DateTime<Utc>,
    pub duration: f64,
    pub channels: usize,
    pub models: Vec<Uuid>,
    pub model_info: HashMap<Uuid, ModelInfo>,
}

#[derive(Deserialize)]
pub struct ModelInfo {
    pub name: String,
    pub version: String,
    pub arch: String,
}

#[derive(Deserialize)]
pub struct Results {
    pub channels: Vec<Channel>,
}

#[derive(Deserialize)]
pub struct Channel {
    pub alternatives: Vec<Alternative>,
}

#[derive(Deserialize)]
pub struct Alternative {
    pub transcript: String,
    pub confidence: f64,
    pub words: Vec<Word>,
    pub paragraphs: Option<Paragraphs>,
}

#[derive(Deserialize)]
pub struct Word {
    pub word: String,
    pub start: f64,
    pub end: f64,
    pub confidence: f64,
    pub punctuated_word: String,
}

#[derive(Deserialize)]
pub struct Paragraphs {
    pub transcript: String,
    pub paragraphs: Vec<Paragraph>,
}

#[derive(Deserialize)]
pub struct Paragraph {
    pub sentences: Vec<Sentence>,
    pub num_words: usize,
    pub start: f64,
    pub end: f64,
}

#[derive(Deserialize)]
pub struct Sentence {
    pub text: String,
    pub start: f64,
    pub end: f64,
}
