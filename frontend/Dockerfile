FROM debian:bookworm AS build
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y curl git libglu1-mesa unzip xz-utils zip && \
    apt-get clean
ADD https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.24.3-stable.tar.xz /tmp/flutter.tar.xz
RUN tar -xf /tmp/flutter.tar.xz -C /usr/local && \
    rm /tmp/flutter.tar.xz && \
    git config --global --add safe.directory /usr/local/flutter && \
    useradd --create-home flutteruser
USER flutteruser
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"
RUN flutter doctor
WORKDIR /app
COPY --chown=flutteruser:flutteruser . .
ARG BACKEND_URL
RUN flutter pub get && \
    dart run build_runner build --delete-conflicting-outputs && \
    flutter build web --dart-define=BACKEND_URL="$BACKEND_URL"

FROM nginx:1.27
COPY --from=build /app/build/web /usr/share/nginx/html
