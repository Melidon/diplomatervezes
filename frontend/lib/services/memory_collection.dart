import "dart:typed_data";

import "package:chopper/chopper.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/services/backend.dart";
import "package:reminiscence_hub/generated/services/memory_collection_service.dart" as generated;
import "package:reminiscence_hub/utils/cache.dart";

class EntityService {
  static generated.EntityService get instance => Backend.instance.getService<generated.EntityService>();
}

class MemoryService {
  static generated.MemoryService get instance => Backend.instance.getService<generated.MemoryService>();
}

class MemoryTextService {
  static generated.MemoryTextService get instance => Backend.instance.getService<generated.MemoryTextService>();
}

class ImageService {
  static generated.ImageService get instance => _instance;
  static final _instance = ImageServiceWithCache(Backend.instance.getService<generated.ImageService>());
}

class ImageServiceWithCache implements generated.ImageService {
  final generated.ImageService imageService;
  final cache = Cache<String, Uint8List>();

  ImageServiceWithCache(this.imageService) : client = imageService.client;

  @override
  ChopperClient client;

  @override
  Future<ImageMeta> createImage({required String memoryId, required Uint8List image}) => imageService.createImage(memoryId: memoryId, image: image);

  @override
  Type get definitionType => ImageService;

  @override
  Future<Response> deleteImage({required String imageId}) => imageService.deleteImage(imageId: imageId);

  @override
  Future<Uint8List> getImage({required String imageId}) {
    return cache.getOrLoad(
      imageId,
      () async => imageService.getImage(imageId: imageId),
    );
  }

  @override
  Future<List<ImageMeta>> listImages({required String memoryId}) => imageService.listImages(memoryId: memoryId);

  @override
  Future<Response> updateImage({required String imageId, required UpdateImageMeta imageMeta}) =>
      imageService.updateImage(imageId: imageId, imageMeta: imageMeta);
}

class ImageTextService {
  static generated.ImageTextService get instance => Backend.instance.getService<generated.ImageTextService>();
}

class AnnotationService {
  static generated.AnnotationService get instance => Backend.instance.getService<generated.AnnotationService>();
}

class RecordingService {
  static generated.RecordingService get instance => Backend.instance.getService<generated.RecordingService>();
}

class TranscriptService {
  static generated.TranscriptService get instance => Backend.instance.getService<generated.TranscriptService>();
}
