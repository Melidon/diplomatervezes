import "package:reminiscence_hub/generated/services/chats_service.dart" as generated;
import "package:reminiscence_hub/services/backend.dart";

class ChatsService {
  static generated.ChatsService get instance => Backend.instance.getService<generated.ChatsService>();
}
