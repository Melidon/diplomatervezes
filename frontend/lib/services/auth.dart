import "dart:async";

import "package:dart_jsonwebtoken/dart_jsonwebtoken.dart";
import "package:localstorage/localstorage.dart";
import "package:reminiscence_hub/services/backend.dart";
import "package:reminiscence_hub/generated/services/auth_service.dart" as generated;
import "package:reminiscence_hub/generated/models/auth_models.dart";

class Auth {
  static Auth get instance => _instance;
  static final _instance = Auth._();
  Auth._();

  Stream<User?> authStateChanges() => AuthRepository.instance.authStateChanges().map(
        (accessToken) => accessToken == null ? null : User.fromToken(accessToken),
      );

  final authService = Backend.instance.getService<generated.AuthService>();

  Future<void> createUser(SignUpRequest signUpRequest) async {
    final response = await authService.signUp(signUpRequest: signUpRequest);
    if (!response.isSuccessful) {
      return Future.error(response.error!);
    }
    AuthRepository.instance.setAccessToken(response.body!);
  }

  Future<void> signIn(signInRequest) async {
    final response = await authService.signIn(signInRequest: signInRequest);
    if (!response.isSuccessful) {
      return Future.error(response.error!);
    }
    AuthRepository.instance.setAccessToken(response.body!);
  }

  void signOut() {
    AuthRepository.instance.setAccessToken(null);
  }
}

class RelationshipService {
  static generated.RelationshipService get instance => Backend.instance.getService<generated.RelationshipService>();
}

class AuthRepository {
  static AuthRepository get instance => _instance;
  static final _instance = AuthRepository._();
  AuthRepository._();

  Stream<String?> authStateChanges() => _authStateController.stream;

  late final StreamController<String?> _authStateController = StreamController<String?>.broadcast(
    onListen: () {
      final accessToken = getAccessToken();
      _authStateController.add(accessToken?.accessToken);
    },
  );

  static const _accessTokenKey = "accessToken";
  static const _tokenTypeKey = "tokenType";

  AuthResponse? getAccessToken() {
    final accessToken = localStorage.getItem(_accessTokenKey);
    if (accessToken == null) {
      return null;
    }
    final tokenType = localStorage.getItem(_tokenTypeKey);
    if (tokenType == null) {
      return null;
    }
    return AuthResponse(accessToken: accessToken, tokenType: tokenType);
  }

  void setAccessToken(AuthResponse? accessToken) {
    if (accessToken == null) {
      localStorage.removeItem(_accessTokenKey);
      localStorage.removeItem(_tokenTypeKey);
    } else {
      localStorage.setItem(_accessTokenKey, accessToken.accessToken);
      localStorage.setItem(_tokenTypeKey, accessToken.tokenType);
    }
    _authStateController.add(accessToken?.accessToken);
  }
}

class User {
  final String id;
  final String name;
  final Role role;

  User({
    required this.id,
    required this.name,
    required this.role,
  });

  factory User.fromToken(String token) {
    final payload = JWT.decode(token).payload as Map<String, dynamic>;
    return User(
      id: payload["user_id"],
      name: payload["name"],
      role: RoleFrom.json(payload["role"]),
    );
  }
}
