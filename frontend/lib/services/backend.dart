import "dart:async";
import "dart:io";

import "package:chopper/chopper.dart" hide Authenticator;
import "package:chopper/chopper.dart" as chopper show Authenticator;
import "package:reminiscence_hub/generated/services/auth_service.dart" show AuthService, RelationshipService;
import "package:reminiscence_hub/generated/models/auth_models.dart" show AuthResponse;
import "package:reminiscence_hub/generated/json_serializable_converter.dart" show JsonSerializableConverter;
import "package:reminiscence_hub/generated/services/memory_collection_service.dart";
import "package:reminiscence_hub/generated/services/chats_service.dart";
import "package:reminiscence_hub/services/auth.dart" show AuthRepository;

// Loaded at compile time
const backendUrl = String.fromEnvironment("BACKEND_URL", defaultValue: "http://localhost/api");

class Backend {
  static ChopperClient get instance => _chopper;
  static final _chopper = ChopperClient(
    baseUrl: Uri.parse(backendUrl),
    interceptors: [
      AuthInterceptor(),
    ],
    authenticator: Authenticator(),
    converter: JsonSerializableConverter(),
    services: [
      // AuthService
      AuthService.create(),
      RelationshipService.create(),

      // MemoryCollectionService
      EntityService.create(),
      MemoryService.create(),
      MemoryTextService.create(),
      ImageService.create(),
      ImageTextService.create(),
      AnnotationService.create(),
      RecordingService.create(),
      TranscriptService.create(),

      // ChatsService
      ChatsService.create(),
    ],
  );
}

class AuthInterceptor implements Interceptor {
  @override
  FutureOr<Response<BodyType>> intercept<BodyType>(Chain<BodyType> chain) {
    final token = AuthRepository.instance.getAccessToken();
    if (token == null) {
      return chain.proceed(chain.request);
    }
    final request = applyHeader(chain.request, HttpHeaders.authorizationHeader, token.toAuthorizationHeader());
    return chain.proceed(request);
  }
}

class Authenticator extends chopper.Authenticator {
  Authenticator();

  @override
  FutureOr<Request?> authenticate(
    Request request,
    Response response, [
    Request? originalRequest,
  ]) {
    if (response.statusCode == HttpStatus.unauthorized) {
      AuthRepository.instance.setAccessToken(null);
    }
    return null;
  }
}

extension on AuthResponse {
  String toAuthorizationHeader() => "$tokenType $accessToken";
}
