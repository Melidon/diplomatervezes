import "package:json_annotation/json_annotation.dart";

part "chat_models.g.dart";

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class ChatDescription {
  String name;
  String path;

  ChatDescription({
    required this.name,
    required this.path,
  });

  factory ChatDescription.fromJson(Map<String, dynamic> json) => _$ChatDescriptionFromJson(json);
  Map<String, dynamic> toJson() => _$ChatDescriptionToJson(this);
}
