import "package:json_annotation/json_annotation.dart";

part "memory_collection_models.g.dart";

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class CreateEntity {
  final String ownerId;
  final String name;

  CreateEntity({
    required this.ownerId,
    required this.name,
  });

  factory CreateEntity.fromJson(Map<String, dynamic> json) => _$CreateEntityFromJson(json);
  Map<String, dynamic> toJson() => _$CreateEntityToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateEntity {
  final String name;

  UpdateEntity({
    required this.name,
  });

  factory UpdateEntity.fromJson(Map<String, dynamic> json) => _$UpdateEntityFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateEntityToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class Entity {
  final String id;
  final String name;

  Entity({
    required this.id,
    required this.name,
  });

  factory Entity.fromJson(Map<String, dynamic> json) => _$EntityFromJson(json);
  Map<String, dynamic> toJson() => _$EntityToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class CreateMemory {
  final String ownerId;
  final String name;

  CreateMemory({
    required this.ownerId,
    required this.name,
  });

  factory CreateMemory.fromJson(Map<String, dynamic> json) => _$CreateMemoryFromJson(json);
  Map<String, dynamic> toJson() => _$CreateMemoryToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateMemory {
  final String name;

  UpdateMemory({
    required this.name,
  });

  factory UpdateMemory.fromJson(Map<String, dynamic> json) => _$UpdateMemoryFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateMemoryToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class Memory {
  final String id;
  final String name;

  Memory({
    required this.id,
    required this.name,
  });

  factory Memory.fromJson(Map<String, dynamic> json) => _$MemoryFromJson(json);
  Map<String, dynamic> toJson() => _$MemoryToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class CreateMemoryText {
  final String memoryId;
  final String text;

  CreateMemoryText({
    required this.memoryId,
    required this.text,
  });

  factory CreateMemoryText.fromJson(Map<String, dynamic> json) => _$CreateMemoryTextFromJson(json);
  Map<String, dynamic> toJson() => _$CreateMemoryTextToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateMemoryText {
  final String text;

  UpdateMemoryText({
    required this.text,
  });

  factory UpdateMemoryText.fromJson(Map<String, dynamic> json) => _$UpdateMemoryTextFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateMemoryTextToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class MemoryText {
  final String id;
  final String text;

  MemoryText({
    required this.id,
    required this.text,
  });

  factory MemoryText.fromJson(Map<String, dynamic> json) => _$MemoryTextFromJson(json);
  Map<String, dynamic> toJson() => _$MemoryTextToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateImageMeta {
  final String name;

  UpdateImageMeta({
    required this.name,
  });

  factory UpdateImageMeta.fromJson(Map<String, dynamic> json) => _$UpdateImageMetaFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateImageMetaToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class ImageMeta {
  final String id;
  final String name;

  ImageMeta({required this.id, required this.name});

  factory ImageMeta.fromJson(Map<String, dynamic> json) => _$ImageMetaFromJson(json);
  Map<String, dynamic> toJson() => _$ImageMetaToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class CreateImageText {
  final String imageId;
  final String text;

  CreateImageText({
    required this.imageId,
    required this.text,
  });

  factory CreateImageText.fromJson(Map<String, dynamic> json) => _$CreateImageTextFromJson(json);
  Map<String, dynamic> toJson() => _$CreateImageTextToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateImageText {
  final String text;

  UpdateImageText({
    required this.text,
  });

  factory UpdateImageText.fromJson(Map<String, dynamic> json) => _$UpdateImageTextFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateImageTextToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class ImageText {
  final String id;
  final String text;

  ImageText({
    required this.id,
    required this.text,
  });

  factory ImageText.fromJson(Map<String, dynamic> json) => _$ImageTextFromJson(json);
  Map<String, dynamic> toJson() => _$ImageTextToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class CreateAnnotation {
  final String imageId;
  final String entityId;
  final int left;
  final int top;
  final int width;
  final int height;

  CreateAnnotation({
    required this.imageId,
    required this.entityId,
    required this.left,
    required this.top,
    required this.width,
    required this.height,
  });

  factory CreateAnnotation.fromJson(Map<String, dynamic> json) => _$CreateAnnotationFromJson(json);
  Map<String, dynamic> toJson() => _$CreateAnnotationToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateAnnotation {
  final String entityId;
  final int left;
  final int top;
  final int width;
  final int height;

  UpdateAnnotation({
    required this.entityId,
    required this.left,
    required this.top,
    required this.width,
    required this.height,
  });

  factory UpdateAnnotation.fromJson(Map<String, dynamic> json) => _$UpdateAnnotationFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateAnnotationToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class Annotation {
  final String id;
  final String entityId;
  final int left;
  final int top;
  final int width;
  final int height;
  final bool revised;

  Annotation({
    required this.id,
    required this.entityId,
    required this.left,
    required this.top,
    required this.width,
    required this.height,
    required this.revised,
  });

  factory Annotation.fromJson(Map<String, dynamic> json) => _$AnnotationFromJson(json);
  Map<String, dynamic> toJson() => _$AnnotationToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateRecordingMeta {
  final String name;

  UpdateRecordingMeta({
    required this.name,
  });

  factory UpdateRecordingMeta.fromJson(Map<String, dynamic> json) => _$UpdateRecordingMetaFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateRecordingMetaToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class RecordingMeta {
  final String id;
  final String name;

  RecordingMeta({
    required this.id,
    required this.name,
  });

  factory RecordingMeta.fromJson(Map<String, dynamic> json) => _$RecordingMetaFromJson(json);
  Map<String, dynamic> toJson() => _$RecordingMetaToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class CreateTranscript {
  final String recordingId;
  final String text;

  CreateTranscript({
    required this.recordingId,
    required this.text,
  });

  factory CreateTranscript.fromJson(Map<String, dynamic> json) => _$CreateTranscriptFromJson(json);
  Map<String, dynamic> toJson() => _$CreateTranscriptToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class UpdateTranscript {
  final String text;

  UpdateTranscript({
    required this.text,
  });

  factory UpdateTranscript.fromJson(Map<String, dynamic> json) => _$UpdateTranscriptFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateTranscriptToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class Transcript {
  final String id;
  final String text;
  final bool revised;

  Transcript({
    required this.id,
    required this.text,
    required this.revised,
  });

  factory Transcript.fromJson(Map<String, dynamic> json) => _$TranscriptFromJson(json);
  Map<String, dynamic> toJson() => _$TranscriptToJson(this);
}
