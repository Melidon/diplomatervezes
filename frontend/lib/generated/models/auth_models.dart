import "package:json_annotation/json_annotation.dart";

part "auth_models.g.dart";

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class AuthResponse {
  String accessToken;
  String tokenType;

  AuthResponse({
    required this.accessToken,
    required this.tokenType,
  });

  factory AuthResponse.fromJson(Map<String, dynamic> json) => _$AuthResponseFromJson(json);
  Map<String, dynamic> toJson() => _$AuthResponseToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class SignUpRequest {
  String username;
  String password;
  Role role;

  SignUpRequest({
    required this.username,
    required this.password,
    required this.role,
  });

  factory SignUpRequest.fromJson(Map<String, dynamic> json) => _$SignUpRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SignUpRequestToJson(this);
}

@JsonEnum(fieldRename: FieldRename.pascal)
enum Role {
  relative,
  elderly,
}

extension RoleFrom on Role {
  static Role json(String value) => $enumDecode(_$RoleEnumMap, value);
  String toJson() => _$RoleEnumMap[this]!;
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class SignInRequest {
  String username;
  String password;

  SignInRequest({
    required this.username,
    required this.password,
  });

  factory SignInRequest.fromJson(Map<String, dynamic> json) => _$SignInRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SignInRequestToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class ChangePasswordRequest {
  String newPassword;

  ChangePasswordRequest({
    required this.newPassword,
  });

  factory ChangePasswordRequest.fromJson(Map<String, dynamic> json) => _$ChangePasswordRequestFromJson(json);
  Map<String, dynamic> toJson() => _$ChangePasswordRequestToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class RelativeRequest {
  String relativeId;

  RelativeRequest({
    required this.relativeId,
  });

  factory RelativeRequest.fromJson(Map<String, dynamic> json) => _$RelativeRequestFromJson(json);
  Map<String, dynamic> toJson() => _$RelativeRequestToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  fieldRename: FieldRename.snake,
)
class RelativeResponse {
  String id;
  String username;

  RelativeResponse({
    required this.id,
    required this.username,
  });

  factory RelativeResponse.fromJson(Map<String, dynamic> json) => _$RelativeResponseFromJson(json);
  Map<String, dynamic> toJson() => _$RelativeResponseToJson(this);
}
