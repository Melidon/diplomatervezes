import "dart:async";
import "package:chopper/chopper.dart";
import "package:reminiscence_hub/generated/models/chat_models.dart";

part "chats_service.chopper.dart";

@ChopperApi(baseUrl: "/chats")
abstract class ChatsService extends ChopperService {
  static ChatsService create([ChopperClient? client]) => _$ChatsService(client);

  @Get()
  Future<List<ChatDescription>> listChats();
}
