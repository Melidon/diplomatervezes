import "dart:async";
import "package:chopper/chopper.dart";
import "package:reminiscence_hub/generated/models/auth_models.dart";

part "auth_service.chopper.dart";

@ChopperApi(baseUrl: "/auth")
abstract class AuthService extends ChopperService {
  static AuthService create([ChopperClient? client]) => _$AuthService(client);

  @Post(path: "/sign_up")
  Future<Response<AuthResponse>> signUp({
    @Body() required SignUpRequest signUpRequest,
  });

  @Post(path: "/sign_in")
  Future<Response<AuthResponse>> signIn({
    @Body() required SignInRequest signInRequest,
  });

  @Patch(path: "/change_password")
  Future<Response<void>> changePassword({
    @Body() required ChangePasswordRequest changePasswordRequest,
  });

  @Delete(path: "/delete_account")
  Future<Response<void>> deleteAccount();
}

@ChopperApi(baseUrl: "/auth/relationship")
abstract class RelationshipService extends ChopperService {
  static RelationshipService create([ChopperClient? client]) => _$RelationshipService(client);

  @Post()
  Future<void> addRelative({
    @Body() required RelativeRequest relativeRequest,
  });

  @Get()
  Future<List<RelativeResponse>> listRelatives();

  @Delete()
  Future<void> removeRelative({
    @Body() required RelativeRequest relativeRequest,
  });
}
