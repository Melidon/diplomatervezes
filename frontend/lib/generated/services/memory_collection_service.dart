import "dart:typed_data";

import "package:chopper/chopper.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";

part "memory_collection_service.chopper.dart";

@ChopperApi(baseUrl: "/memory_collection/entities")
abstract class EntityService extends ChopperService {
  static EntityService create([ChopperClient? client]) => _$EntityService(client);

  @Post()
  Future<Entity> createEntity({
    @Body() required CreateEntity entity,
  });

  @Get(path: "/{entity_id}")
  Future<Entity> getEntity({
    @Path("entity_id") required String entityId,
  });

  @Get()
  Future<List<Entity>> listEntities({
    @Query("owner_id") required String ownerId,
  });

  @Put(path: "/{entity_id}")
  Future<Response> updateEntity({
    @Path("entity_id") required String entityId,
    @Body() required UpdateEntity entity,
  });

  @Delete(path: "/{entity_id}")
  Future<Response> deleteEntity({
    @Path("entity_id") required String entityId,
  });
}

@ChopperApi(baseUrl: "/memory_collection/memories")
abstract class MemoryService extends ChopperService {
  static MemoryService create([ChopperClient? client]) => _$MemoryService(client);

  @Post()
  Future<Memory> createMemory({
    @Body() required CreateMemory memory,
  });

  @Get(path: "/{memory_id}")
  Future<Memory> getMemory({
    @Path("memory_id") required String memoryId,
  });

  @Get()
  Future<List<Memory>> listMemories({
    @Query("owner_id") required String ownerId,
  });

  @Put(path: "/{memory_id}")
  Future<Response> updateMemory({
    @Path("memory_id") required String memoryId,
    @Body() required UpdateMemory memory,
  });

  @Delete(path: "/{memory_id}")
  Future<Response> deleteMemory({
    @Path("memory_id") required String memoryId,
  });
}

@ChopperApi(baseUrl: "/memory_collection/memory_texts")
abstract class MemoryTextService extends ChopperService {
  static MemoryTextService create([ChopperClient? client]) => _$MemoryTextService(client);

  @Post()
  Future<MemoryText> createMemoryText({
    @Body() required CreateMemoryText memoryText,
  });

  @Get(path: "/{memory_text_id}")
  Future<MemoryText> getMemoryText({
    @Path("memory_text_id") required String memoryTextId,
  });

  @Get()
  Future<List<MemoryText>> listMemoryTexts({
    @Query("memory_id") required String memoryId,
  });

  @Put(path: "/{memory_text_id}")
  Future<Response> updateMemoryText({
    @Path("memory_text_id") required String memoryTextId,
    @Body() required UpdateMemoryText memoryText,
  });

  @Delete(path: "/{memory_text_id}")
  Future<Response> deleteMemoryText({
    @Path("memory_text_id") required String memoryTextId,
  });
}

@ChopperApi(baseUrl: "/memory_collection/images")
abstract class ImageService extends ChopperService {
  static ImageService create([ChopperClient? client]) => _$ImageService(client);

  @Post()
  @Multipart()
  Future<ImageMeta> createImage({
    @Query("memory_id") required String memoryId,
    @PartFile() required Uint8List image,
  });

  @FactoryConverter(
    response: convertBinaryResponse,
  )
  @Get(path: "/{image_id}")
  Future<Uint8List> getImage({
    @Path("image_id") required String imageId,
  });

  @Get()
  Future<List<ImageMeta>> listImages({
    @Query("memory_id") required String memoryId,
  });

  @Put(path: "/{image_id}")
  Future<Response> updateImage({
    @Path("image_id") required String imageId,
    @Body() required UpdateImageMeta imageMeta,
  });

  @Delete(path: "/{image_id}")
  Future<Response> deleteImage({
    @Path("image_id") required String imageId,
  });
}

Response<Uint8List> convertBinaryResponse(Response response) {
  return response.copyWith<Uint8List>(body: response.bodyBytes);
}

@ChopperApi(baseUrl: "/memory_collection/image_texts")
abstract class ImageTextService extends ChopperService {
  static ImageTextService create([ChopperClient? client]) => _$ImageTextService(client);

  @Post()
  Future<ImageText> createImageText({
    @Body() required CreateImageText imageText,
  });

  @Get(path: "/{image_text_id}")
  Future<ImageText> getImageText({
    @Path("image_text_id") required String imageTextId,
  });

  @Get()
  Future<List<ImageText>> listImageTexts({
    @Query("image_id") required String imageId,
  });

  @Put(path: "/{image_text_id}")
  Future<Response> updateImageText({
    @Path("image_text_id") required String imageTextId,
    @Body() required UpdateImageText imageText,
  });

  @Delete(path: "/{image_text_id}")
  Future<Response> deleteImageText({
    @Path("image_text_id") required String imageTextId,
  });
}

@ChopperApi(baseUrl: "/memory_collection/annotations")
abstract class AnnotationService extends ChopperService {
  static AnnotationService create([ChopperClient? client]) => _$AnnotationService(client);

  @Post()
  Future<Annotation> createAnnotation({
    @Body() required CreateAnnotation annotation,
  });

  @Get(path: "/{annotation_id}")
  Future<Annotation> getAnnotation({
    @Path("annotation_id") required String annotationId,
  });

  @Get()
  Future<List<Annotation>> listAnnotations({
    @Query("image_id") required String imageId,
  });

  @Put(path: "/{annotation_id}")
  Future<Response> updateAnnotation({
    @Path("annotation_id") required String annotationId,
    @Body() required UpdateAnnotation annotation,
  });

  @Delete(path: "/{annotation_id}")
  Future<Response> deleteAnnotation({
    @Path("annotation_id") required String annotationId,
  });
}

@ChopperApi(baseUrl: "/memory_collection/recordings")
abstract class RecordingService extends ChopperService {
  static RecordingService create([ChopperClient? client]) => _$RecordingService(client);

  @Post()
  @Multipart()
  Future<RecordingMeta> createRecording({
    @Query("memory_id") required String memoryId,
    @PartFile() required Uint8List recording,
  });

  @FactoryConverter(
    response: convertBinaryResponse,
  )
  @Get(path: "/{recording_id}")
  Future<Uint8List> getRecording({
    @Path("recording_id") required String recordingId,
  });

  @Get()
  Future<List<RecordingMeta>> listRecordings({
    @Query("memory_id") required String memoryId,
  });

  @Put(path: "/{recording_id}")
  Future<Response> updateRecording({
    @Path("recording_id") required String recordingId,
    @Body() required UpdateRecordingMeta recordingMeta,
  });

  @Delete(path: "/{recording_id}")
  Future<Response> deleteRecording({
    @Path("recording_id") required String recordingId,
  });
}

@ChopperApi(baseUrl: "/memory_collection/transcripts")
abstract class TranscriptService extends ChopperService {
  static TranscriptService create([ChopperClient? client]) => _$TranscriptService(client);

  @Post()
  Future<Transcript> createTranscript({
    @Body() required CreateTranscript transcript,
  });

  @Get(path: "/{transcript_id}")
  Future<Transcript> getTranscript({
    @Path("transcript_id") required String transcriptId,
  });

  @Get()
  Future<List<Transcript>> listTranscripts({
    @Query("recording_id") required String recordingId,
  });

  @Put(path: "/{transcript_id}")
  Future<Response> updateTranscript({
    @Path("transcript_id") required String transcriptId,
    @Body() required UpdateTranscript transcript,
  });

  @Delete(path: "/{transcript_id}")
  Future<Response> deleteTranscript({
    @Path("transcript_id") required String transcriptId,
  });
}
