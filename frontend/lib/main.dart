import "package:flutter/material.dart";
import "package:localstorage/localstorage.dart";
import "package:reminiscence_hub/generated/models/auth_models.dart";
import "package:reminiscence_hub/screens/elderly_home_page.dart";
import "package:reminiscence_hub/screens/relative_home_page.dart";
import "package:reminiscence_hub/screens/sign_in_page.dart";
import "package:reminiscence_hub/services/auth.dart";

Future<void> main() async {
  await initLocalStorage();
  runApp(const ReminiscenceHubApp());
}

class ReminiscenceHubApp extends StatelessWidget {
  const ReminiscenceHubApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Reminiscence Hub",
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: const AuthGate(),
    );
  }
}

class AuthGate extends StatelessWidget {
  const AuthGate({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Auth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return const SignInPage();
        }
        return HomePage(user: snapshot.data!);
      },
    );
  }
}

class HomePage extends StatelessWidget {
  final User user;

  const HomePage({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return switch (user.role) {
      Role.elderly => ElderlyHomePage(user: user),
      Role.relative => const RelativeHomePage(),
    };
  }
}
