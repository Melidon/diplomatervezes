import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/screens/memory_page.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class MemoriesScreen extends StatefulWidget {
  final String ownerId;

  const MemoriesScreen({super.key, required this.ownerId});

  @override
  State<MemoriesScreen> createState() => _MemoriesScreenState();
}

class _MemoriesScreenState extends State<MemoriesScreen> {
  late Future<List<Memory>> memoriesFuture;
  late List<Memory> memories;

  @override
  void initState() {
    super.initState();
    memoriesFuture = MemoryService.instance.listMemories(ownerId: widget.ownerId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createMemory(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: memoriesFuture,
        builder: (context, data) {
          memories = data;
          return ListView.builder(
            itemCount: memories.length,
            itemBuilder: (context, index) {
              final memory = memories[index];
              return ListTile(
                title: Text(memory.name),
                trailing: EditAndDeleteMenu(
                  item: memory,
                  onEdit: editMemory,
                  onDelete: deleteMemory,
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MemoryPage(
                        ownerId: widget.ownerId,
                        memory: memory,
                      ),
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  Future<void> createMemory(BuildContext context) async {
    final createMemory = await showDialog<CreateMemory>(
      context: context,
      builder: (context) {
        final nameController = TextEditingController();
        return AlertDialog(
          title: const Text("Create Memory"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              labelText: "Name",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(
                  context,
                  CreateMemory(
                    ownerId: widget.ownerId,
                    name: nameController.text,
                  )),
              child: const Text("Create"),
            ),
          ],
        );
      },
    );
    if (createMemory == null) {
      return;
    }
    try {
      final memory = await MemoryService.instance.createMemory(
        memory: createMemory,
      );
      setState(() {
        memories.add(memory);
      });
      if (context.mounted) {
        showSnackBar(context, "${memory.name} created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editMemory(BuildContext context, Memory memory) async {
    final updateMemory = await showDialog<UpdateMemory>(
      context: context,
      builder: (context) {
        final nameController = TextEditingController(text: memory.name);
        return AlertDialog(
          title: Text("Edit ${memory.name}"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              labelText: "Name",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateMemory(name: nameController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateMemory == null) {
      return;
    }
    try {
      await MemoryService.instance.updateMemory(
        memoryId: memory.id,
        memory: updateMemory,
      );
      setState(() {
        final index = memories.indexWhere((m) => m == memory);
        memories[index] = Memory(
          id: memory.id,
          name: updateMemory.name,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "${updateMemory.name} updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteMemory(BuildContext context, Memory memory) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Delete ${memory.name}"),
          content: Text("Are you sure you want to delete ${memory.name}?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await MemoryService.instance.deleteMemory(memoryId: memory.id);
      setState(() {
        memories.remove(memory);
      });
      if (context.mounted) {
        showSnackBar(context, "${memory.name} deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
