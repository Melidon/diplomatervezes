import "package:file_picker/file_picker.dart";
import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/screens/image_page.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class ImagesScreen extends StatefulWidget {
  final String ownerId;
  final String memoryId;

  const ImagesScreen({
    super.key,
    required this.ownerId,
    required this.memoryId,
  });

  @override
  State<ImagesScreen> createState() => _ImagesScreenState();
}

class _ImagesScreenState extends State<ImagesScreen> {
  late Future<List<ImageMeta>> imageMetasFuture;
  late List<ImageMeta> imageMetas;

  @override
  void initState() {
    super.initState();
    imageMetasFuture = ImageService.instance.listImages(
      memoryId: widget.memoryId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createImage(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: imageMetasFuture,
        builder: (context, data) {
          imageMetas = data;
          return ListView.builder(
            itemCount: imageMetas.length,
            itemBuilder: (context, index) {
              final imageMeta = imageMetas[index];
              return FutureBuilder(
                future: ImageService.instance.getImage(
                  imageId: imageMeta.id,
                ),
                builder: (context, snapshot) {
                  late Widget leading;
                  GestureTapCallback? onTap;
                  if (snapshot.hasData) {
                    final imageBytes = snapshot.data!;
                    leading = Hero(
                      tag: imageMeta.id,
                      child: Image.memory(imageBytes),
                    );
                    onTap = () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ImagePage(
                            ownerId: widget.ownerId,
                            imageMeta: imageMeta,
                            imageBytes: imageBytes,
                          ),
                        ),
                      );
                    };
                  } else if (snapshot.hasError) {
                    leading = const Icon(Icons.error);
                  } else {
                    leading = const CircularProgressIndicator();
                  }
                  return ListTile(
                    leading: leading,
                    title: Text(imageMeta.name),
                    trailing: EditAndDeleteMenu(
                      item: imageMeta,
                      onEdit: editImage,
                      onDelete: deleteImage,
                    ),
                    onTap: onTap,
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  Future<void> createImage(BuildContext context) async {
    final result = await FilePicker.platform.pickFiles(withData: true);
    if (result == null) {
      return;
    }
    final image = result.files.single.bytes;
    if (image == null) {
      return;
    }
    try {
      final imageResponse = await ImageService.instance.createImage(
        memoryId: widget.memoryId,
        image: image,
      );
      setState(() {
        imageMetas.add(imageResponse);
      });
      if (context.mounted) {
        showSnackBar(context, "${imageResponse.name} created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editImage(BuildContext context, ImageMeta imageMeta) async {
    final updateImageMeta = await showDialog<UpdateImageMeta>(
      context: context,
      builder: (context) {
        final nameController = TextEditingController(text: imageMeta.name);
        return AlertDialog(
          title: Text("Edit ${imageMeta.name}"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              labelText: "Name",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateImageMeta(name: nameController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateImageMeta == null) {
      return;
    }
    try {
      await ImageService.instance.updateImage(
        imageId: imageMeta.id,
        imageMeta: updateImageMeta,
      );
      setState(() {
        final index = imageMetas.indexWhere((i) => i == imageMeta);
        imageMetas[index] = ImageMeta(
          id: imageMeta.id,
          name: updateImageMeta.name,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "${updateImageMeta.name} updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteImage(BuildContext context, ImageMeta imageMeta) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Delete ${imageMeta.name}"),
          content: Text("Are you sure you want to delete ${imageMeta.name}?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await ImageService.instance.deleteImage(
        imageId: imageMeta.id,
      );
      setState(() {
        imageMetas.remove(imageMeta);
      });
      if (context.mounted) {
        showSnackBar(context, "${imageMeta.name} deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
