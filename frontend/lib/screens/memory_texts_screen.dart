import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class MemoryTextsScreen extends StatefulWidget {
  final String memoryId;

  const MemoryTextsScreen({
    super.key,
    required this.memoryId,
  });

  @override
  State<MemoryTextsScreen> createState() => _MemoryTextsScreenState();
}

class _MemoryTextsScreenState extends State<MemoryTextsScreen> {
  late Future<List<MemoryText>> memoryTextsFuture;
  late List<MemoryText> memoryTexts;

  @override
  void initState() {
    super.initState();
    memoryTextsFuture = MemoryTextService.instance.listMemoryTexts(
      memoryId: widget.memoryId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createMemoryText(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: memoryTextsFuture,
        builder: (context, data) {
          memoryTexts = data;
          return ListView.builder(
            itemCount: memoryTexts.length,
            itemBuilder: (context, index) {
              final text = memoryTexts[index];
              return ListTile(
                title: Text(text.text), // TODO: Only show first 50 characters
                trailing: EditAndDeleteMenu(
                  item: text,
                  onEdit: editMemoryText,
                  onDelete: deleteMemoryText,
                ),
              );
            },
          );
        },
      ),
    );
  }

  Future<void> createMemoryText(BuildContext context) async {
    final createMemoryText = await showDialog<CreateMemoryText>(
      context: context,
      builder: (context) {
        final textController = TextEditingController();
        return AlertDialog(
          title: const Text("Create Text"),
          content: TextField(
            controller: textController,
            maxLines: null,
            decoration: const InputDecoration(
              labelText: "Text",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(
                context,
                CreateMemoryText(
                  memoryId: widget.memoryId,
                  text: textController.text,
                ),
              ),
              child: const Text("Create"),
            ),
          ],
        );
      },
    );
    if (createMemoryText == null) {
      return;
    }
    try {
      final memoryText = await MemoryTextService.instance.createMemoryText(memoryText: createMemoryText);
      setState(() {
        memoryTexts.add(memoryText);
      });
      if (context.mounted) {
        showSnackBar(context, "Text created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editMemoryText(BuildContext context, MemoryText memoryText) async {
    final updateMemoryText = await showDialog<UpdateMemoryText>(
      context: context,
      builder: (context) {
        final textController = TextEditingController(text: memoryText.text);
        return AlertDialog(
          title: const Text("Edit Text"),
          content: TextField(
            controller: textController,
            maxLines: null,
            decoration: const InputDecoration(
              labelText: "Text",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateMemoryText(text: textController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateMemoryText == null) {
      return;
    }
    try {
      await MemoryTextService.instance.updateMemoryText(
        memoryTextId: memoryText.id,
        memoryText: updateMemoryText,
      );
      setState(() {
        final index = memoryTexts.indexWhere((m) => m == memoryText);
        memoryTexts[index] = MemoryText(
          id: memoryText.id,
          text: updateMemoryText.text,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "Text updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteMemoryText(BuildContext context, MemoryText memoryText) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Delete Text"),
          content: const Text("Are you sure you want to delete this text?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await MemoryTextService.instance.deleteMemoryText(memoryTextId: memoryText.id);
      setState(() {
        memoryTexts.remove(memoryText);
      });
      if (context.mounted) {
        showSnackBar(context, "Text deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
