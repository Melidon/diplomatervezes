import "package:file_picker/file_picker.dart";
import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/screens/recording_page.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class RecordingsScreen extends StatefulWidget {
  final String memoryId;

  const RecordingsScreen({
    super.key,
    required this.memoryId,
  });

  @override
  State<RecordingsScreen> createState() => _RecordingsScreenState();
}

class _RecordingsScreenState extends State<RecordingsScreen> {
  late Future<List<RecordingMeta>> recordingsFuture;
  late List<RecordingMeta> recordings;

  @override
  void initState() {
    super.initState();
    recordingsFuture = RecordingService.instance.listRecordings(memoryId: widget.memoryId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createRecording(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: recordingsFuture,
        builder: (context, data) {
          recordings = data;
          return ListView.builder(
            itemCount: recordings.length,
            itemBuilder: (context, index) {
              final recording = recordings[index];
              return ListTile(
                title: Text(recording.name),
                trailing: EditAndDeleteMenu(
                  item: recording,
                  onEdit: editRecordingMeta,
                  onDelete: deleteRecording,
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RecordingPage(
                        memoryId: widget.memoryId,
                        recording: recording,
                      ),
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  Future<void> createRecording(BuildContext context) async {
    final result = await FilePicker.platform.pickFiles(withData: true);
    if (result == null) {
      return;
    }
    final recording = result.files.single.bytes;
    if (recording == null) {
      return;
    }
    try {
      final recordingMeta = await RecordingService.instance.createRecording(
        memoryId: widget.memoryId,
        recording: recording,
      );
      setState(() {
        recordings.add(recordingMeta);
      });
      if (context.mounted) {
        showSnackBar(context, "${recordingMeta.name} created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editRecordingMeta(BuildContext context, RecordingMeta recordingMeta) async {
    final updateRecordingMeta = await showDialog<UpdateRecordingMeta>(
      context: context,
      builder: (context) {
        final nameController = TextEditingController(text: recordingMeta.name);
        return AlertDialog(
          title: Text("Edit ${recordingMeta.name}"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              labelText: "Name",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateRecordingMeta(name: nameController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateRecordingMeta == null) {
      return;
    }
    try {
      await RecordingService.instance.updateRecording(
        recordingId: recordingMeta.id,
        recordingMeta: updateRecordingMeta,
      );
      setState(() {
        final index = recordings.indexWhere((recording) => recording == recordingMeta);
        recordings[index] = RecordingMeta(
          id: recordingMeta.id,
          name: updateRecordingMeta.name,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "${updateRecordingMeta.name} updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteRecording(BuildContext context, RecordingMeta recordingMeta) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Delete ${recordingMeta.name}"),
          content: Text("Are you sure you want to delete ${recordingMeta.name}?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await RecordingService.instance.deleteRecording(recordingId: recordingMeta.id);
      setState(() {
        recordings.remove(recordingMeta);
      });
      if (context.mounted) {
        showSnackBar(context, "${recordingMeta.name} deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
