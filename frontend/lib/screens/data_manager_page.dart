import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/auth_models.dart";
import "package:reminiscence_hub/screens/entities_screen.dart";
import "package:reminiscence_hub/screens/memories_screen.dart";

class DataManagerPage extends StatefulWidget {
  final RelativeResponse owner;

  const DataManagerPage({super.key, required this.owner});

  @override
  State<DataManagerPage> createState() => _DataManagerPageState();
}

class _DataManagerPageState extends State<DataManagerPage> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.owner.username),
      ),
      bottomNavigationBar: NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.map),
            label: "Memories",
          ),
          NavigationDestination(
            icon: Icon(Icons.person),
            label: "Entities",
          ),
        ],
        selectedIndex: currentPageIndex,
        onDestinationSelected: (index) {
          setState(() {
            currentPageIndex = index;
          });
        },
      ),
      body: [
        MemoriesScreen(ownerId: widget.owner.id),
        EntitiesScreen(ownerId: widget.owner.id),
      ][currentPageIndex],
    );
  }
}
