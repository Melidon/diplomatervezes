import 'package:flutter/material.dart';
import 'package:reminiscence_hub/generated/models/chat_models.dart';
import 'package:reminiscence_hub/screens/chat_page.dart';
import 'package:reminiscence_hub/services/auth.dart';
import 'package:reminiscence_hub/services/chats.dart';
import 'package:reminiscence_hub/utils/safe_future_builder.dart';

class ChatSelectorPage extends StatefulWidget {
  final User user;

  const ChatSelectorPage({
    super.key,
    required this.user,
  });

  @override
  State<ChatSelectorPage> createState() => _ChatSelectorPageState();
}

class _ChatSelectorPageState extends State<ChatSelectorPage> {
  final Future<List<ChatDescription>> chatDescriptionsFuture = ChatsService.instance.listChats();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Chat Selector"),
      ),
      body: SafeFutureBuilder(
        future: chatDescriptionsFuture,
        builder: (context, chatDescriptions) {
          return ListView.builder(
            itemCount: chatDescriptions.length,
            itemBuilder: (context, index) {
              final chatDescription = chatDescriptions[index];
              return ListTile(
                title: Text(chatDescription.name),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChatPage(
                        chatDescription: chatDescription,
                        user: widget.user,
                      ),
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }
}
