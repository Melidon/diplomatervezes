import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/auth_models.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class RelativeManagerPage extends StatefulWidget {
  const RelativeManagerPage({super.key});

  @override
  State<RelativeManagerPage> createState() => _RelativeManagerPageState();
}

class _RelativeManagerPageState extends State<RelativeManagerPage> {
  final relativesFuture = RelationshipService.instance.listRelatives();
  late List<RelativeResponse> relatives;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Relative Manager"),
      ),
      body: SafeFutureBuilder(
        future: relativesFuture,
        builder: (context, data) {
          relatives = data;
          return ListView.builder(
            itemCount: relatives.length,
            itemBuilder: (context, index) {
              final relative = relatives[index];
              return ListTile(
                title: Text(relative.username),
                trailing: IconButton(
                  onPressed: () async {
                    try {
                      await RelationshipService.instance.removeRelative(relativeRequest: RelativeRequest(relativeId: relative.id));
                      setState(() {
                        relatives.remove(relative);
                      });
                      if (context.mounted) {
                        showSnackBar(context, "${relative.username} removed");
                      }
                    } catch (error) {
                      if (context.mounted) {
                        showSnackBar(context, error.toString(), isError: true);
                      }
                    }
                  },
                  icon: const Icon(Icons.delete),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final formKey = GlobalKey<FormState>();
          final idController = TextEditingController();
          final relativeId = await showDialog<String>(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text("Enter relative ID"),
              content: Form(
                key: formKey,
                child: TextFormField(
                  controller: idController,
                  validator: (id) {
                    if (id == null) {
                      return "ID cannot be empty";
                    }
                    final RegExp regex = RegExp(r"^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$", caseSensitive: false);
                    if (!regex.hasMatch(id)) {
                      return "Invalid ID format";
                    }
                    return null;
                  },
                ),
              ),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text("Cancel"),
                ),
                TextButton(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      Navigator.of(context).pop(idController.text);
                    }
                  },
                  child: const Text("Add"),
                ),
              ],
            ),
          );
          if (relativeId == null) {
            return;
          }
          try {
            await RelationshipService.instance.addRelative(relativeRequest: RelativeRequest(relativeId: relativeId));
            // TODO: If backend would return the added relative, it would be possible to add it to the list correctly
            setState(() {
              relatives.add(RelativeResponse(username: relativeId, id: relativeId));
            });
            if (context.mounted) {
              showSnackBar(context, "$relativeId added");
            }
          } catch (error) {
            if (context.mounted) {
              showSnackBar(context, error.toString(), isError: true);
            }
          }
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
