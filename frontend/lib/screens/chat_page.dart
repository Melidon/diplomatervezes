import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:reminiscence_hub/generated/models/chat_models.dart';
import 'package:reminiscence_hub/services/auth.dart';
import 'package:reminiscence_hub/services/backend.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ChatPage extends StatefulWidget {
  final types.User user;
  final ChatDescription chatDescription;

  ChatPage({
    super.key,
    required User user,
    required this.chatDescription,
  }) : user = types.User(id: user.id, firstName: user.name);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  late WebSocketChannel _channel;
  final List<types.Message> _messages = [];

  @override
  void initState() {
    super.initState();
    _connect();
  }

  void _connect() async {
    final baseUrl = Backend.instance.baseUrl.toString().substring(4);
    final path = widget.chatDescription.path;
    final uri = Uri.parse('ws$baseUrl$path?user_id=${widget.user.id}');
    _channel = WebSocketChannel.connect(uri);
    try {
      await _channel.ready;
      _channel.stream.listen(
        (event) {
          try {
            final message = types.Message.fromJson(jsonDecode(event));
            _addMessage(message);
          } catch (error) {
            _addSystemMessage(error);
          }
        },
        onDone: () => _addSystemMessage('Connection closed'),
        onError: (error) => _addSystemMessage(error),
      );
    } catch (error) {
      _addSystemMessage(error);
    }
  }

  void _addMessage(types.Message message) {
    setState(() {
      _messages.insert(0, message);
    });
  }

  void _addSystemMessage(Object? error) {
    const system = types.User(id: '0', firstName: 'System');
    final message = types.TextMessage(
      author: system,
      createdAt: DateTime.now().millisecondsSinceEpoch,
      id: "",
      text: error.toString(),
    );
    _addMessage(message);
  }

  void _handleSendPressed(types.PartialText message) async {
    try {
      _channel.sink.add(jsonEncode(message.toJson()));
    } catch (error) {
      _addSystemMessage(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.chatDescription.name),
      ),
      body: Chat(
        messages: _messages,
        onSendPressed: _handleSendPressed,
        user: widget.user,
      ),
    );
  }

  @override
  void dispose() {
    // TODO: Figure out why this throws an exception
    // _channel.sink.close();
    super.dispose();
  }
}
