import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class EntitiesScreen extends StatefulWidget {
  final String ownerId;

  const EntitiesScreen({super.key, required this.ownerId});

  @override
  State<EntitiesScreen> createState() => _EntitiesScreenState();
}

class _EntitiesScreenState extends State<EntitiesScreen> {
  late Future<List<Entity>> entitiesFuture;
  late List<Entity> entities;

  @override
  void initState() {
    super.initState();
    entitiesFuture = EntityService.instance.listEntities(ownerId: widget.ownerId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createEntity(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: entitiesFuture,
        builder: (context, data) {
          entities = data;
          return ListView.builder(
            itemCount: entities.length,
            itemBuilder: (context, index) {
              final entity = entities[index];
              return ListTile(
                title: Text(entity.name),
                trailing: EditAndDeleteMenu(
                  item: entity,
                  onEdit: editEntity,
                  onDelete: deleteEntity,
                ),
              );
            },
          );
        },
      ),
    );
  }

  Future<void> createEntity(BuildContext context) async {
    final createEntity = await showDialog<CreateEntity>(
      context: context,
      builder: (context) {
        final nameController = TextEditingController();
        return AlertDialog(
          title: const Text("Create Entity"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              labelText: "Name",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(
                  context,
                  CreateEntity(
                    ownerId: widget.ownerId,
                    name: nameController.text,
                  )),
              child: const Text("Create"),
            ),
          ],
        );
      },
    );
    if (createEntity == null) {
      return;
    }
    try {
      final entity = await EntityService.instance.createEntity(
        entity: createEntity,
      );
      setState(() {
        entities.add(entity);
      });
      if (context.mounted) {
        showSnackBar(context, "${entity.name} created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editEntity(BuildContext context, Entity entity) async {
    final updateEntity = await showDialog<UpdateEntity>(
      context: context,
      builder: (context) {
        final nameController = TextEditingController(text: entity.name);
        return AlertDialog(
          title: Text("Edit ${entity.name}"),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(
              labelText: "Name",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateEntity(name: nameController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateEntity == null) {
      return;
    }
    try {
      await EntityService.instance.updateEntity(
        entityId: entity.id,
        entity: updateEntity,
      );
      setState(() {
        final index = entities.indexWhere((e) => e == entity);
        entities[index] = Entity(
          id: entity.id,
          name: updateEntity.name,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "${updateEntity.name} updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteEntity(BuildContext context, Entity entity) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Delete ${entity.name}"),
          content: Text("Are you sure you want to delete ${entity.name}?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await EntityService.instance.deleteEntity(entityId: entity.id);
      setState(() {
        entities.remove(entity);
      });
      if (context.mounted) {
        showSnackBar(context, "${entity.name} deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
