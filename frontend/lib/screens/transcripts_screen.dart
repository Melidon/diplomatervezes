import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class TranscriptsScreen extends StatefulWidget {
  final String recordingId;
  final Widget child;

  const TranscriptsScreen({
    super.key,
    required this.recordingId,
    required this.child,
  });

  @override
  State<TranscriptsScreen> createState() => _TranscriptsScreenState();
}

class _TranscriptsScreenState extends State<TranscriptsScreen> {
  late Future<List<Transcript>> transcriptsFuture;
  late List<Transcript> transcripts;

  @override
  void initState() {
    super.initState();
    transcriptsFuture = TranscriptService.instance.listTranscripts(
      recordingId: widget.recordingId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createTranscript(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: transcriptsFuture,
        builder: (context, data) {
          transcripts = data;
          return Column(
            children: [
              widget.child,
              Expanded(
                child: ListView.builder(
                  itemCount: transcripts.length,
                  itemBuilder: (context, index) {
                    final transcript = transcripts[index];
                    return ListTile(
                      leading: Tooltip(
                        message: transcript.revised ? 'Revised' : 'Not Revised',
                        child: transcript.revised
                            ? const Icon(
                                Icons.check,
                                color: Colors.green,
                              )
                            : const Icon(
                                Icons.warning,
                                color: Colors.orange,
                              ),
                      ),
                      title: Text(transcript.text), // TODO: Only show first 50 characters
                      trailing: EditAndDeleteMenu(
                        item: transcript,
                        onEdit: editTranscript,
                        onDelete: deleteTranscript,
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Future<void> createTranscript(BuildContext context) async {
    final createTranscript = await showDialog<CreateTranscript>(
      context: context,
      builder: (context) {
        final transcriptController = TextEditingController();
        return AlertDialog(
          title: const Text("Create Transcript"),
          content: TextField(
            controller: transcriptController,
            maxLines: null,
            decoration: const InputDecoration(
              labelText: "Transcript",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(
                  context,
                  CreateTranscript(
                    recordingId: widget.recordingId,
                    text: transcriptController.text,
                  )),
              child: const Text("Create"),
            ),
          ],
        );
      },
    );
    if (createTranscript == null) {
      return;
    }
    try {
      final transcript = await TranscriptService.instance.createTranscript(transcript: createTranscript);
      setState(() {
        transcripts.add(transcript);
      });
      if (context.mounted) {
        showSnackBar(context, "Transcript created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editTranscript(BuildContext context, Transcript transcript) async {
    final updateTranscript = await showDialog<UpdateTranscript>(
      context: context,
      builder: (context) {
        final transcriptController = TextEditingController(text: transcript.text);
        return AlertDialog(
          title: const Text("Edit Transcript"),
          content: TextField(
            controller: transcriptController,
            maxLines: null,
            decoration: const InputDecoration(
              labelText: "Transcript",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateTranscript(text: transcriptController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateTranscript == null) {
      return;
    }
    try {
      await TranscriptService.instance.updateTranscript(
        transcriptId: transcript.id,
        transcript: updateTranscript,
      );
      setState(() {
        final index = transcripts.indexWhere((t) => t == transcript);
        transcripts[index] = Transcript(
          id: transcript.id,
          text: updateTranscript.text,
          revised: true,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "Transcript updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteTranscript(BuildContext context, Transcript transcript) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Delete Transcript"),
          content: const Text("Are you sure you want to delete this transcript?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await TranscriptService.instance.deleteTranscript(transcriptId: transcript.id);
      setState(() {
        transcripts.remove(transcript);
      });
      if (context.mounted) {
        showSnackBar(context, "Transcript deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
