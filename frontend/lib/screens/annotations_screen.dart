import "dart:typed_data";
import "dart:ui" as ui show Image;

import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/annotated_image.dart";

class AnnotationsScreen extends StatefulWidget {
  final String ownerId;
  final String imageId;
  final Uint8List imageBytes;

  const AnnotationsScreen({
    super.key,
    required this.ownerId,
    required this.imageId,
    required this.imageBytes,
  });

  @override
  State<AnnotationsScreen> createState() => _AnnotationsScreenState();
}

class _AnnotationsScreenState extends State<AnnotationsScreen> {
  late Future<(ui.Image, List<Annotation>, Map<String, Entity>)> combinedFuture;
  late ui.Image image;
  late List<Annotation> annotations;
  late Map<String, Entity> entities;

  @override
  void initState() {
    super.initState();
    combinedFuture = () async {
      final imageFuture = decodeImageFromList(widget.imageBytes);
      final annotationsFuture = AnnotationService.instance.listAnnotations(
        imageId: widget.imageId,
      );
      final entitiesFuture = EntityService.instance.listEntities(ownerId: widget.ownerId);
      final combined = await Future.wait([annotationsFuture, imageFuture, entitiesFuture]);
      final image = combined[1] as ui.Image;
      final annotations = combined[0] as List<Annotation>;
      final entities = Map<String, Entity>.fromIterable((combined[2] as List<Entity>), key: (entity) => entity.id);
      return (image, annotations, entities);
    }();
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: widget.imageId,
      child: FutureBuilder(
        future: combinedFuture,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            image = snapshot.data!.$1;
            annotations = snapshot.data!.$2;
            entities = snapshot.data!.$3;
            return AnnotatedImage(
              image: image,
              annotations: annotations,
              entities: entities,
            );
          } else {
            return Image.memory(widget.imageBytes);
          }
        },
      ),
    );
  }
}
