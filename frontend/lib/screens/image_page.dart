import "dart:typed_data";

import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/screens/annotations_screen.dart";
import "package:reminiscence_hub/screens/image_texts_screen.dart";

class ImagePage extends StatefulWidget {
  final String ownerId;
  final ImageMeta imageMeta;
  final Uint8List imageBytes;

  const ImagePage({
    super.key,
    required this.ownerId,
    required this.imageMeta,
    required this.imageBytes,
  });

  @override
  State<ImagePage> createState() => _ImagePageState();
}

class _ImagePageState extends State<ImagePage> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.imageMeta.name),
      ),
      bottomNavigationBar: NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.comment),
            label: "Annotations",
          ),
          NavigationDestination(
            icon: Icon(Icons.text_snippet),
            label: "Texts",
          ),
        ],
        selectedIndex: currentPageIndex,
        onDestinationSelected: (index) {
          setState(() {
            currentPageIndex = index;
          });
        },
      ),
      body: [
        AnnotationsScreen(
          ownerId: widget.ownerId,
          imageId: widget.imageMeta.id,
          imageBytes: widget.imageBytes,
        ),
        ImageTextsScreen(
          imageId: widget.imageMeta.id,
        ),
      ][currentPageIndex],
    );
  }
}
