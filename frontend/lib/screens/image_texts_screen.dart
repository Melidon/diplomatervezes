import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/edit_and_delete_menu.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class ImageTextsScreen extends StatefulWidget {
  final String imageId;

  const ImageTextsScreen({
    super.key,
    required this.imageId,
  });

  @override
  State<ImageTextsScreen> createState() => _ImageTextsScreenState();
}

class _ImageTextsScreenState extends State<ImageTextsScreen> {
  late Future<List<ImageText>> imageTextsFuture;
  late List<ImageText> imageTexts;

  @override
  void initState() {
    super.initState();
    imageTextsFuture = ImageTextService.instance.listImageTexts(
      imageId: widget.imageId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => createImageText(context),
        child: const Icon(Icons.add),
      ),
      body: SafeFutureBuilder(
        future: imageTextsFuture,
        builder: (context, data) {
          imageTexts = data;
          return ListView.builder(
            itemCount: imageTexts.length,
            itemBuilder: (context, index) {
              final text = imageTexts[index];
              return ListTile(
                title: Text(text.text), // TODO: Only show first 50 characters
                trailing: EditAndDeleteMenu(
                  item: text,
                  onEdit: editImageText,
                  onDelete: deleteImageText,
                ),
              );
            },
          );
        },
      ),
    );
  }

  Future<void> createImageText(BuildContext context) async {
    final createImageText = await showDialog<CreateImageText>(
      context: context,
      builder: (context) {
        final textController = TextEditingController();
        return AlertDialog(
          title: const Text("Create Text"),
          content: TextField(
            controller: textController,
            maxLines: null,
            decoration: const InputDecoration(
              labelText: "Text",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(
                  context,
                  CreateImageText(
                    imageId: widget.imageId,
                    text: textController.text,
                  )),
              child: const Text("Create"),
            ),
          ],
        );
      },
    );
    if (createImageText == null) {
      return;
    }
    try {
      final imageText = await ImageTextService.instance.createImageText(imageText: createImageText);
      setState(() {
        imageTexts.add(imageText);
      });
      if (context.mounted) {
        showSnackBar(context, "Text created");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> editImageText(BuildContext context, ImageText imageText) async {
    final updateImageText = await showDialog<UpdateImageText>(
      context: context,
      builder: (context) {
        final textController = TextEditingController(text: imageText.text);
        return AlertDialog(
          title: const Text("Edit Text"),
          content: TextField(
            controller: textController,
            maxLines: null,
            decoration: const InputDecoration(
              labelText: "Text",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, UpdateImageText(text: textController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (updateImageText == null) {
      return;
    }
    try {
      await ImageTextService.instance.updateImageText(
        imageTextId: imageText.id,
        imageText: updateImageText,
      );
      setState(() {
        final index = imageTexts.indexWhere((i) => i == imageText);
        imageTexts[index] = ImageText(
          id: imageText.id,
          text: updateImageText.text,
        );
      });
      if (context.mounted) {
        showSnackBar(context, "Text updated");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }

  Future<void> deleteImageText(BuildContext context, ImageText textResponse) async {
    final confirmed = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Delete Text"),
          content: const Text("Are you sure you want to delete this text?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (confirmed != true) {
      return;
    }
    try {
      await ImageTextService.instance.deleteImageText(imageTextId: textResponse.id);
      setState(() {
        imageTexts.remove(textResponse);
      });
      if (context.mounted) {
        showSnackBar(context, "Text deleted");
      }
    } catch (error) {
      if (context.mounted) {
        showSnackBar(context, error.toString(), isError: true);
      }
    }
  }
}
