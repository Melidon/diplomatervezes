import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/auth_models.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/utils/show_snack_bar.dart";

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _formKey = GlobalKey<FormState>();
  bool isSignInMode = true;

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  Role? _selectedRole;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                isSignInMode ? "Sign in" : "Register",
                style: const TextStyle(
                  fontSize: 32.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 16.0),
              Row(
                children: [
                  Text(isSignInMode ? "Don't have an account? " : "Already have an account? "),
                  TextButton(
                    style: ButtonStyle(
                      overlayColor: WidgetStateProperty.all<Color>(Colors.transparent),
                    ),
                    onPressed: () {
                      setState(() {
                        isSignInMode = !isSignInMode;
                      });
                    },
                    child: Text(isSignInMode ? "Register" : "Sign in"),
                  ),
                ],
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                controller: _usernameController,
                decoration: const InputDecoration(
                  labelText: "Username",
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Username is required";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                controller: _passwordController,
                obscureText: true,
                decoration: const InputDecoration(
                  labelText: "Password",
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Password is required";
                  }
                  // TODO: Check password strength
                  return null;
                },
              ),
              if (!isSignInMode) const SizedBox(height: 16.0),
              if (!isSignInMode)
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: "Confirm Password",
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value != _passwordController.text) {
                      return "Passwords do not match";
                    }
                    return null;
                  },
                ),
              if (!isSignInMode) const SizedBox(height: 16.0),
              if (!isSignInMode)
                DropdownButtonFormField<Role>(
                  value: _selectedRole,
                  onChanged: (Role? value) {
                    setState(() {
                      _selectedRole = value;
                    });
                  },
                  items: Role.values.map((role) {
                    return DropdownMenuItem<Role>(
                      value: role,
                      child: Text(role.toString().split(".").last),
                    );
                  }).toList(),
                  decoration: const InputDecoration(
                    labelText: "Role",
                  ),
                  validator: (value) {
                    if (value == null) {
                      return "Role is required";
                    }
                    return null;
                  },
                ),
              const SizedBox(height: 16.0),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      final username = _usernameController.text;
                      final password = _passwordController.text;
                      try {
                        if (isSignInMode) {
                          await Auth.instance.signIn(SignInRequest(
                            username: username,
                            password: password,
                          ));
                        } else {
                          final role = _selectedRole!;
                          await Auth.instance.createUser(SignUpRequest(
                            username: username,
                            password: password,
                            role: role,
                          ));
                        }
                      } catch (error) {
                        if (context.mounted) {
                          showSnackBar(context, error.toString(), isError: true);
                        }
                      }
                    }
                  },
                  child: Text(isSignInMode ? "Sign in" : "Register"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
