import "dart:typed_data";

import "package:audioplayers/audioplayers.dart";
import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/screens/transcripts_screen.dart";
import "package:reminiscence_hub/services/memory_collection.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";

class RecordingPage extends StatefulWidget {
  final String memoryId;
  final RecordingMeta recording;

  const RecordingPage({
    super.key,
    required this.memoryId,
    required this.recording,
  });

  @override
  State<RecordingPage> createState() => _RecordingPageState();
}

class _RecordingPageState extends State<RecordingPage> {
  late Future<Uint8List> recordingFuture;

  @override
  void initState() {
    super.initState();
    recordingFuture = RecordingService.instance.getRecording(recordingId: widget.recording.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.recording.name),
      ),
      body: SafeFutureBuilder(
        future: recordingFuture,
        builder: (context, data) {
          return TranscriptsScreen(
            recordingId: widget.recording.id,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: AudioButton(audioBytes: data),
            ),
          );
        },
      ),
    );
  }
}

class AudioButton extends StatefulWidget {
  final Uint8List audioBytes;

  const AudioButton({
    super.key,
    required this.audioBytes,
  });

  @override
  State<AudioButton> createState() => _AudioButtonState();
}

class _AudioButtonState extends State<AudioButton> {
  final audioPlayer = AudioPlayer();
  Duration maxDuration = Duration.zero;
  Duration currentPosition = Duration.zero;
  PlayerState playerState = PlayerState.stopped;

  @override
  void initState() {
    super.initState();
    audioPlayer.setSource(BytesSource(widget.audioBytes));
    audioPlayer.onPlayerStateChanged.listen((state) {
      setState(() {
        playerState = state;
      });
    });
    audioPlayer.onDurationChanged.listen((duration) {
      setState(() {
        maxDuration = duration;
      });
    });
    audioPlayer.onPositionChanged.listen((position) {
      setState(() {
        currentPosition = position;
      });
    });
  }

  static String formatDuration(Duration duration, [bool showHours = false]) {
    var result = duration.toString().split(".").first;
    if (!showHours) {
      result = result.split(":").skip(1).join(":");
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final showHours = maxDuration.inHours > 0;
    return SizedBox(
      width: 300,
      child: ElevatedButton(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(playerState == PlayerState.playing ? Icons.pause : Icons.play_arrow),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: 100,
                child: LinearProgressIndicator(
                  value: maxDuration.inMilliseconds == 0 ? 0 : currentPosition.inMilliseconds / maxDuration.inMilliseconds,
                ),
              ),
            ),
            Text("${formatDuration(currentPosition, showHours)} / ${formatDuration(maxDuration, showHours)}"),
          ],
        ),
        onPressed: () {
          if (audioPlayer.state == PlayerState.playing) {
            audioPlayer.pause();
          } else {
            audioPlayer.resume();
          }
        },
      ),
    );
  }
}
