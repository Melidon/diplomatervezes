import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";
import "package:reminiscence_hub/screens/images_screen.dart";
import "package:reminiscence_hub/screens/memory_texts_screen.dart";
import "package:reminiscence_hub/screens/recordings_screen.dart";

class MemoryPage extends StatefulWidget {
  final String ownerId;
  final Memory memory;

  const MemoryPage({
    super.key,
    required this.ownerId,
    required this.memory,
  });

  @override
  State<MemoryPage> createState() => _MemoryPageState();
}

class _MemoryPageState extends State<MemoryPage> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.memory.name),
      ),
      bottomNavigationBar: NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.photo_album),
            label: "Images",
          ),
          NavigationDestination(
            icon: Icon(Icons.mic),
            label: "Recordings",
          ),
          NavigationDestination(
            icon: Icon(Icons.text_snippet),
            label: "Texts",
          ),
        ],
        selectedIndex: currentPageIndex,
        onDestinationSelected: (index) {
          setState(() {
            currentPageIndex = index;
          });
        },
      ),
      body: [
        ImagesScreen(
          ownerId: widget.ownerId,
          memoryId: widget.memory.id,
        ),
        RecordingsScreen(memoryId: widget.memory.id),
        MemoryTextsScreen(memoryId: widget.memory.id),
      ][currentPageIndex],
    );
  }
}
