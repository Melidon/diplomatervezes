import "package:flutter/material.dart";
import "package:reminiscence_hub/screens/data_manager_page.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/utils/safe_future_builder.dart";

class RelativeHomePage extends StatefulWidget {
  const RelativeHomePage({super.key});

  @override
  State<RelativeHomePage> createState() => _RelativeHomePageState();
}

class _RelativeHomePageState extends State<RelativeHomePage> {
  final elderlyListFuture = RelationshipService.instance.listRelatives();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Reminiscence Hub"),
        actions: [
          IconButton(
            onPressed: Auth.instance.signOut,
            tooltip: "Sign out",
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: SafeFutureBuilder(
        future: elderlyListFuture,
        builder: (context, data) {
          final elderlyList = data;
          return ListView.builder(
            itemCount: elderlyList.length,
            itemBuilder: (context, index) {
              final elderly = elderlyList[index];
              return ListTile(
                title: Text(elderly.username),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DataManagerPage(owner: elderly),
                  ));
                },
              );
            },
          );
        },
      ),
    );
  }
}
