import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/auth_models.dart";
import "package:reminiscence_hub/screens/chat_selector_page.dart";
import "package:reminiscence_hub/screens/data_manager_page.dart";
import "package:reminiscence_hub/screens/relative_manager_page.dart";
import "package:reminiscence_hub/services/auth.dart";

class ElderlyHomePage extends StatelessWidget {
  final User user;

  const ElderlyHomePage({
    super.key,
    required this.user,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Reminiscence Hub"),
        actions: [
          IconButton(
            onPressed: Auth.instance.signOut,
            tooltip: "Sign out",
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => DataManagerPage(
                    owner: RelativeResponse(
                      username: user.name,
                      id: user.id,
                    ),
                  ),
                ));
              },
              child: const Text("Manage my data"),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const RelativeManagerPage(),
                ));
              },
              child: const Text("Manage relatives"),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ChatSelectorPage(user: user),
                ));
              },
              child: const Text("Chat with AI"),
            ),
          ],
        ),
      ),
    );
  }
}
