import "package:flutter/material.dart";

typedef Callback<T> = void Function(BuildContext context, T item);

class EditAndDeleteMenu<T> extends StatelessWidget {
  final T item;
  final Callback<T> onEdit;
  final Callback<T> onDelete;

  const EditAndDeleteMenu({
    super.key,
    required this.item,
    required this.onEdit,
    required this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    return MenuAnchor(
      builder: (context, controller, child) => IconButton(
        onPressed: () {
          if (controller.isOpen) {
            controller.close();
          } else {
            controller.open();
          }
        },
        icon: const Icon(Icons.more_vert),
      ),
      menuChildren: [
        MenuItemButton(
          child: const Row(
            children: [
              Icon(Icons.edit),
              SizedBox(width: 8),
              Text("Edit"),
            ],
          ),
          onPressed: () => onEdit(context, item),
        ),
        MenuItemButton(
          child: const Row(
            children: [
              Icon(Icons.delete),
              SizedBox(width: 8),
              Text("Delete"),
            ],
          ),
          onPressed: () => onDelete(context, item),
        ),
      ],
    );
  }
}
