class Cache<K, V> {
  final _cache = <K, V>{};

  Future<V> getOrLoad(K key, Future<V> Function() load) async {
    if (_cache.containsKey(key)) {
      return _cache[key]!;
    }
    final value = await load();
    _cache[key] = value;
    return value;
  }
}
