import "package:flutter/material.dart";

typedef WidgetBuilder<T> = Widget Function(BuildContext context, T data);
typedef ErrorWidgetBuilder = Widget Function(BuildContext context, Object? error);
typedef LoadingWidgetBuilder = Widget Function(BuildContext context);

class SafeFutureBuilder<T> extends StatelessWidget {
  final Future<T> future;
  final WidgetBuilder<T> builder;
  final ErrorWidgetBuilder? errorBuilder;
  final LoadingWidgetBuilder? loadingBuilder;

  const SafeFutureBuilder({
    super.key,
    required this.future,
    required this.builder,
    this.errorBuilder,
    this.loadingBuilder,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return builder(context, snapshot.data as T);
        }
        if (snapshot.hasError) {
          if (errorBuilder != null) {
            return errorBuilder!(context, snapshot.error);
          }
          return Center(
            child: SelectableText(
              snapshot.error.toString(),
              style: TextStyle(
                color: Theme.of(context).colorScheme.error,
              ),
            ),
          );
        }
        if (loadingBuilder != null) {
          return loadingBuilder!(context);
        }
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
