import "dart:ui" as ui show Image;
import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/models/memory_collection_models.dart";

class AnnotatedImage extends StatelessWidget {
  const AnnotatedImage({
    super.key,
    required this.image,
    required this.annotations,
    required this.entities,
  });

  final ui.Image image;
  final List<Annotation> annotations;
  final Map<String, Entity> entities;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: SizedBox(
        width: image.width.toDouble(),
        height: image.height.toDouble(),
        child: CustomPaint(
          painter: _AnnotatedImagePainter(
            image: image,
            annotations: annotations,
            entities: entities,
          ),
        ),
      ),
    );
  }
}

class _AnnotatedImagePainter extends CustomPainter {
  const _AnnotatedImagePainter({
    required this.image,
    required this.annotations,
    required this.entities,
  });

  final ui.Image image;
  final List<Annotation> annotations;
  final Map<String, Entity> entities;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawImage(image, Offset.zero, Paint());
    final paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8;
    for (final annotation in annotations) {
      canvas.drawRect(
        Rect.fromLTWH(
          annotation.left.toDouble(),
          annotation.top.toDouble(),
          annotation.width.toDouble(),
          annotation.height.toDouble(),
        ),
        paint,
      );
      final textPainter = TextPainter(
        text: TextSpan(
          text: entities[annotation.entityId]!.name,
          style: TextStyle(
            color: Colors.red,
            fontSize: annotation.width / 2,
          ),
        ),
        textDirection: TextDirection.ltr,
      );
      textPainter.layout();
      textPainter.paint(
        canvas,
        Offset(
          annotation.left - (textPainter.width - annotation.width) / 2,
          (annotation.top + annotation.height).toDouble(),
        ),
      );
    }
  }

  @override
  bool shouldRepaint(_AnnotatedImagePainter oldDelegate) {
    return oldDelegate.image != image || oldDelegate.annotations != annotations || oldDelegate.entities != entities;
  }
}
