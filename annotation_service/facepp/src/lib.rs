use {
    reqwest::{
        multipart::{Form, Part},
        Client, RequestBuilder, Response, StatusCode, Url,
    },
    serde::Deserialize,
    std::{borrow::Cow, future::Future, sync::LazyLock},
    tokio::{
        sync::Mutex,
        time::{sleep_until, Duration, Instant},
    },
};

static LAST_CALL: LazyLock<Mutex<Instant>> = LazyLock::new(|| Mutex::new(Instant::now()));

#[derive(Clone)]
pub struct FacePlusPlus {
    client: Client,
    api_key: String,
    api_secret: String,
}

impl FacePlusPlus {
    pub fn new(api_key: impl Into<String>, api_secret: impl Into<String>) -> Self {
        Self {
            client: Client::new(),
            api_key: api_key.into(),
            api_secret: api_secret.into(),
        }
    }

    fn post(&self, url: impl AsRef<str>) -> RequestBuilder {
        let url = Url::parse("https://api-us.faceplusplus.com/facepp/v3/")
            .expect("Failed to parse URL")
            .join(url.as_ref())
            .expect("Failed to join URL");
        let params = [
            ("api_key", self.api_key.as_str()),
            ("api_secret", self.api_secret.as_str()),
        ];
        self.client.post(url).query(&params)
    }

    pub fn detect(
        &self,
        image_file: impl Into<Cow<'static, [u8]>>,
    ) -> impl Future<Output = Result<DetectResponse, Error>> {
        self.post("detect")
            .multipart(Form::new().part(
                "image_file",
                Part::bytes(image_file).file_name("image_file"),
            ))
            .send_and_deserialize()
    }

    pub fn search(
        &self,
        face_token: impl AsRef<str>,
        outer_id: impl AsRef<str>,
    ) -> impl Future<Output = Result<SearchResponse, Error>> {
        self.post("search")
            .query(&[
                ("face_token", face_token.as_ref()),
                ("outer_id", outer_id.as_ref()),
            ])
            .send_and_deserialize()
    }

    pub fn faceset_getfacesets(
        &self,
        start: Option<usize>,
    ) -> impl Future<Output = Result<GetFaceSetsResponse, Error>> {
        let mut request = self.post("faceset/getfacesets");
        if let Some(start) = start {
            request = request.query(&[("start", start.to_string())]);
        }
        request.send_and_deserialize()
    }

    pub fn faceset_delete(
        &self,
        outer_id: impl AsRef<str>,
    ) -> impl Future<Output = Result<DeleteResponse, Error>> {
        self.post("faceset/delete")
            .query(&[("outer_id", outer_id.as_ref()), ("check_empty", "0")])
            .send_and_deserialize()
    }

    pub fn faceset_getdetail(
        &self,
        outer_id: impl AsRef<str>,
    ) -> impl Future<Output = Result<GetDetailResponse, Error>> {
        self.post("faceset/getdetail")
            .query(&[("outer_id", outer_id.as_ref())])
            .send_and_deserialize()
    }

    pub fn faceset_removeface(
        &self,
        outer_id: impl AsRef<str>,
        face_token: impl AsRef<str>,
    ) -> impl Future<Output = Result<RemoveFaceResponse, Error>> {
        self.post("faceset/removeface")
            .query(&[
                ("outer_id", outer_id.as_ref()),
                ("face_tokens", face_token.as_ref()),
            ])
            .send_and_deserialize()
    }

    pub fn faceset_addface<'a>(
        &self,
        outer_id: impl AsRef<str>,
        face_tokens: impl Iterator<Item = &'a str>,
    ) -> impl Future<Output = Result<AddFaceResponse, Error>> {
        let face_tokens = face_tokens.collect::<Vec<_>>().join(",");
        self.post("faceset/addface")
            .query(&[
                ("outer_id", outer_id.as_ref()),
                ("face_tokens", face_tokens.as_str()),
            ])
            .send_and_deserialize()
    }

    pub fn faceset_create(
        &self,
        outer_id: impl AsRef<str>,
    ) -> impl Future<Output = Result<CreateResponse, Error>> {
        self.post("faceset/create")
            .query(&[("outer_id", outer_id.as_ref())])
            .send_and_deserialize()
    }
}

trait SendAndDeserialize<T>: Sized {
    type Error;

    async fn send_and_deserialize(self) -> Result<T, Self::Error>;
}

impl<T: for<'a> Deserialize<'a>> SendAndDeserialize<T> for RequestBuilder {
    type Error = Error;

    async fn send_and_deserialize(self) -> Result<T, Self::Error> {
        let mut last_call_guard = LAST_CALL.lock().await;
        sleep_until(*last_call_guard + Duration::from_secs(1)).await;
        let result = self.send().await;
        *last_call_guard = Instant::now();
        result?.try_into_async().await
    }
}

trait TryIntoAsync<T>: Sized {
    type Error;

    fn try_into_async(self) -> impl Future<Output = Result<T, Self::Error>>;
}

impl<T: for<'a> Deserialize<'a>> TryIntoAsync<T> for Response {
    type Error = Error;

    async fn try_into_async(self) -> Result<T, Self::Error> {
        let status = self.status();
        let response_text = self.text().await?;
        match status {
            StatusCode::OK => Ok(serde_json::from_str(response_text.as_str())?),
            _ => Err(Error::Other(serde_json::from_str(response_text.as_str())?)),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct DetectResponse {
    pub request_id: String,
    pub time_used: u64,
    pub faces: Vec<Face>,
    pub image_id: String,
    pub face_num: usize,
}

#[derive(Debug, Deserialize)]
pub struct SearchResponse {
    pub request_id: String,
    pub time_used: u64,
    pub thresholds: Thresholds,
    pub results: Vec<SearchResult>,
}

#[derive(Debug, Deserialize)]
pub struct Thresholds {
    #[serde(rename = "1e-3")]
    pub _1e_3: f64,
    #[serde(rename = "1e-4")]
    pub _1e_4: f64,
    #[serde(rename = "1e-5")]
    pub _1e_5: f64,
}

#[derive(Debug, Deserialize)]
pub struct SearchResult {
    pub confidence: f64,
    pub user_id: String,
    pub face_token: String,
}

#[derive(Debug, Deserialize)]
pub struct Face {
    pub face_token: String,
    pub face_rectangle: FaceRectangle,
}

#[derive(Debug, Deserialize)]
pub struct FaceRectangle {
    pub left: i32,
    pub top: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(Debug, Deserialize)]
pub struct GetFaceSetsResponse {
    pub request_id: String,
    pub facesets: Vec<FaceSet>,
    pub next: Option<usize>,
    pub time_used: u64,
}

#[derive(Debug, Deserialize)]
pub struct FaceSet {
    pub faceset_token: String,
    pub outer_id: String,
    pub display_name: String,
    pub tags: String,
}

#[derive(Debug, Deserialize)]
pub struct DeleteResponse {
    pub time_used: u64,
    pub faceset_token: String,
    pub outer_id: String,
    pub request_id: String,
}

#[derive(Debug, Deserialize)]
pub struct GetDetailResponse {
    pub faceset_token: String,
    pub display_name: String,
    pub face_tokens: Vec<String>,
    pub time_used: u64,
    pub tags: String,
    pub user_data: String,
    pub face_count: usize,
    pub request_id: String,
    pub outer_id: String,
}

#[derive(Debug, Deserialize)]
pub struct RemoveFaceResponse {
    pub faceset_token: String,
    pub face_removed: usize,
    pub time_used: u64,
    pub face_count: usize,
    pub request_id: String,
    pub outer_id: String,
    pub failure_detail: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct AddFaceResponse {
    pub faceset_token: String,
    pub time_used: u64,
    pub face_count: usize,
    pub face_added: usize,
    pub request_id: String,
    pub outer_id: String,
    pub failure_detail: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct CreateResponse {
    pub faceset_token: String,
    pub time_used: u64,
    pub face_count: usize,
    pub face_added: usize,
    pub request_id: String,
    pub outer_id: String,
    pub failure_detail: Vec<FailureDetail>,
}

#[derive(Debug, Deserialize)]
pub struct FailureDetail {
    pub reason: String,
    pub face_token: String,
}

#[derive(Debug, Deserialize)]
pub struct OtherError {
    pub error_message: String,
}

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    Serde(serde_json::Error),
    Other(OtherError),
}

impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Self::Reqwest(error)
    }
}

impl From<serde_json::Error> for Error {
    fn from(error: serde_json::Error) -> Self {
        Self::Serde(error)
    }
}
