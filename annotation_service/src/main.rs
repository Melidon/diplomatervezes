use {
    amqprs::{
        callbacks::{DefaultChannelCallback, DefaultConnectionCallback},
        channel::Channel,
        connection::Connection,
    },
    annotation_service::{
        consumers::{ImageCreatedConsumer, UserCreatedConsumer, UserDeletedConsumer},
        utils::{
            amqp::Listener,
            exchanges::{
                AuthExchanges::{UserCreated, UserDeleted},
                MemoryCollectionExchanges::ImageCreated,
            },
        },
    },
    facepp::FacePlusPlus,
    sea_orm::{Database, DatabaseConnection, Iden},
    std::env,
    tokio::time::{sleep, Duration},
};

fn get_facepp_client() -> FacePlusPlus {
    let api_key = env::var("FACEPP_API_KEY").expect("Failed to fetch FACEPP_API_KEY");
    let api_secret = env::var("FACEPP_API_SECRET").expect("Failed to fetch FACEPP_API_SECRET");
    FacePlusPlus::new(api_key, api_secret)
}

async fn connect_to_database() -> DatabaseConnection {
    let database_url = env::var("DATABASE_URL").expect("Failed to fetch DATABASE_URL");
    Database::connect(&database_url)
        .await
        .expect("Failed to connect to database!")
}

async fn connect_to_message_queue() -> (Connection, Channel) {
    let rabbitmq_uri = env::var("RABBITMQ_URI").expect("Failed to fetch RABBITMQ_URI");
    let connection_arguments = rabbitmq_uri
        .as_str()
        .try_into()
        .expect("RABBITMQ_URI is invalid!");
    let connection = Connection::open(&connection_arguments)
        .await
        .expect("Failed to connect to RabbitMQ!");
    connection
        .register_callback(DefaultConnectionCallback)
        .await
        .expect("Failed to register connection callback!");

    let channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");

    (connection, channel)
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();
    let facepp = get_facepp_client();
    let db = connect_to_database().await;
    // DO NOT RENAME `connection` TO `_connection`!!!
    // It will cause the connection to be dropped.
    // Thus it will be closed and channels won't work.
    let (connection, channel) = connect_to_message_queue().await;
    channel
        .on(
            UserCreated.to_string(),
            UserCreatedConsumer::new(facepp.clone()),
        )
        .await;
    channel
        .on(
            UserDeleted.to_string(),
            UserDeletedConsumer::new(facepp.clone()),
        )
        .await;
    channel
        .on(
            ImageCreated.to_string(),
            ImageCreatedConsumer::new(db, facepp),
        )
        .await;
    sleep(Duration::from_secs(u64::MAX)).await;
}
