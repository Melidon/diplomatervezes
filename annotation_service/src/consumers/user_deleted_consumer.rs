use {
    crate::utils::annotation_error::AnnotationError,
    amqprs::{channel::Channel, consumer::AsyncConsumer, BasicProperties, Deliver},
    async_trait::async_trait,
    facepp::FacePlusPlus,
    tracing::error,
    uuid::Uuid,
};

pub struct UserDeletedConsumer {
    facepp: FacePlusPlus,
}

impl UserDeletedConsumer {
    pub fn new(facepp: FacePlusPlus) -> Self {
        Self { facepp }
    }
}

#[async_trait]
impl AsyncConsumer for UserDeletedConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        content: Vec<u8>,
    ) {
        if let Err(error) = consume(&self.facepp, content).await {
            error!("{:?}", error);
        }
    }
}

async fn consume(facepp: &FacePlusPlus, content: Vec<u8>) -> Result<(), AnnotationError> {
    let user_id = Uuid::try_from(content).map_err(AnnotationError::Uuid)?;
    facepp
        .faceset_delete(user_id.to_string())
        .await
        .map_err(AnnotationError::FacePlusPlus)?;
    Ok(())
}
