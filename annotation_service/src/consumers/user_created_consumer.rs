use {
    crate::utils::annotation_error::AnnotationError,
    amqprs::{channel::Channel, consumer::AsyncConsumer, BasicProperties, Deliver},
    async_trait::async_trait,
    facepp::FacePlusPlus,
    tracing::error,
    uuid::Uuid,
};

pub struct UserCreatedConsumer {
    facepp: FacePlusPlus,
}

impl UserCreatedConsumer {
    pub fn new(facepp: FacePlusPlus) -> Self {
        Self { facepp }
    }
}

#[async_trait]
impl AsyncConsumer for UserCreatedConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        content: Vec<u8>,
    ) {
        if let Err(error) = consume(&self.facepp, content).await {
            error!("{:?}", error);
        }
    }
}

async fn consume(facepp: &FacePlusPlus, content: Vec<u8>) -> Result<(), AnnotationError> {
    let user_id = Uuid::try_from(content).map_err(AnnotationError::Uuid)?;
    println!("Creating face set for user: {:?}", user_id);
    facepp
        .faceset_create(user_id.to_string())
        .await
        .map_err(AnnotationError::FacePlusPlus)?;
    Ok(())
}
