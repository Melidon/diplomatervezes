use {
    crate::{
        entities::{
            annotation::{self, ActiveModel as Annotation, Entity as AnnotationTable},
            entity::{ActiveModel as Entity, Entity as EntityTable},
            image::{self, Entity as ImageTable},
            memory,
        },
        utils::{annotation_error::AnnotationError, async_extra::OptionAsyncExtra},
    },
    amqprs::{channel::Channel, consumer::AsyncConsumer, BasicProperties, Deliver},
    async_trait::async_trait,
    facepp::{Face, FacePlusPlus},
    sea_orm::{
        ColumnTrait, DatabaseConnection, EntityTrait, JoinType, QueryFilter, QuerySelect,
        RelationTrait, Set,
    },
    std::{option::Option, result::Result, vec::Vec},
    tracing::{error, info},
    uuid::Uuid,
};

pub struct ImageCreatedConsumer {
    db: DatabaseConnection,
    facepp: FacePlusPlus,
}

#[async_trait]
impl AsyncConsumer for ImageCreatedConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        content: Vec<u8>,
    ) {
        if let Err(error) = async {
            let image_id = Uuid::try_from(content)?;
            self.annotate_image(image_id).await
        }
        .await
        {
            error!("{:?}", error);
        }
    }
}

impl ImageCreatedConsumer {
    pub fn new(db: DatabaseConnection, facepp: FacePlusPlus) -> Self {
        Self { db, facepp }
    }

    #[inline(always)]
    async fn annotate_image(&self, image_id: Uuid) -> Result<(), AnnotationError> {
        info!("Annotating image: {:?}", image_id);
        let (image, user_id) = get_image_and_user_id(&self.db, image_id)
            .await?
            .ok_or(AnnotationError::NotFound)?;
        let detect_response = self.facepp.detect(image).await?;
        let user_id_string = user_id.to_string();
        let user_id_str = user_id_string.as_str();
        for face in detect_response.faces.iter() {
            self.annotate_face(image_id, user_id, user_id_str, face)
                .await?;
        }
        let face_tokens = detect_response
            .faces
            .iter()
            .map(|face| face.face_token.as_str());
        self.facepp
            .faceset_addface(user_id_str, face_tokens)
            .await?;
        Ok(())
    }

    #[inline(always)]
    async fn annotate_face(
        &self,
        image_id: Uuid,
        user_id: Uuid,
        user_id_str: &str,
        face: &Face,
    ) -> Result<(), AnnotationError> {
        info!("Annotating face: {:?}", face);
        let most_similar_face_token =
            get_most_similar_face_token(&self.facepp, face.face_token.as_str(), user_id_str)
                .await?;
        let entity_id = most_similar_face_token
            .and_then_async_result(|face_token| find_entity_for_face_token(&self.db, face_token))
            .await?
            .unwrap_or_else_async_result(|| create_unknown_entity(&self.db, user_id))
            .await?;
        crete_annotation(&self.db, image_id, entity_id, face).await?;
        Ok(())
    }
}

#[inline(always)]
async fn get_image_and_user_id(
    db: &DatabaseConnection,
    image_id: Uuid,
) -> Result<Option<(Vec<u8>, Uuid)>, sea_orm::DbErr> {
    ImageTable::find_by_id(image_id)
        .join(JoinType::InnerJoin, image::Relation::Memory.def())
        .select_only()
        .column(image::Column::Bytes)
        .column(memory::Column::OwnerId)
        .into_tuple::<(Vec<u8>, Uuid)>()
        .one(db)
        .await
}

#[inline(always)]
async fn get_most_similar_face_token(
    facepp: &FacePlusPlus,
    face_token: impl AsRef<str>,
    outer_id: impl AsRef<str>,
) -> Result<Option<String>, facepp::Error> {
    let search_result = facepp.search(face_token, outer_id).await;
    let most_similar_face_token = match search_result {
        Ok(search_response) => Ok(search_response
            .results
            .into_iter()
            .next()
            .map(|result| result.face_token)),
        Err(facepp::Error::Other(facepp::OtherError { error_message }))
            if error_message == "EMPTY_FACESET" =>
        {
            Ok(None)
        }
        Err(error) => Err(error),
    }?;
    Ok(most_similar_face_token)
}

#[inline(always)]
async fn find_entity_for_face_token(
    db: &DatabaseConnection,
    face_token: impl AsRef<str>,
) -> Result<Option<Uuid>, sea_orm::DbErr> {
    AnnotationTable::find()
        .filter(annotation::Column::FacesetToken.eq(face_token.as_ref()))
        .select_only()
        .column(annotation::Column::EntityId)
        .into_tuple::<Uuid>()
        .one(db)
        .await
}

#[inline(always)]
async fn create_unknown_entity(
    db: &DatabaseConnection,
    user_id: Uuid,
) -> Result<Uuid, sea_orm::DbErr> {
    let entity = Entity {
        id: Set(Uuid::new_v4()),
        owner_id: Set(user_id),
        name: Set("Unknown".to_owned()),
    };
    let entity = EntityTable::insert(entity).exec_with_returning(db).await?;
    Ok(entity.id)
}

#[inline(always)]
async fn crete_annotation(
    db: &DatabaseConnection,
    image_id: Uuid,
    entity_id: Uuid,
    face: &Face,
) -> Result<(), sea_orm::DbErr> {
    let annotation = Annotation {
        id: Set(Uuid::new_v4()),
        image_id: Set(image_id),
        entity_id: Set(entity_id),
        faceset_token: Set(Some(face.face_token.clone())),
        revised: Set(false),
        left: Set(face.face_rectangle.left),
        top: Set(face.face_rectangle.top),
        width: Set(face.face_rectangle.width),
        height: Set(face.face_rectangle.height),
    };
    AnnotationTable::insert(annotation).exec(db).await?;
    Ok(())
}
