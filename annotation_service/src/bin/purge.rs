use {facepp::FacePlusPlus, std::env, tracing::info};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();
    let api_key = env::var("FACEPP_API_KEY").expect("Failed to fetch FACEPP_API_KEY");
    let api_secret = env::var("FACEPP_API_SECRET").expect("Failed to fetch FACEPP_API_SECRET");
    let facepp = FacePlusPlus::new(api_key, api_secret);
    let mut facesets = Vec::new();
    let mut start = Some(1);
    while start.is_some() {
        let response = facepp.faceset_getfacesets(start).await.unwrap();
        facesets.extend(response.facesets);
        start = response.next;
    }
    for faceset in facesets {
        info!("Deleting faceset: {}", faceset.outer_id);
        facepp.faceset_delete(faceset.outer_id).await.unwrap();
    }
}
