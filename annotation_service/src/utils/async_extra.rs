use std::future::Future;

pub(crate) trait OptionAsyncExtra<T> {
    async fn unwrap_or_else_async_result<F, Fut, E>(self, f: F) -> Result<T, E>
    where
        F: Send + FnOnce() -> Fut,
        Fut: Send + Future<Output = Result<T, E>>,
        Result<T, E>: Send;

    async fn and_then_async_result<F, Fut, D, E>(self, f: F) -> Result<Option<D>, E>
    where
        F: Send + FnOnce(T) -> Fut,
        Fut: Send + Future<Output = Result<Option<D>, E>>,
        Result<Option<D>, E>: Send;
}

impl<T> OptionAsyncExtra<T> for Option<T> {
    async fn unwrap_or_else_async_result<F, Fut, E>(self, f: F) -> Result<T, E>
    where
        F: Send + FnOnce() -> Fut,
        Fut: Send + Future<Output = Result<T, E>>,
        Result<T, E>: Send,
    {
        match self {
            Some(x) => Ok(x),
            None => f().await,
        }
    }

    async fn and_then_async_result<F, Fut, D, E>(self, f: F) -> Result<Option<D>, E>
    where
        F: Send + FnOnce(T) -> Fut,
        Fut: Send + Future<Output = Result<Option<D>, E>>,
        Result<Option<D>, E>: Send,
    {
        match self {
            Some(x) => f(x).await,
            None => Ok(None),
        }
    }
}
