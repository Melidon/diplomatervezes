#![allow(dead_code)]

#[derive(Debug)]
pub(crate) enum AnnotationError {
    Uuid(uuid::Error),
    Database(sea_orm::DbErr),
    NotFound,
    FacePlusPlus(facepp::Error),
}

impl From<uuid::Error> for AnnotationError {
    fn from(error: uuid::Error) -> Self {
        Self::Uuid(error)
    }
}

impl From<sea_orm::DbErr> for AnnotationError {
    fn from(error: sea_orm::DbErr) -> Self {
        Self::Database(error)
    }
}

impl From<facepp::Error> for AnnotationError {
    fn from(error: facepp::Error) -> Self {
        Self::FacePlusPlus(error)
    }
}
