use {
    amqprs::{
        channel::{
            BasicConsumeArguments, Channel, ExchangeDeclareArguments, ExchangeType,
            QueueBindArguments, QueueDeclareArguments,
        },
        consumer::AsyncConsumer,
    },
    std::future::Future,
};

pub trait Listener {
    fn on<Consumer>(&self, event: impl AsRef<str>, consumer: Consumer) -> impl Future<Output = ()>
    where
        Consumer: AsyncConsumer + Send + 'static;
}

impl Listener for Channel {
    async fn on<Consumer>(&self, event: impl AsRef<str>, consumer: Consumer)
    where
        Consumer: AsyncConsumer + Send + 'static,
    {
        // Declare exchange
        self.exchange_declare(ExchangeDeclareArguments::of_type(
            event.as_ref(),
            ExchangeType::Fanout,
        ))
        .await
        .expect("Failed to declare exchange!");
        // Declare queue
        let (queue_name, _, _) = self
            .queue_declare(QueueDeclareArguments::exclusive_server_named())
            .await
            .expect("Failed to declare queue!")
            .expect("`no_wait` argument should be `false`");
        // Bind queue to exchange
        self.queue_bind(QueueBindArguments::new(&queue_name, event.as_ref(), ""))
            .await
            .expect("Failed to bind queue!");
        // Start consuming
        self.basic_consume(
            consumer,
            BasicConsumeArguments::new(&queue_name, "")
                .auto_ack(true)
                .finish(),
        )
        .await
        .expect("Failed to start consuming!");
    }
}
