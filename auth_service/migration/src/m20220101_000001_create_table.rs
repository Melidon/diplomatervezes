use sea_orm_migration::{
    prelude::*,
    schema::*,
    sea_orm::{EnumIter, Iterable},
    sea_query::extension::postgres::Type,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                Type::create()
                    .as_enum(RoleEnum)
                    .values(RoleVariants::iter())
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(uuid(User::Id).primary_key())
                    .col(string_uniq(User::Name))
                    .col(string(User::PasswordHash))
                    .col(enumeration(User::Role, RoleEnum, RoleVariants::iter()))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Relationship::Table)
                    .if_not_exists()
                    .col(uuid(Relationship::ElderlyId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-elderly_id")
                            .from(Relationship::Table, Relationship::ElderlyId)
                            .to(User::Table, User::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(uuid(Relationship::RelativeId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-relative_id")
                            .from(Relationship::Table, Relationship::RelativeId)
                            .to(User::Table, User::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .primary_key(
                        Index::create()
                            .name("pk-relationship")
                            .col(Relationship::ElderlyId)
                            .col(Relationship::RelativeId)
                            .primary(),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Relationship::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(User::Table).to_owned())
            .await?;

        manager
            .drop_type(Type::drop().name(RoleEnum).to_owned())
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
struct RoleEnum;

#[derive(DeriveIden, EnumIter)]
enum RoleVariants {
    Elderly,
    Relative,
    Admin,
}

#[derive(DeriveIden)]
pub enum User {
    Table,
    Id,
    Name,
    PasswordHash,
    Role,
}

#[derive(DeriveIden)]
enum Relationship {
    Table,
    ElderlyId,
    RelativeId,
}
