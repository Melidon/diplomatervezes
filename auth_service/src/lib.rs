mod entities;
pub mod routes;

use {
    amqprs::channel::Channel,
    axum::{
        routing::{delete, patch, post},
        Extension, Router,
    },
    routes::{
        add_relative, change_password, delete_account, list_relatives, remove_relative, sign_in,
        sign_up,
    },
    sea_orm::DatabaseConnection,
    tower_http::cors::CorsLayer,
};

pub fn get_app(db: DatabaseConnection, mq: Channel) -> Router {
    Router::new().nest(
        "/api/auth",
        Router::new()
            .route("/sign_up", post(sign_up))
            .route("/sign_in", post(sign_in))
            .route("/change_password", patch(change_password))
            .route("/delete_account", delete(delete_account))
            .route(
                "/relationship",
                post(add_relative)
                    .get(list_relatives)
                    .delete(remove_relative),
            )
            .layer(Extension(db))
            .layer(Extension(mq))
            .layer(CorsLayer::very_permissive()), // TODO: Allow less if possible
    )
}
