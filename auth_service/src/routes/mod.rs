mod auth_body;
mod auth_error;
pub mod auth_exchanges;
mod change_password_payload;
mod claims;
mod keys;
mod relative_body;
mod relative_payload;
mod sign_in_payload;
mod sign_up_payload;

use {
    self::{
        auth_body::AuthBody,
        auth_error::AuthError,
        auth_exchanges::AuthExchanges,
        change_password_payload::ChangePasswordPayload,
        claims::{Claims, Role},
        relative_payload::RelativePayload,
        sign_in_payload::SignInPayload,
        sign_up_payload::SignUpPayload,
    },
    crate::{
        entities::{
            relationship::{self, ActiveModel as Relationship, Entity as RelationshipTable},
            user::{self, ActiveModel as User, Entity as UserTable},
        },
        routes::relative_body::RelativeBody,
    },
    amqprs::{
        channel::{BasicPublishArguments, Channel},
        BasicProperties,
    },
    axum::{http::StatusCode, Extension, Json},
    sea_orm::{
        ColumnTrait, DatabaseConnection, EntityTrait, Iden, JoinType, QueryFilter, QuerySelect,
        RelationTrait, Set,
    },
    uuid::Uuid,
};

pub(crate) async fn sign_up(
    Extension(db): Extension<DatabaseConnection>,
    Extension(mq): Extension<Channel>,
    Json(payload): Json<SignUpPayload>,
) -> Result<(StatusCode, Json<AuthBody>), AuthError> {
    let password_hash = hash_password(payload.password)?;
    let user = User {
        id: Set(Uuid::new_v4()),
        name: Set(payload.username),
        password_hash: Set(password_hash),
        role: Set(payload.role.into()),
    };
    let db_user = UserTable::insert(user)
        .exec_with_returning(&db)
        .await
        .map_err(AuthError::Database)?;
    mq.basic_publish(
        BasicProperties::default(),
        db_user.id.into_bytes().to_vec(),
        BasicPublishArguments::new(&AuthExchanges::UserCreated.to_string(), ""),
    )
    .await
    .map_err(AuthError::MessageQueue)?;
    let jwt = Claims::new(db_user, &db)
        .await
        .map_err(AuthError::Database)?
        .try_into()
        .map_err(AuthError::JsonWebToken)?;
    Ok((StatusCode::CREATED, Json(AuthBody::new(jwt))))
}

pub(crate) async fn sign_in(
    Extension(db): Extension<DatabaseConnection>,
    Json(payload): Json<SignInPayload>,
) -> Result<Json<AuthBody>, AuthError> {
    let db_user = UserTable::find()
        .filter(user::Column::Name.eq(payload.username))
        .one(&db)
        .await
        .map_err(AuthError::Database)?
        .ok_or(AuthError::WrongUsername)?;
    bcrypt::verify(payload.password, &db_user.password_hash)
        .map_err(AuthError::Hash)?
        .then_some(())
        .ok_or(AuthError::WrongPassword)?;
    let jwt = Claims::new(db_user, &db)
        .await
        .map_err(AuthError::Database)?
        .try_into()
        .map_err(AuthError::JsonWebToken)?;
    Ok(Json(AuthBody::new(jwt)))
}

pub(crate) async fn change_password(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
    Json(ChangePasswordPayload { new_password }): Json<ChangePasswordPayload>,
) -> Result<StatusCode, AuthError> {
    let user = User {
        id: Set(claims.user_id),
        password_hash: Set(hash_password(new_password)?),
        ..User::default()
    };
    UserTable::update(user)
        .exec(&db)
        .await
        .map_err(AuthError::Database)?;
    Ok(StatusCode::OK)
}

pub(crate) async fn delete_account(
    Extension(db): Extension<DatabaseConnection>,
    Extension(mq): Extension<Channel>,
    claims: Claims,
) -> Result<StatusCode, AuthError> {
    UserTable::delete_by_id(claims.user_id)
        .exec(&db)
        .await
        .map_err(AuthError::Database)?;
    mq.basic_publish(
        BasicProperties::default(),
        claims.user_id.into_bytes().to_vec(),
        BasicPublishArguments::new(&AuthExchanges::UserDeleted.to_string(), ""),
    )
    .await
    .map_err(AuthError::MessageQueue)?;
    Ok(StatusCode::NO_CONTENT)
}

pub(crate) async fn add_relative(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
    Json(RelativePayload { relative_id }): Json<RelativePayload>,
) -> Result<StatusCode, AuthError> {
    if claims.role != Role::Elderly {
        return Err(AuthError::Forbidden);
    }
    // TODO: Check if relative is belongs to a relative and not to an elderly
    let elderly_id = claims.user_id;
    let relationship = Relationship {
        elderly_id: Set(elderly_id),
        relative_id: Set(relative_id),
    };
    RelationshipTable::insert(relationship)
        .exec_without_returning(&db)
        .await
        .map_err(AuthError::Database)?;
    // TODO: Return a RelativeBody for the newly added relative
    Ok(StatusCode::CREATED)
}

pub(crate) async fn list_relatives(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
) -> Result<Json<Vec<RelativeBody>>, AuthError> {
    let (filter, relation, ids) = match claims.role {
        Role::Elderly => (
            relationship::Column::ElderlyId.eq(claims.user_id),
            relationship::Relation::User1.def(),
            relationship::Column::RelativeId,
        ),
        Role::Relative => (
            relationship::Column::RelativeId.eq(claims.user_id),
            relationship::Relation::User2.def(),
            relationship::Column::ElderlyId,
        ),
        _ => return Err(AuthError::Forbidden),
    };
    let relatives = RelationshipTable::find()
        .filter(filter)
        .join(JoinType::InnerJoin, relation)
        .select_only()
        .column(ids)
        .column(user::Column::Name)
        .into_tuple::<(Uuid, String)>()
        .all(&db)
        .await
        .map_err(AuthError::Database)?
        .into_iter()
        .map(RelativeBody::from)
        .collect();
    Ok(Json(relatives))
}

pub(crate) async fn remove_relative(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
    Json(RelativePayload { relative_id }): Json<RelativePayload>,
) -> Result<StatusCode, AuthError> {
    if claims.role != Role::Elderly {
        return Err(AuthError::Forbidden);
    }
    let elderly_id = claims.user_id;
    RelationshipTable::delete_by_id((elderly_id, relative_id)) // TODO: Check order!
        .exec(&db)
        .await
        .map_err(AuthError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}

fn hash_password(password: String) -> Result<String, AuthError> {
    bcrypt::hash(password, bcrypt::DEFAULT_COST).map_err(AuthError::Hash)
}
