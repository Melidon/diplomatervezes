use {serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(crate) struct RelativeBody {
    pub(super) id: Uuid,
    pub(super) username: String,
}

impl From<(Uuid, String)> for RelativeBody {
    fn from((id, username): (Uuid, String)) -> Self {
        Self { id, username }
    }
}
