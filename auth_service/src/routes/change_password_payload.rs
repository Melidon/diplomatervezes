use serde::Deserialize;

#[derive(Deserialize)]
pub(crate) struct ChangePasswordPayload {
    pub(super) new_password: String,
}
