use serde::Deserialize;

#[derive(Deserialize)]
pub(crate) struct SignInPayload {
    pub(super) username: String,
    pub(super) password: String,
}
