use {
    jsonwebtoken::{DecodingKey, EncodingKey},
    once_cell::sync::Lazy,
    std::env,
};

pub(super) struct Keys {
    pub(super) encoding: EncodingKey,
    pub(super) decoding: DecodingKey,
}

impl Keys {
    fn new(secret: &[u8]) -> Self {
        Self {
            encoding: EncodingKey::from_secret(secret),
            decoding: DecodingKey::from_secret(secret),
        }
    }
}

pub(super) static KEYS: Lazy<Keys> = Lazy::new(|| {
    let secret = env::var("JWT_SECRET").expect("JWT_SECRET must be set");
    Keys::new(secret.as_bytes())
});
