use std::{
    error::Error,
    fmt::{Display, Formatter, Result},
};

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
};

#[derive(Debug)]
pub(crate) enum AuthError {
    Hash(bcrypt::BcryptError),
    Database(sea_orm::DbErr),
    JsonWebToken(jsonwebtoken::errors::Error),
    WrongUsername,
    WrongPassword,
    TokenExtract(axum_extra::typed_header::TypedHeaderRejection),
    ExpiredToken,
    Forbidden,
    MessageQueue(amqprs::error::Error),
}

impl Display for AuthError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            AuthError::Hash(bcrypt_error) => write!(f, "{}", bcrypt_error),
            AuthError::Database(db_err) => write!(f, "{}", db_err),
            AuthError::JsonWebToken(error) => write!(f, "{}", error),
            AuthError::WrongUsername => write!(f, "Wrong username"),
            AuthError::WrongPassword => write!(f, "Wrong password"),
            AuthError::TokenExtract(typed_header_rejection) => {
                write!(f, "{}", typed_header_rejection)
            }
            AuthError::ExpiredToken => write!(f, "Expired token"),
            AuthError::Forbidden => write!(f, "Forbidden"),
            AuthError::MessageQueue(error) => write!(f, "{}", error),
        }
    }
}

impl Error for AuthError {}

impl IntoResponse for AuthError {
    fn into_response(self) -> Response {
        let status = match &self {
            AuthError::Hash(_) => StatusCode::INTERNAL_SERVER_ERROR,
            AuthError::Database(_) => StatusCode::INTERNAL_SERVER_ERROR,
            AuthError::JsonWebToken(_) => StatusCode::INTERNAL_SERVER_ERROR,
            AuthError::WrongUsername => StatusCode::UNAUTHORIZED,
            AuthError::WrongPassword => StatusCode::UNAUTHORIZED,
            AuthError::TokenExtract(_) => StatusCode::UNAUTHORIZED,
            AuthError::ExpiredToken => StatusCode::UNAUTHORIZED,
            AuthError::Forbidden => StatusCode::FORBIDDEN,
            AuthError::MessageQueue(_) => StatusCode::INTERNAL_SERVER_ERROR,
        };
        (status, self.to_string()).into_response()
    }
}
