use {serde::Deserialize, uuid::Uuid};

#[derive(Deserialize)]
pub(crate) struct RelativePayload {
    pub(super) relative_id: Uuid,
}
