use serde::Serialize;

#[derive(Serialize)]
pub(crate) struct AuthBody {
    access_token: String,
    token_type: String,
}

impl AuthBody {
    pub(super) fn new(Jwt(access_token): Jwt) -> Self {
        Self {
            access_token,
            token_type: "Bearer".to_string(),
        }
    }
}

pub(super) struct Jwt(pub(super) String);
