use {
    super::{auth_body::Jwt, auth_error::AuthError, keys::KEYS},
    crate::entities::{
        prelude::Relationship as RelationshipTable,
        relationship::Column::{ElderlyId, RelativeId},
        sea_orm_active_enums::RoleEnum,
        user::Model as User,
    },
    axum::{async_trait, extract::FromRequestParts, http::request::Parts, RequestPartsExt},
    axum_extra::{
        headers::{authorization::Bearer, Authorization},
        TypedHeader,
    },
    chrono::{Duration, Utc},
    jsonwebtoken::{decode, errors::Error, Header, Validation},
    sea_orm::{ColumnTrait, DatabaseConnection, DbErr, EntityTrait, QueryFilter, QuerySelect},
    serde::{Deserialize, Serialize},
    uuid::Uuid,
};

#[derive(Serialize, Deserialize)]
pub(crate) struct Claims {
    exp: i64,
    pub(super) user_id: Uuid,
    name: String,
    pub(super) role: Role,
    elderly_people: Option<Vec<Uuid>>,
}

impl Claims {
    pub(super) async fn new(user: User, db: &DatabaseConnection) -> Result<Self, DbErr> {
        let exp = (Utc::now() + Duration::days(1)).timestamp();
        let user_id = user.id;
        let name = user.name;
        let role = user.role.into();
        let elderly_people = match role {
            Role::Relative => Some(
                RelationshipTable::find()
                    .filter(RelativeId.eq(user_id))
                    .select_only()
                    .column(ElderlyId)
                    .into_tuple::<Uuid>()
                    .all(db)
                    .await?,
            ),
            _ => None,
        };
        Ok(Self {
            exp,
            user_id,
            name,
            role,
            elderly_people,
        })
    }
}

impl TryFrom<Claims> for Jwt {
    type Error = Error;

    fn try_from(claims: Claims) -> Result<Self, Self::Error> {
        let jwt = jsonwebtoken::encode(&Header::default(), &claims, &KEYS.encoding)?;
        Ok(Self(jwt))
    }
}

#[async_trait]
impl<S> FromRequestParts<S> for Claims
where
    S: Send + Sync,
{
    type Rejection = AuthError;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(AuthError::TokenExtract)?;
        let claims = decode::<Claims>(bearer.token(), &KEYS.decoding, &Validation::default())
            .map_err(AuthError::JsonWebToken)?
            .claims;
        if claims.exp < Utc::now().timestamp() {
            return Err(AuthError::ExpiredToken);
        }
        Ok(claims)
    }
}

#[derive(Serialize, Deserialize, PartialEq)]
pub(super) enum Role {
    Relative,
    Elderly,
    Admin,
}

impl From<RoleEnum> for Role {
    fn from(role: RoleEnum) -> Self {
        match role {
            RoleEnum::Relative => Role::Relative,
            RoleEnum::Elderly => Role::Elderly,
            RoleEnum::Admin => Role::Admin,
        }
    }
}
