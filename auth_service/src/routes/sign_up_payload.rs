use {crate::entities::sea_orm_active_enums::RoleEnum, serde::Deserialize};

#[derive(Deserialize)]
pub(crate) struct SignUpPayload {
    pub(super) username: String,
    pub(super) password: String,
    pub(super) role: RolePayload,
}

#[derive(Deserialize)]
pub(crate) enum RolePayload {
    Elderly,
    Relative,
}

impl From<RolePayload> for RoleEnum {
    fn from(role: RolePayload) -> Self {
        match role {
            RolePayload::Elderly => RoleEnum::Elderly,
            RolePayload::Relative => RoleEnum::Relative,
        }
    }
}
