use {
    amqprs::{
        callbacks::{DefaultChannelCallback, DefaultConnectionCallback},
        channel::{Channel, ExchangeDeclareArguments, ExchangeType},
        connection::Connection,
    },
    auth_service::routes::auth_exchanges::AuthExchanges,
    migration::{Migrator, MigratorTrait},
    sea_orm::{Database, DatabaseConnection, Iden, Iterable},
    std::{
        env,
        net::{Ipv4Addr, SocketAddrV4},
    },
    tokio::net::TcpListener,
};

async fn connect_to_database() -> DatabaseConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set!");
    let connection = Database::connect(&database_url)
        .await
        .expect("Failed to connect to database!");
    Migrator::up(&connection, None)
        .await
        .expect("Failed to migrate!");
    connection
}

async fn connect_to_message_queue() -> (Connection, Channel) {
    let rabbitmq_uri = env::var("RABBITMQ_URI").expect("RABBITMQ_URI must be set!");
    let connection_arguments = rabbitmq_uri
        .as_str()
        .try_into()
        .expect("RABBITMQ_URI is invalid!");
    let connection = Connection::open(&connection_arguments)
        .await
        .expect("Failed to connect to RabbitMQ!");
    connection
        .register_callback(DefaultConnectionCallback)
        .await
        .expect("Failed to register connection callback!");
    let channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");
    for auth_exchange in AuthExchanges::iter() {
        channel
            .exchange_declare(ExchangeDeclareArguments::of_type(
                &auth_exchange.to_string(),
                ExchangeType::Fanout,
            ))
            .await
            .expect("Failed to declare exchange!");
    }
    (connection, channel)
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();
    let db = connect_to_database().await;
    // DO NOT RENAME `connection` TO `_connection`!!!
    // It will cause the connection to be dropped.
    // Thus it will be closed and channels won't work.
    #[allow(unused_variables)]
    let (connection, channel) = connect_to_message_queue().await;
    let port = env::var("PORT").map_or(3000, |port| port.parse().expect("PORT is invalid!"));
    let listener = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port))
        .await
        .expect("Could not bind TCP listener!");
    let app = auth_service::get_app(db, channel);
    axum::serve(listener, app).await.unwrap();
}
