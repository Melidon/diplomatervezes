from dotenv import load_dotenv
import os
from datetime import datetime
from pydantic import BaseModel
import requests

load_dotenv()
API_KEY = os.getenv("DG_API_KEY")


class Sentence(BaseModel):
    text: str
    start: float
    end: float


class Paragraph(BaseModel):
    sentences: list[Sentence]
    num_words: int
    start: float
    end: float


class Paragraphs(BaseModel):
    transcript: str
    paragraphs: list[Paragraph]


class Word(BaseModel):
    word: str
    start: float
    end: float
    confidence: float
    punctuated_word: str


class Alternative(BaseModel):
    transcript: str
    confidence: float
    words: list[Word]
    paragraphs: Paragraphs


class Channel(BaseModel):
    alternatives: list[Alternative]


class Results(BaseModel):
    channels: list[Channel]


class Model(BaseModel):
    name: str
    version: str
    arch: str


class Metadata(BaseModel):
    transaction_key: str
    request_id: str
    sha256: str
    created: datetime
    duration: float
    channels: int
    models: list[str]
    model_info: dict[str, Model]


class Transcription(BaseModel):
    metadata: Metadata
    results: Results


def transcribe_audio(audio_file):
    HEADERS = {
        "Authorization": f"Token {API_KEY}",
        "Content-Type": "audio/mp3",
    }

    PARAMS = {
        "model": "nova-2",
        "smart_format": "true",
    }
    with open(audio_file, "rb") as audio:
        response = requests.post(
            "https://api.deepgram.com/v1/listen",
            headers=HEADERS,
            params=PARAMS,
            data=audio,
        )
    return Transcription(**response.json())


if __name__ == "__main__":
    AUDIO_FILE = "test/educator_marcus_audio.mp3"
    transcription = transcribe_audio(AUDIO_FILE)
    print(transcription)
