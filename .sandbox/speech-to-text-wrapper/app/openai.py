from dotenv import load_dotenv
from openai import OpenAI

load_dotenv()

client = OpenAI()

audio_file = open("test/educator_marcus_audio.mp3", "rb")
transcription = client.audio.transcriptions.create(
    model="whisper-1",
    file=audio_file,
)
print(transcription.text)
