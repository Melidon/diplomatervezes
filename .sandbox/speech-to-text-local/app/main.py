from transformers import Wav2Vec2Processor, Wav2Vec2ForCTC
import torchaudio
import torch

PROCESSOR = Wav2Vec2Processor.from_pretrained("facebook/wav2vec2-base-960h")
SAMPLING_RATE = PROCESSOR.feature_extractor.sampling_rate
MODEL = Wav2Vec2ForCTC.from_pretrained("facebook/wav2vec2-base-960h")


def transcribe(audio_file):
    waveform, sampling_rate = torchaudio.load(audio_file)
    if waveform.shape[0] > 1:
        waveform = waveform.mean(dim=0)
    if sampling_rate != SAMPLING_RATE:
        resampler = torchaudio.transforms.Resample(
            orig_freq=sampling_rate,
            new_freq=SAMPLING_RATE,
        )
        waveform = resampler(waveform)
    input_values = PROCESSOR(
        waveform,
        return_tensors="pt",
        sampling_rate=SAMPLING_RATE,
    ).input_values
    with torch.no_grad():
        logits = MODEL(input_values).logits
    predicted_ids = torch.argmax(logits, dim=-1)
    transcription = PROCESSOR.batch_decode(predicted_ids)[0]
    return transcription


if __name__ == "__main__":
    audio_path = "test/educator_marcus_audio.mp3"
    audio_file = open(audio_path, "rb")
    print(transcribe(audio_file))
