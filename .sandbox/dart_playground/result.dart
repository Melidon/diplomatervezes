sealed class Result<T, E> {}

class Ok<T, E> extends Result<T, E> {
  Ok(this.value);
  T value;
}

class Err<T, E> extends Result<T, E> {
  Err(this.value);
  E value;
}

extension Impl<T, E> on Result<T, E> {
  bool is_ok() => this is Ok;

  bool is_ok_and(bool Function(T) f) {
    return switch (this) {
      Err() => false,
      Ok(value: final x) => f(x),
    };
  }

  bool is_err() => !is_ok();

  bool is_err_and(bool Function(E) f) {
    return switch (this) {
      Ok() => false,
      Err(value: final x) => f(x),
    };
  }

  T? ok() {
    return switch (this) {
      Ok(value: final x) => x,
      Err() => null,
    };
  }

  E? err() {
    return switch (this) {
      Ok() => null,
      Err(value: final x) => x,
    };
  }

  Result<U, E> map<U>(U Function(T) f) {
    return switch (this) {
      Ok(value: final t) => Ok(f(t)),
      Err(value: final e) => Err(e),
    };
  }

  U map_or<U>(U defaultValue, U Function(T) f) {
    return switch (this) {
      Ok(value: final t) => f(t),
      Err() => defaultValue,
    };
  }

  U map_or_else<U>(U Function(E) defaultFn, U Function(T) f) {
    return switch (this) {
      Ok(value: final t) => f(t),
      Err(value: final e) => defaultFn(e),
    };
  }

  Result<T, F> map_err<F>(F Function(E) op) {
    return switch (this) {
      Ok(value: final t) => Ok(t),
      Err(value: final e) => Err(op(e)),
    };
  }

  Result<T, E> inspect(void Function(T) f) {
    if (this is Ok) {
      f((this as Ok).value);
    }
    return this;
  }

  Result<T, E> inspect_err(void Function(E) f) {
    if (this is Err) {
      f((this as Err).value);
    }
    return this;
  }

  T expect(String msg) {
    return switch (this) {
      Ok(value: final t) => t,
      Err(value: final e) => unwrap_failed(msg, e),
    };
  }

  T unwrap() {
    return switch (this) {
      Ok(value: final t) => t,
      Err(value: final e) =>
        unwrap_failed("called `Result::unwrap()` on an `Err` value", e),
    };
  }

  E expect_err(String msg) {
    return switch (this) {
      Ok(value: final t) => unwrap_failed(msg, t),
      Err(value: final e) => e,
    };
  }

  E unwrap_err() {
    return switch (this) {
      Ok(value: final t) =>
        unwrap_failed("called `Result::unwrap_err()` on an `Ok` value", t),
      Err(value: final e) => e,
    };
  }

  Result<U, E> and<U>(Result<U, E> res) {
    return switch (this) {
      Ok() => res,
      Err(value: final e) => Err(e),
    };
  }

  Result<U, E> and_then<U>(Result<U, E> Function(T) op) {
    return switch (this) {
      Ok(value: final t) => op(t),
      Err(value: final e) => Err(e),
    };
  }

  Result<T, F> or<F>(Result<T, F> res) {
    return switch (this) {
      Ok(value: final t) => Ok(t),
      Err() => res,
    };
  }

  Result<T, F> or_else<F>(Result<T, F> Function(E) op) {
    return switch (this) {
      Ok(value: final t) => Ok(t),
      Err(value: final e) => op(e),
    };
  }

  T unwrap_or(T defaultValue) {
    return switch (this) {
      Ok(value: final t) => t,
      Err() => defaultValue,
    };
  }

  T unwrap_or_else(T Function(E) op) {
    return switch (this) {
      Ok(value: final t) => t,
      Err(value: final e) => op(e),
    };
  }
}

extension Impl2<T, E> on Result<T?, E> {
  Result<T, E>? transpose() {
    return switch (this) {
      Ok(value: final x) => x != null ? Ok(x) : null,
      Err(value: final e) => Err(e),
    };
  }
}

Never unwrap_failed(String msg, dynamic error) {
  throw Exception("$msg: $error");
}
