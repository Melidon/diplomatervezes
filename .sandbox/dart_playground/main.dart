import 'result.dart';

Result<int, String> the_meaning_of_life() {
  return Err("42");
}

void main() {
  final answer = the_meaning_of_life();
  if (answer.is_err()) {
    print("The answer is not available");
  }
  final integer = switch (answer) {
    Ok(value: final x) => x,
    Err() => 0,
  };
  print("The answer is $integer");
}
