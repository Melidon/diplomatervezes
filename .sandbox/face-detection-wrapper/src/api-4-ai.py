from dotenv import load_dotenv
import os
import requests

load_dotenv()

RAPIDAPI_KEY = os.getenv("RAPIDAPI_KEY")

if RAPIDAPI_KEY is None:
    raise ValueError("RAPIDAPI_KEY is not set")

URL = "https://face-detection14.p.rapidapi.com/v1/results"
QUERYSTRING = {"detection": "true", "embeddings": "false"}
HEADERS = {
    "X-RapidAPI-Key": RAPIDAPI_KEY,
    "X-RapidAPI-Host": "face-detection14.p.rapidapi.com",
}


def face(path: os.PathLike):
    files = {"image": open(path, "rb")}
    response = requests.post(URL, params=QUERYSTRING, headers=HEADERS, files=files)
    return response.json()


if __name__ == "__main__":
    result = face("/home/melidon/Downloads/test.jpg")
    print(result)
