from dotenv import load_dotenv
import os
import requests

load_dotenv()

RAPIDAPI_KEY = os.getenv("RAPIDAPI_KEY")

if RAPIDAPI_KEY is None:
    raise ValueError("RAPIDAPI_KEY is not set")

URL = "https://face-detection11.p.rapidapi.com/FaceDetection"
HEADERS = {
    "X-RapidAPI-Key": RAPIDAPI_KEY,
    "X-RapidAPI-Host": "face-detection11.p.rapidapi.com",
}


def face(path: os.PathLike):
    files = {"photo": open(path, "rb")}
    response = requests.post(URL, headers=HEADERS, files=files)
    return response.json()


if __name__ == "__main__":
    result = face("/home/melidon/Downloads/test.jpg")
    print(result)
