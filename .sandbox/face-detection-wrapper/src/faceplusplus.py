from dotenv import load_dotenv
import os
import requests

load_dotenv()

FACEPP_KEY = os.getenv("FACEPP_KEY")
if FACEPP_KEY is None:
    raise ValueError("FACEPP_KEY is not set")

FACEPP_SECRET = os.getenv("FACEPP_SECRET")
if FACEPP_SECRET is None:
    raise ValueError("FACEPP_SECRET is not set")

URL = "https://api-us.faceplusplus.com/facepp/v3/detect"
BODY = {
    "api_key": FACEPP_KEY,
    "api_secret": FACEPP_SECRET,
}


def face(path: os.PathLike):
    files = {"image_file": open(path, "rb")}
    response = requests.post(URL, data=BODY, files=files)
    return response.json()


if __name__ == "__main__":
    result = face("/home/melidon/Downloads/test.jpg")
    print(result)
