from dotenv import load_dotenv
import os
import requests

load_dotenv()

RAPIDAPI_KEY = os.getenv("RAPIDAPI_KEY")

if RAPIDAPI_KEY is None:
    raise ValueError("RAPIDAPI_KEY is not set")

URL = "https://face-detection6.p.rapidapi.com/img/face"
HEADERS = {
    "content-type": "application/json",
    "X-RapidAPI-Key": RAPIDAPI_KEY,
    "X-RapidAPI-Host": "face-detection6.p.rapidapi.com",
}


def face(url: str):
    payload = {
        "url": url,
        "accuracy_boost": 2,
    }
    response = requests.post(URL, headers=HEADERS, json=payload)
    return response.json()


if __name__ == "__main__":
    url = "https://inferdo.com/img/face-1.jpg"
    result = face(url)
    print(result)
