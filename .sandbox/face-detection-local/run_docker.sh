#!/bin/bash

docker build -t face-detection-service .
docker run --gpus all --rm --name face-detection-service -p 80:80 face-detection-service
