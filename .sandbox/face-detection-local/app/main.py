from fastapi import FastAPI, UploadFile
from PIL.Image import Image, open as open_image
from pydantic import BaseModel
from time import perf_counter
from torch import cuda, device, Tensor
from torchvision.models.detection import (
    fasterrcnn_resnet50_fpn_v2,
    FasterRCNN_ResNet50_FPN_V2_Weights,
)
from torchvision.transforms.functional import to_tensor
from typing import Callable

DEVICE = device("cuda" if cuda.is_available() else "cpu")
if DEVICE.type == "cpu":
    print("Unable to use CUDA, falling back to CPU.")
WEIGHTS = FasterRCNN_ResNet50_FPN_V2_Weights.DEFAULT
MODEL = fasterrcnn_resnet50_fpn_v2(weights=WEIGHTS, box_score_thresh=0.9).to(DEVICE)
MODEL.eval()

preprocess: Callable[[Tensor], Tensor] = WEIGHTS.transforms()


def get_boxes(image: Image) -> list[tuple[float, float, float, float]]:
    image_tensor = preprocess(to_tensor(image)).to(DEVICE)
    batch = [image_tensor]
    prediction = MODEL(batch)[0]
    labels: list[str] = [WEIGHTS.meta["categories"][i] for i in prediction["labels"]]
    boxes: list[Tensor] = prediction["boxes"]
    return [
        tuple(box.tolist()) for box, label in zip(boxes, labels) if label == "person"
    ]


app = FastAPI()


class Rectangle(BaseModel):
    top: int
    left: int
    width: int
    height: int


class Face(BaseModel):
    face_rectangle: Rectangle


class Response(BaseModel):
    time_used: int
    faces: list[Face]
    face_num: int


@app.get("/detect")
async def detect(image_file: UploadFile) -> Response:
    image = open_image(image_file.file)
    start = perf_counter()
    boxes = get_boxes(image)
    end = perf_counter()
    return Response(
        time_used=int((end - start) * 1000),
        faces=[
            Face(
                face_rectangle=Rectangle(
                    top=int(y1),
                    left=int(x1),
                    width=int(x2 - x1),
                    height=int(y2 - y1),
                ),
            )
            for x1, y1, x2, y2 in boxes
        ],
        face_num=len(boxes),
    )
