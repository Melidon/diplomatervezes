## Architecture

I decided to use the microservices architecture as it will best meet my needs.

### API Gateway

All incoming requests will be handled by an API Gateway, which will be the only service available to the outside world.
I will also handle the TLS certificates, so internal communication can use normal HTTP.

For this, I've selected [Traefik](https://doc.traefik.io/traefik/).

I have no previous experience with it, but it is supposed to be one of the best alternatives.

### Auth Service

The only purpose of this service is to handle user sign-in and provide JWT tokens.

- This service will be written in [Rust](https://www.rust-lang.org/).
- I'll use the [axum](https://github.com/tokio-rs/axum) as the web application framework, since it is written by the same team as tokio, which is the most widely used async runtime for Rust.
- I'll use [SeaORM](https://www.sea-ql.org/SeaORM/) as the ORM.
- I'll use [PostgresSQL](https://www.postgresql.org/) as the database.
