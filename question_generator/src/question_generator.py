import spacy
from spacy.tokens import Span, Token
from typing import Optional, Tuple

# dep_ https://universaldependencies.org/docs/en/dep/index.html
# pos_ https://universaldependencies.org/u/pos/
# tag_ https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html

# lemma_ Base form of the token, with no inflectional suffixes. Example: "swam" -> "swim"


def debug_sentence(sentence: Span):
    print(sentence.text)
    for token in sentence:
        print(f"{token.text:<10} {token.dep_:<10} {token.pos_:<10} {token.tag_:<10}")


def subject_from_sentence(sentence: Span) -> Optional[Token]:
    for token in sentence:
        if token.dep_ == "nsubj":
            return token
    return None


def predicate_from_sentence(sentence: Span) -> Optional[Token]:
    for token in sentence:
        if token.dep_ == "ROOT":
            return token
    return None


def rest_of_the_sentence_text(sentence: Span) -> Optional[str]:
    predicate = predicate_from_sentence(sentence)
    if predicate is None:
        return None
    predicate_i = predicate.i - sentence[0].i
    return " ".join(
        [token.text for token in sentence[predicate_i + 1 : -1] if token.dep_ != "neg"]
    )


def get_subject_span_text(subject: Token) -> str:
    subject_tokens = list(subject.children)
    subject_tokens.append(subject)
    min_i = min([token.i for token in subject_tokens])
    max_i = max([token.i for token in subject_tokens])
    subject_span = subject.doc[min_i : max_i + 1]
    subject_span_text = (
        subject_span.text.lower()
        if subject_span[0].pos_ == "PRON" or subject_span[0].pos_ == "DET"
        else subject_span.text
    )
    subject_span_text = (
        "you"
        if subject_span_text == "i" or subject_span_text == "we"
        else subject_span_text
    )
    return subject_span_text


def question_from_sentence(sentence: Span) -> Optional[Tuple[str, str, str]]:
    # debug_sentence(sentence)
    subject = subject_from_sentence(sentence)
    predicate = predicate_from_sentence(sentence)
    rest_text = rest_of_the_sentence_text(sentence)
    if subject is None or predicate is None or rest_text is None:
        return None

    subject_span_text = get_subject_span_text(subject)
    question = None

    if predicate.tag_ == "VBD" or "VBD" in [
        token.tag_ for token in predicate.children if token.dep_ == "aux"
    ]:
        if predicate.lemma_ == "be":
            if subject.tag_ == "NN" or subject.tag_ == "NNP":
                question = f"Was {subject_span_text} {rest_text}?"
            if subject.tag_ == "NNS" or subject.tag_ == "NNPS":
                question = f"Were {subject_span_text} {rest_text}?"
        else:
            question = f"Did {subject_span_text} {predicate.lemma_} {rest_text}?"
    elif predicate.tag_ == "VBP":
        question = f"Do {subject_span_text} {predicate.lemma_} {rest_text}?"
    # TODO: Add more cases

    if question is None:
        return None

    answer = "No" if "neg" in [token.dep_ for token in sentence] else "Yes"
    return sentence.text.strip(), question, answer


spacy.require_gpu()
nlp = spacy.load("en_core_web_trf")


def generate_questions(text: str) -> list[Tuple[str, str, str]]:
    doc = nlp(text)
    questions = [
        question
        for question in (question_from_sentence(sentence) for sentence in doc.sents)
        if question is not None
    ]
    return questions
