from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties
from psycopg import Connection
from question_generator import generate_questions
from uuid import UUID, uuid4


class TranscriptCreatedConsumer:
    def __init__(self, memory_collection_db: Connection, question_db: Connection):
        self.memory_collection_db = memory_collection_db
        self.question_db = question_db

    def __call__(
        self,
        _ch: BlockingChannel,
        _method: Basic.Deliver,
        _properties: BasicProperties,
        body: bytes,
    ):
        transcript_id = UUID(bytes=body)
        print(f"Received transcript with id: {transcript_id}")
        user_id, text = (
            self.memory_collection_db.cursor()
            .execute(
                """
                SELECT memory.owner_id AS user_id, transcript.text
                FROM transcript
                INNER JOIN recording ON recording.id = transcript.recording_id
                INNER JOIN memory ON memory.id = recording.memory_id
                WHERE transcript.id = %s
                """,
                (transcript_id,),
            )
            .fetchone()
        )
        questions = generate_questions(text)
        self.question_db.cursor().executemany(
            "INSERT INTO question_table (id, user_id, question, answer) VALUES (%s, %s, %s, %s)",
            [
                (uuid4(), user_id, question, answer)
                for (_, question, answer) in questions
            ],
        )
        self.question_db.commit()
        print(f"Generated {len(questions)} questions for transcript {transcript_id}")
