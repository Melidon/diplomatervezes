import dotenv
import os
import pika
from pika.exchange_type import ExchangeType
import psycopg
from transcript_created_consumer import TranscriptCreatedConsumer

TRANSCRIPT_CREATED = "transcript_created"


def main():
    dotenv.load_dotenv()
    memory_collection_db_url = os.getenv("MEMORY_COLLECTION_DB_URL")
    memory_collection_db = psycopg.connect(memory_collection_db_url)
    question_db_url = os.getenv("QUESTION_DB_URL")
    question_db = psycopg.connect(question_db_url)
    rabbitmq_uri = os.getenv("RABBITMQ_URI")
    parameters = pika.URLParameters(rabbitmq_uri)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(
        exchange=TRANSCRIPT_CREATED, exchange_type=ExchangeType.fanout
    )
    queue = channel.queue_declare(queue="", exclusive=True)
    channel.queue_bind(exchange=TRANSCRIPT_CREATED, queue=queue.method.queue)
    channel.basic_consume(
        queue=queue.method.queue,
        on_message_callback=TranscriptCreatedConsumer(
            memory_collection_db, question_db
        ),
        auto_ack=True,
    )
    channel.start_consuming()


if __name__ == "__main__":
    main()
