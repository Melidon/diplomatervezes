#!/bin/bash

python -m venv .venv && \
source .venv/bin/activate && \

pip install --upgrade pip && \

# # dotenv
# pip install python-dotenv
# # pika
# pip install pika
# # psycopg
# pip install "psycopg[binary,pool]"
# # spaCy
# pip install -U pip setuptools wheel
# pip install -U 'spacy[cuda12x]'
# python -m spacy download en_core_web_trf

# pip freeze > requirements.txt

pip install --requirement requirements.txt
