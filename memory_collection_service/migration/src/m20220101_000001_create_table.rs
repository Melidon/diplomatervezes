use sea_orm_migration::{prelude::*, schema::*};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Entity::Table)
                    .if_not_exists()
                    .col(uuid(Entity::Id).primary_key())
                    .col(uuid(Entity::OwnerId))
                    .col(string(Entity::Name))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Memory::Table)
                    .if_not_exists()
                    .col(uuid(Memory::Id).primary_key())
                    .col(uuid(Memory::OwnerId))
                    .col(string(Memory::Name))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(MemoryText::Table)
                    .if_not_exists()
                    .col(uuid(MemoryText::Id).primary_key())
                    .col(uuid(MemoryText::MemoryId))
                    .foreign_key(
                        ForeignKey::create()
                            // .name("fk-text_memory_id")
                            .from(MemoryText::Table, MemoryText::MemoryId)
                            .to(Memory::Table, Memory::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(text(MemoryText::Text))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Image::Table)
                    .if_not_exists()
                    .col(uuid(Image::Id).primary_key())
                    .col(uuid(Image::MemoryId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-image_memory_id")
                            .from(Image::Table, Image::MemoryId)
                            .to(Memory::Table, Memory::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(string(Image::Name))
                    .col(binary(Image::Bytes))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(ImageText::Table)
                    .if_not_exists()
                    .col(uuid(ImageText::Id).primary_key())
                    .col(uuid(ImageText::ImageId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-text_image_id")
                            .from(ImageText::Table, ImageText::ImageId)
                            .to(Image::Table, Image::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(text(ImageText::Text))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Annotation::Table)
                    .if_not_exists()
                    .col(uuid(Annotation::Id).primary_key())
                    .col(uuid(Annotation::ImageId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-annotation_image_id")
                            .from(Annotation::Table, Annotation::ImageId)
                            .to(Image::Table, Image::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(uuid(Annotation::EntityId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-annotation_entity_id")
                            .from(Annotation::Table, Annotation::EntityId)
                            .to(Entity::Table, Entity::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(string_null(Annotation::FacesetToken))
                    .col(boolean(Annotation::Revised))
                    .col(integer(Annotation::Left))
                    .col(integer(Annotation::Top))
                    .col(integer(Annotation::Width))
                    .col(integer(Annotation::Height))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Recording::Table)
                    .if_not_exists()
                    .col(uuid(Recording::Id).primary_key())
                    .col(uuid(Recording::MemoryId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-recording_memory_id")
                            .from(Recording::Table, Recording::MemoryId)
                            .to(Memory::Table, Memory::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(string(Recording::Name))
                    .col(binary(Recording::Bytes))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Transcript::Table)
                    .if_not_exists()
                    .col(uuid(Transcript::Id).primary_key())
                    .col(uuid(Transcript::RecordingId))
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-transcript_recording_id")
                            .from(Transcript::Table, Transcript::RecordingId)
                            .to(Recording::Table, Recording::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(text(Transcript::Text))
                    .col(boolean(Transcript::Revised))
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Transcript::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Recording::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Annotation::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(ImageText::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Image::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(MemoryText::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Memory::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Entity::Table).to_owned())
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
enum Entity {
    Table,
    Id,
    OwnerId,
    Name,
}

#[derive(DeriveIden)]
enum Memory {
    Table,
    Id,
    OwnerId,
    Name,
}

#[derive(DeriveIden)]
enum MemoryText {
    Table,
    Id,
    MemoryId,
    Text,
}

#[derive(DeriveIden)]
enum Image {
    Table,
    Id,
    MemoryId,
    Name,
    Bytes,
}

#[derive(DeriveIden)]
enum ImageText {
    Table,
    Id,
    ImageId,
    Text,
}

#[derive(DeriveIden)]
enum Annotation {
    Table,
    Id,
    ImageId,
    EntityId,
    FacesetToken,
    Revised,
    Left,
    Top,
    Width,
    Height,
}

#[derive(DeriveIden)]
enum Recording {
    Table,
    Id,
    MemoryId,
    Name,
    Bytes,
}

#[derive(DeriveIden)]
enum Transcript {
    Table,
    Id,
    RecordingId,
    Text,
    Revised,
}
