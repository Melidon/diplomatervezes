use {
    axum::{
        http::StatusCode,
        response::{IntoResponse, Response},
    },
    std::{
        error::Error,
        fmt::{Display, Formatter, Result},
    },
};

#[derive(Debug)]
pub(crate) enum MemoryCollectionError {
    TokenExtract(axum_extra::typed_header::TypedHeaderRejection),
    JsonWebToken(jsonwebtoken::errors::Error),
    ExpiredToken,
    Forbidden,
    Database(sea_orm::DbErr),
    NotFound,
    MultipartError(axum::extract::multipart::MultipartError),
    NoFile,
    MissingName,
    InvalidFormat {
        supported_formats: &'static [file_format::FileFormat],
    },
    MessageQueue(amqprs::error::Error),
}

impl Display for MemoryCollectionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            MemoryCollectionError::TokenExtract(typed_header_rejection) => {
                write!(f, "{}", typed_header_rejection)
            }
            MemoryCollectionError::JsonWebToken(error) => write!(f, "{}", error),
            MemoryCollectionError::ExpiredToken => write!(f, "Expired token"),
            MemoryCollectionError::Forbidden => write!(f, "Forbidden"),
            MemoryCollectionError::Database(db_err) => write!(f, "{}", db_err),
            MemoryCollectionError::NotFound => write!(f, "Not found"),
            MemoryCollectionError::MultipartError(multipart_error) => {
                write!(f, "{}", multipart_error)
            }
            MemoryCollectionError::NoFile => write!(f, "No file"),
            MemoryCollectionError::MissingName => write!(f, "Missing name"),
            MemoryCollectionError::InvalidFormat { supported_formats } => write!(
                f,
                "Invalid format. Supported formats: {:?}",
                supported_formats
                    .iter()
                    .map(file_format::FileFormat::media_type)
                    .collect::<Vec<_>>()
            ),
            MemoryCollectionError::MessageQueue(error) => write!(f, "{}", error),
        }
    }
}

impl Error for MemoryCollectionError {}

impl IntoResponse for MemoryCollectionError {
    fn into_response(self) -> Response {
        let status = match &self {
            MemoryCollectionError::TokenExtract(_) => StatusCode::UNAUTHORIZED,
            MemoryCollectionError::JsonWebToken(_) => StatusCode::UNAUTHORIZED,
            MemoryCollectionError::ExpiredToken => StatusCode::UNAUTHORIZED,
            MemoryCollectionError::Forbidden => StatusCode::FORBIDDEN,
            MemoryCollectionError::Database(_) => StatusCode::INTERNAL_SERVER_ERROR,
            MemoryCollectionError::NotFound => StatusCode::NOT_FOUND,
            MemoryCollectionError::MultipartError(multipart_error) => multipart_error.status(),
            MemoryCollectionError::NoFile => StatusCode::BAD_REQUEST,
            MemoryCollectionError::MissingName => StatusCode::BAD_REQUEST,
            MemoryCollectionError::InvalidFormat {
                supported_formats: _,
            } => StatusCode::BAD_REQUEST,
            MemoryCollectionError::MessageQueue(_) => StatusCode::INTERNAL_SERVER_ERROR,
        };
        (status, self.to_string()).into_response()
    }
}
