use {
    super::{claims::Claims, memory_collection_error::MemoryCollectionError},
    crate::entities::{
        annotation, entity, image, image_text, memory, memory_text, prelude::*, recording,
        transcript,
    },
    sea_orm::{DatabaseConnection, EntityTrait, JoinType, QuerySelect, RelationTrait},
    uuid::Uuid,
};

pub(crate) async fn check_access_to_owner(
    db: &DatabaseConnection,
    claims: &Claims,
    owner_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_entity(
    db: &DatabaseConnection,
    claims: &Claims,
    entity_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_memory(
    db: &DatabaseConnection,
    claims: &Claims,
    memory_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_image(
    db: &DatabaseConnection,
    claims: &Claims,
    image_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_annotation(
    db: &DatabaseConnection,
    claims: &Claims,
    annotation_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_recording(
    db: &DatabaseConnection,
    claims: &Claims,
    recording_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_transcript(
    db: &DatabaseConnection,
    claims: &Claims,
    transcript_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_memory_text(
    db: &DatabaseConnection,
    claims: &Claims,
    memory_text_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}

pub(crate) async fn check_access_to_image_text(
    db: &DatabaseConnection,
    claims: &Claims,
    image_text_id: &Uuid,
) -> Result<(), MemoryCollectionError> {
    // TODO
    Ok(())
}
