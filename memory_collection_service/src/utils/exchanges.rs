use sea_orm::{DeriveIden, EnumIter};

#[derive(DeriveIden, EnumIter)]
pub enum AuthExchanges {
    UserCreated,
    UserDeleted,
}

#[derive(DeriveIden, EnumIter)]
pub enum MemoryCollectionExchanges {
    ImageCreated,
    RecordingCreated,
}
