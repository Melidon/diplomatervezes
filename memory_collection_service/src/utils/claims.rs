use {
    super::memory_collection_error::MemoryCollectionError,
    axum::{async_trait, extract::FromRequestParts, http::request::Parts, RequestPartsExt},
    axum_extra::{
        headers::{authorization::Bearer, Authorization},
        TypedHeader,
    },
    chrono::Utc,
    jsonwebtoken::{decode, DecodingKey, Validation},
    once_cell::sync::Lazy,
    serde::Deserialize,
    std::env,
    uuid::Uuid,
};

#[derive(Deserialize)]
pub(crate) struct Claims {
    exp: i64,
    pub(super) user_id: Uuid,
    pub(super) role: Role,
    elderly_people: Option<Vec<Uuid>>,
}

#[async_trait]
impl<S> FromRequestParts<S> for Claims
where
    S: Send + Sync,
{
    type Rejection = MemoryCollectionError;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(MemoryCollectionError::TokenExtract)?;
        let claims = decode::<Claims>(bearer.token(), &DECODING_KEY, &Validation::default())
            .map_err(MemoryCollectionError::JsonWebToken)?
            .claims;
        if claims.exp < Utc::now().timestamp() {
            return Err(MemoryCollectionError::ExpiredToken);
        }
        Ok(claims)
    }
}

static DECODING_KEY: Lazy<DecodingKey> = Lazy::new(|| {
    let secret = env::var("JWT_SECRET").expect("JWT_SECRET must be set");
    DecodingKey::from_secret(secret.as_bytes())
});

#[derive(Deserialize)]
pub(super) enum Role {
    Relative,
    Elderly,
    Admin,
}
