mod incoming_transcript;
mod outgoing_transcript;

use {
    self::{
        incoming_transcript::{CreateTranscript, QueryParams, UpdateTranscript},
        outgoing_transcript::OutgoingTranscript,
    },
    crate::{
        entities::transcript::{
            ActiveModel as Transcript, Column::RecordingId, Entity as TranscriptTable,
        },
        utils::{
            access_check::{check_access_to_recording, check_access_to_transcript},
            claims::Claims,
            memory_collection_error::MemoryCollectionError,
        },
    },
    axum::{
        extract::{Path, Query},
        http::StatusCode,
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter},
    std::sync::Arc,
    uuid::Uuid,
};

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_transcript))
        .route("/:transcript_id", get(get_transcript))
        .route("/", get(list_transcripts))
        .route("/:transcript_id", put(update_transcript))
        .route("/:transcript_id", delete(delete_transcript))
}

async fn create_transcript(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Json(transcript): Json<CreateTranscript>,
) -> Result<(StatusCode, Json<OutgoingTranscript>), MemoryCollectionError> {
    check_access_to_recording(db.as_ref(), &claims, &transcript.recording_id).await?;
    let transcript = TranscriptTable::insert(Transcript::from(transcript))
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    Ok((StatusCode::CREATED, Json(transcript)))
}

async fn get_transcript(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(transcript_id): Path<Uuid>,
) -> Result<Json<OutgoingTranscript>, MemoryCollectionError> {
    check_access_to_transcript(db.as_ref(), &claims, &transcript_id).await?;
    let transcript = TranscriptTable::find_by_id(transcript_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .into();
    Ok(Json(transcript))
}

async fn list_transcripts(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { recording_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingTranscript>>, MemoryCollectionError> {
    check_access_to_recording(db.as_ref(), &claims, &recording_id).await?;
    let entities = TranscriptTable::find()
        .filter(RecordingId.eq(recording_id))
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingTranscript::from)
        .collect();
    Ok(Json(entities))
}

async fn update_transcript(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(transcript_id): Path<Uuid>,
    Json(transcript): Json<UpdateTranscript>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_transcript(db.as_ref(), &claims, &transcript_id).await?;
    TranscriptTable::update(Transcript::from(transcript))
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_transcript(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(transcript_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_transcript(db.as_ref(), &claims, &transcript_id).await?;
    TranscriptTable::delete_by_id(transcript_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
