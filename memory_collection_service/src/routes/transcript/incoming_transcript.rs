use {
    crate::entities::transcript::ActiveModel as Transcript, sea_orm::Set, serde::Deserialize,
    uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct CreateTranscript {
    pub(super) recording_id: Uuid,
    pub(super) text: String,
}

impl From<CreateTranscript> for Transcript {
    fn from(transcript: CreateTranscript) -> Self {
        Self {
            id: Set(Uuid::new_v4()),
            recording_id: Set(transcript.recording_id),
            text: Set(transcript.text),
            revised: Set(true),
        }
    }
}

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) recording_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateTranscript {
    pub(super) text: String,
}

impl From<UpdateTranscript> for Transcript {
    fn from(transcript: UpdateTranscript) -> Self {
        Self {
            text: Set(transcript.text),
            revised: Set(true),
            ..Default::default()
        }
    }
}
