use {crate::entities::transcript::Model as Transcript, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingTranscript {
    id: Uuid,
    text: String,
    revised: bool,
}

impl From<Transcript> for OutgoingTranscript {
    fn from(transcript: Transcript) -> Self {
        Self {
            id: transcript.id,
            text: transcript.text,
            revised: transcript.revised,
        }
    }
}
