mod incoming_annotation;
mod outgoing_annotation;

use {
    self::{
        incoming_annotation::{CreateAnnotation, QueryParams, UpdateAnnotation},
        outgoing_annotation::OutgoingAnnotation,
    },
    crate::{
        entities::annotation::{
            ActiveModel as Annotation, Column::ImageId, Entity as AnnotationTable,
        },
        utils::{
            access_check::{check_access_to_annotation, check_access_to_image},
            claims::Claims,
            memory_collection_error::MemoryCollectionError,
        },
    },
    axum::{
        extract::{Path, Query},
        http::StatusCode,
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    std::sync::Arc,
    uuid::Uuid,
};

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_annotation))
        .route("/:annotation_id", get(get_annotation))
        .route("/", get(list_annotations))
        .route("/:annotation_id", put(update_annotation))
        .route("/:annotation_id", delete(delete_annotation))
}

async fn create_annotation(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Json(annotation): Json<CreateAnnotation>,
) -> Result<(StatusCode, Json<OutgoingAnnotation>), MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &annotation.image_id).await?;
    let annotation = AnnotationTable::insert(Annotation::from(annotation))
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    Ok((StatusCode::CREATED, Json(annotation)))
}

async fn get_annotation(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(annotation_id): Path<Uuid>,
) -> Result<Json<OutgoingAnnotation>, MemoryCollectionError> {
    check_access_to_annotation(db.as_ref(), &claims, &annotation_id).await?;
    let annotation = AnnotationTable::find_by_id(annotation_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .into();
    Ok(Json(annotation))
}

async fn list_annotations(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { image_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingAnnotation>>, MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &image_id).await?;
    let entities = AnnotationTable::find()
        .filter(ImageId.eq(image_id))
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingAnnotation::from)
        .collect();
    Ok(Json(entities))
}

async fn update_annotation(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(annotation_id): Path<Uuid>,
    Json(annotation): Json<UpdateAnnotation>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_annotation(db.as_ref(), &claims, &annotation_id).await?;
    let annotation = Annotation {
        id: Set(annotation_id),
        ..annotation.into()
    };
    AnnotationTable::update(annotation)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_annotation(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(annotation_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_annotation(db.as_ref(), &claims, &annotation_id).await?;
    AnnotationTable::delete_by_id(annotation_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
