use {
    crate::entities::annotation::ActiveModel as Annotation, sea_orm::Set, serde::Deserialize,
    uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct CreateAnnotation {
    pub(super) image_id: Uuid,
    pub(super) entity_id: Uuid,
    pub(super) left: i32,
    pub(super) top: i32,
    pub(super) width: i32,
    pub(super) height: i32,
}

impl From<CreateAnnotation> for Annotation {
    fn from(annotation: CreateAnnotation) -> Self {
        Self {
            id: Set(Uuid::new_v4()),
            image_id: Set(annotation.image_id),
            entity_id: Set(annotation.entity_id),
            faceset_token: Set(None),
            revised: Set(true),
            left: Set(annotation.left),
            top: Set(annotation.top),
            width: Set(annotation.width),
            height: Set(annotation.height),
        }
    }
}

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) image_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateAnnotation {
    pub(super) entity_id: Uuid,
    pub(super) left: i32,
    pub(super) top: i32,
    pub(super) width: i32,
    pub(super) height: i32,
}

impl From<UpdateAnnotation> for Annotation {
    fn from(annotation: UpdateAnnotation) -> Self {
        Self {
            entity_id: Set(annotation.entity_id),
            left: Set(annotation.left),
            top: Set(annotation.top),
            width: Set(annotation.width),
            height: Set(annotation.height),
            revised: Set(true),
            ..Default::default()
        }
    }
}
