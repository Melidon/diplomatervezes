use {crate::entities::annotation::Model as Annotation, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingAnnotation {
    id: Uuid,
    entity_id: Uuid,
    left: i32,
    top: i32,
    width: i32,
    height: i32,
    revised: bool,
}

impl From<Annotation> for OutgoingAnnotation {
    fn from(annotation: Annotation) -> Self {
        Self {
            id: annotation.id,
            entity_id: annotation.entity_id,
            left: annotation.left,
            top: annotation.top,
            width: annotation.width,
            height: annotation.height,
            revised: annotation.revised,
        }
    }
}
