use {crate::entities::image::ActiveModel as Image, sea_orm::Set, serde::Deserialize, uuid::Uuid};

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) memory_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateImage {
    pub(super) name: String,
}

impl From<UpdateImage> for Image {
    fn from(image: UpdateImage) -> Self {
        Self {
            name: Set(image.name),
            ..Default::default()
        }
    }
}
