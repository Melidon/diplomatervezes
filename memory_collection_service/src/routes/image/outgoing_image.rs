use {crate::entities::image::Model as Image, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingImage {
    pub(super) id: Uuid,
    name: String,
}

impl From<Image> for OutgoingImage {
    fn from(image: Image) -> Self {
        Self {
            id: image.id,
            name: image.name,
        }
    }
}

impl From<(Uuid, String)> for OutgoingImage {
    fn from((id, name): (Uuid, String)) -> Self {
        Self { id, name }
    }
}
