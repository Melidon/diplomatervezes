mod incoming_image;
mod outgoing_image;

use {
    self::{
        incoming_image::{QueryParams, UpdateImage},
        outgoing_image::OutgoingImage,
    },
    crate::{
        entities::image::{
            ActiveModel as Image,
            Column::{Id, MemoryId, Name},
            Entity as ImageTable,
        },
        utils::{
            access_check::{check_access_to_image, check_access_to_memory},
            claims::Claims,
            exchanges::MemoryCollectionExchanges,
            memory_collection_error::MemoryCollectionError,
        },
    },
    amqprs::{
        channel::{BasicPublishArguments, Channel},
        BasicProperties,
    },
    axum::{
        extract::{Multipart, Path, Query},
        http::{
            header::{HeaderName, CONTENT_TYPE},
            HeaderValue, StatusCode,
        },
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    file_format::FileFormat::{
        self, JointPhotographicExpertsGroup as JPEG, PortableNetworkGraphics as PNG,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, Iden, QueryFilter, QuerySelect, Set},
    std::sync::Arc,
    uuid::Uuid,
};

const SUPPORTED_FORMATS: [FileFormat; 2] = [JPEG, PNG];

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_image))
        // .layer(DefaultBodyLimit::max(8 * 1024 * 1024))
        .route("/:image_id", get(get_image))
        .route("/", get(list_images))
        .route("/:image_id", put(update_image))
        .route("/:image_id", delete(delete_image))
}

async fn create_image(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    Extension(mq): Extension<Channel>,
    claims: Claims,
    Query(QueryParams { memory_id }): Query<QueryParams>,
    mut multipart: Multipart,
) -> Result<(StatusCode, Json<OutgoingImage>), MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let field = multipart
        .next_field()
        .await
        .map_err(MemoryCollectionError::MultipartError)?
        .ok_or(MemoryCollectionError::NoFile)?;
    let name = field
        .name()
        .ok_or(MemoryCollectionError::MissingName)?
        .to_string();
    let bytes = field
        .bytes()
        .await
        .map_err(MemoryCollectionError::MultipartError)?
        .to_vec();
    let file_format = FileFormat::from_bytes(&bytes);
    SUPPORTED_FORMATS
        .contains(&file_format)
        .then_some(())
        .ok_or(MemoryCollectionError::InvalidFormat {
            supported_formats: &SUPPORTED_FORMATS,
        })?;
    let image = Image {
        id: Set(Uuid::new_v4()),
        memory_id: Set(memory_id),
        name: Set(name),
        bytes: Set(bytes),
    };
    let image: OutgoingImage = ImageTable::insert(image)
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    mq.basic_publish(
        BasicProperties::default(),
        image.id.into_bytes().to_vec(),
        BasicPublishArguments::new(&MemoryCollectionExchanges::ImageCreated.to_string(), ""),
    )
    .await
    .map_err(MemoryCollectionError::MessageQueue)?;
    Ok((StatusCode::CREATED, Json(image)))
}

async fn get_image(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(image_id): Path<Uuid>,
) -> Result<([(HeaderName, HeaderValue); 1], Vec<u8>), MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &image_id).await?;
    let image = ImageTable::find_by_id(image_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .bytes;
    let content_type = FileFormat::from_bytes(&image)
        .media_type()
        .try_into()
        .expect("Invalid media type");
    Ok(([(CONTENT_TYPE, content_type)], image))
}

async fn list_images(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { memory_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingImage>>, MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let images = ImageTable::find()
        .filter(MemoryId.eq(memory_id))
        .select_only()
        .columns([Id, Name])
        .into_tuple::<(Uuid, String)>()
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingImage::from)
        .collect();
    Ok(Json(images))
}

async fn update_image(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(image_id): Path<Uuid>,
    Json(image): Json<UpdateImage>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &image_id).await?;
    let image = Image {
        id: Set(image_id),
        ..image.into()
    };
    ImageTable::update(image)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_image(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(image_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &image_id).await?;
    ImageTable::delete_by_id(image_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
