mod incoming_memory_text;
mod outgoing_memory_text;

use {
    self::{
        incoming_memory_text::{CreateMemoryText, QueryParams, UpdateMemoryText},
        outgoing_memory_text::OutgoingMemoryText,
    },
    crate::{
        entities::memory_text::{
            ActiveModel as MemoryText, Column::MemoryId, Entity as MemoryTextTable,
        },
        utils::{
            access_check::{check_access_to_memory, check_access_to_memory_text},
            claims::Claims,
            memory_collection_error::MemoryCollectionError,
        },
    },
    axum::{
        extract::{Path, Query},
        http::StatusCode,
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    std::sync::Arc,
    uuid::Uuid,
};

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_memory_text))
        .route("/:memory_text_id", get(get_memory_text))
        .route("/", get(list_memory_texts))
        .route("/:memory_text_id", put(update_memory_text))
        .route("/:memory_text_id", delete(delete_memory_text))
}

async fn create_memory_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Json(memory_text): Json<CreateMemoryText>,
) -> Result<(StatusCode, Json<OutgoingMemoryText>), MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_text.memory_id).await?;
    let memory_text = MemoryTextTable::insert(MemoryText::from(memory_text))
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    Ok((StatusCode::CREATED, Json(memory_text)))
}

async fn get_memory_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(memory_text_id): Path<Uuid>,
) -> Result<Json<OutgoingMemoryText>, MemoryCollectionError> {
    check_access_to_memory_text(db.as_ref(), &claims, &memory_text_id).await?;
    let memory_text = MemoryTextTable::find_by_id(memory_text_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .into();
    Ok(Json(memory_text))
}

async fn list_memory_texts(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { memory_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingMemoryText>>, MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let entities = MemoryTextTable::find()
        .filter(MemoryId.eq(memory_id))
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingMemoryText::from)
        .collect();
    Ok(Json(entities))
}

async fn update_memory_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(memory_text_id): Path<Uuid>,
    Json(memory_text): Json<UpdateMemoryText>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_memory_text(db.as_ref(), &claims, &memory_text_id).await?;
    let memory_text = MemoryText {
        id: Set(memory_text_id),
        ..memory_text.into()
    };
    MemoryTextTable::update(memory_text)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_memory_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(memory_text_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_memory_text(db.as_ref(), &claims, &memory_text_id).await?;
    MemoryTextTable::delete_by_id(memory_text_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
