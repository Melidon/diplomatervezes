use {
    crate::entities::memory_text::ActiveModel as MemoryText, sea_orm::Set, serde::Deserialize,
    uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct CreateMemoryText {
    pub(super) memory_id: Uuid,
    pub(super) text: String,
}

impl From<CreateMemoryText> for MemoryText {
    fn from(memory_text: CreateMemoryText) -> Self {
        Self {
            id: Set(Uuid::new_v4()),
            memory_id: Set(memory_text.memory_id),
            text: Set(memory_text.text),
        }
    }
}

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) memory_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateMemoryText {
    pub(super) text: String,
}

impl From<UpdateMemoryText> for MemoryText {
    fn from(memory_text: UpdateMemoryText) -> Self {
        Self {
            text: Set(memory_text.text),
            ..Default::default()
        }
    }
}
