use {crate::entities::memory_text::Model as MemoryText, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingMemoryText {
    id: Uuid,
    text: String,
}

impl From<MemoryText> for OutgoingMemoryText {
    fn from(memory_text: MemoryText) -> Self {
        Self {
            id: memory_text.id,
            text: memory_text.text,
        }
    }
}
