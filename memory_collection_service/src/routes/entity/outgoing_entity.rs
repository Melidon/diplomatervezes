use {crate::entities::entity::Model as Entity, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingEntity {
    id: Uuid,
    name: String,
}

impl From<Entity> for OutgoingEntity {
    fn from(entity: Entity) -> Self {
        Self {
            id: entity.id,
            name: entity.name,
        }
    }
}
