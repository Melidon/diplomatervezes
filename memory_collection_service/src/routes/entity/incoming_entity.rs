use {
    crate::entities::entity::ActiveModel as Entity, sea_orm::Set, serde::Deserialize, uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct CreateEntity {
    pub(super) owner_id: Uuid,
    pub(super) name: String,
}

impl From<CreateEntity> for Entity {
    fn from(entity: CreateEntity) -> Self {
        Self {
            id: Set(Uuid::new_v4()),
            owner_id: Set(entity.owner_id),
            name: Set(entity.name),
        }
    }
}

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) owner_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateEntity {
    pub(super) name: String,
}

impl From<UpdateEntity> for Entity {
    fn from(entity: UpdateEntity) -> Self {
        Self {
            name: Set(entity.name),
            ..Default::default()
        }
    }
}
