mod incoming_entity;
mod outgoing_entity;

use {
    self::{
        incoming_entity::{CreateEntity, QueryParams, UpdateEntity},
        outgoing_entity::OutgoingEntity,
    },
    crate::{
        entities::entity::{ActiveModel as Entity, Column::OwnerId, Entity as EntityTable},
        utils::{
            access_check::{check_access_to_entity, check_access_to_owner},
            claims::Claims,
            memory_collection_error::MemoryCollectionError,
        },
    },
    axum::{
        extract::{Path, Query},
        http::StatusCode,
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    std::sync::Arc,
    uuid::Uuid,
};

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_entity))
        .route("/:entity_id", get(get_entity))
        .route("/", get(list_entities))
        .route("/:entity_id", put(update_entity))
        .route("/:entity_id", delete(delete_entity))
}

async fn create_entity(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Json(entity): Json<CreateEntity>,
) -> Result<(StatusCode, Json<OutgoingEntity>), MemoryCollectionError> {
    check_access_to_owner(db.as_ref(), &claims, &entity.owner_id).await?;
    let entity = EntityTable::insert(Entity::from(entity))
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    Ok((StatusCode::CREATED, Json(entity)))
}

async fn get_entity(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(entity_id): Path<Uuid>,
) -> Result<Json<OutgoingEntity>, MemoryCollectionError> {
    check_access_to_entity(db.as_ref(), &claims, &entity_id).await?;
    let entity = EntityTable::find_by_id(entity_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .into();
    Ok(Json(entity))
}

async fn list_entities(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { owner_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingEntity>>, MemoryCollectionError> {
    check_access_to_owner(db.as_ref(), &claims, &owner_id).await?;
    let entities = EntityTable::find()
        .filter(OwnerId.eq(owner_id))
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingEntity::from)
        .collect();
    Ok(Json(entities))
}

async fn update_entity(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(entity_id): Path<Uuid>,
    Json(entity): Json<UpdateEntity>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_entity(db.as_ref(), &claims, &entity_id).await?;
    let entity = Entity {
        id: Set(entity_id),
        ..entity.into()
    };
    EntityTable::update(entity)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_entity(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(entity_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_entity(db.as_ref(), &claims, &entity_id).await?;
    EntityTable::delete_by_id(entity_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
