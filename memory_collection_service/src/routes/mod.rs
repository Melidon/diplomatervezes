mod annotation;
mod entity;
mod image;
mod image_text;
mod memory;
mod memory_text;
mod recording;
mod transcript;

use axum::Router;
use {
    annotation::router as annotation_router, entity::router as entity_router,
    image::router as image_router, image_text::router as image_text_router,
    memory::router as memory_router, memory_text::router as memory_text_router,
    recording::router as recording_router, transcript::router as transcript_router,
};

pub(super) fn router() -> Router {
    Router::new()
        .nest("/entities", entity_router())
        .nest("/memories", memory_router())
        .nest("/memory_texts", memory_text_router())
        .nest("/images", image_router())
        .nest("/image_texts", image_text_router())
        .nest("/annotations", annotation_router())
        .nest("/recordings", recording_router())
        .nest("/transcripts", transcript_router())
}
