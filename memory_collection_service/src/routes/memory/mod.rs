mod incoming_memory;
mod outgoing_memory;

use {
    self::{
        incoming_memory::{CreateMemory, QueryParams, UpdateMemory},
        outgoing_memory::OutgoingMemory,
    },
    crate::{
        entities::memory::{ActiveModel as Memory, Column::OwnerId, Entity as MemoryTable},
        utils::{
            access_check::{check_access_to_memory, check_access_to_owner},
            claims::Claims,
            memory_collection_error::MemoryCollectionError,
        },
    },
    axum::{
        extract::{Path, Query},
        http::StatusCode,
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    std::sync::Arc,
    uuid::Uuid,
};

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_memory))
        .route("/:memory_id", get(get_memory))
        .route("/", get(list_memories))
        .route("/:memory_id", put(update_memory))
        .route("/:memory_id", delete(delete_memory))
}

async fn create_memory(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Json(memory): Json<CreateMemory>,
) -> Result<(StatusCode, Json<OutgoingMemory>), MemoryCollectionError> {
    check_access_to_owner(db.as_ref(), &claims, &memory.owner_id).await?;
    let memory = MemoryTable::insert(Memory::from(memory))
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    Ok((StatusCode::CREATED, Json(memory)))
}

async fn get_memory(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(memory_id): Path<Uuid>,
) -> Result<Json<OutgoingMemory>, MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let memory = MemoryTable::find_by_id(memory_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .into();
    Ok(Json(memory))
}

async fn list_memories(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { owner_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingMemory>>, MemoryCollectionError> {
    check_access_to_owner(db.as_ref(), &claims, &owner_id).await?;
    let memories = MemoryTable::find()
        .filter(OwnerId.eq(owner_id))
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingMemory::from)
        .collect();
    Ok(Json(memories))
}

async fn update_memory(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(memory_id): Path<Uuid>,
    Json(memory): Json<UpdateMemory>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let memory = Memory {
        id: Set(memory_id),
        ..memory.into()
    };
    MemoryTable::update(memory)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_memory(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(memory_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    MemoryTable::delete_by_id(memory_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
