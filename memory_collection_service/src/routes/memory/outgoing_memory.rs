use {crate::entities::memory::Model as Memory, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingMemory {
    id: Uuid,
    name: String,
}

impl From<Memory> for OutgoingMemory {
    fn from(memory: Memory) -> Self {
        Self {
            id: memory.id,
            name: memory.name,
        }
    }
}
