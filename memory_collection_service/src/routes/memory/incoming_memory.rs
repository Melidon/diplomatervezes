use {
    crate::entities::memory::ActiveModel as Memory, sea_orm::Set, serde::Deserialize, uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct CreateMemory {
    pub(super) owner_id: Uuid,
    pub(super) name: String,
}

impl From<CreateMemory> for Memory {
    fn from(memory: CreateMemory) -> Self {
        Self {
            id: Set(Uuid::new_v4()),
            owner_id: Set(memory.owner_id),
            name: Set(memory.name),
        }
    }
}

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) owner_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateMemory {
    pub(super) name: String,
}

impl From<UpdateMemory> for Memory {
    fn from(memory: UpdateMemory) -> Self {
        Self {
            name: Set(memory.name),
            ..Default::default()
        }
    }
}
