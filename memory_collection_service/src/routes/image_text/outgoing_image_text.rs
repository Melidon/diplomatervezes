use {crate::entities::image_text::Model as Text, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingImageText {
    id: Uuid,
    text: String,
}

impl From<Text> for OutgoingImageText {
    fn from(text: Text) -> Self {
        Self {
            id: text.id,
            text: text.text,
        }
    }
}
