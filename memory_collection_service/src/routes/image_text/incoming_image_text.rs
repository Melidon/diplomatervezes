use {
    crate::entities::image_text::ActiveModel as Text, sea_orm::Set, serde::Deserialize, uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct CreateImageText {
    pub(super) image_id: Uuid,
    pub(super) text: String,
}

impl From<CreateImageText> for Text {
    fn from(text: CreateImageText) -> Self {
        Self {
            id: Set(Uuid::new_v4()),
            image_id: Set(text.image_id),
            text: Set(text.text),
        }
    }
}

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) image_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateImageText {
    pub(super) text: String,
}

impl From<UpdateImageText> for Text {
    fn from(text: UpdateImageText) -> Self {
        Self {
            text: Set(text.text),
            ..Default::default()
        }
    }
}
