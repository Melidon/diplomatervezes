mod incoming_image_text;
mod outgoing_image_text;

use {
    self::{
        incoming_image_text::{CreateImageText, QueryParams, UpdateImageText},
        outgoing_image_text::OutgoingImageText,
    },
    crate::{
        entities::image_text::{
            ActiveModel as ImageText, Column::ImageId, Entity as ImageTextTable,
        },
        utils::{
            access_check::{check_access_to_image, check_access_to_image_text},
            claims::Claims,
            memory_collection_error::MemoryCollectionError,
        },
    },
    axum::{
        extract::{Path, Query},
        http::StatusCode,
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    std::sync::Arc,
    uuid::Uuid,
};

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_image_text))
        .route("/:image_text_id", get(get_image_text))
        .route("/", get(list_image_texts))
        .route("/:image_text_id", put(update_image_text))
        .route("/:image_text_id", delete(delete_image_text))
}

async fn create_image_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Json(image_text): Json<CreateImageText>,
) -> Result<(StatusCode, Json<OutgoingImageText>), MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &image_text.image_id).await?;
    let text = ImageTextTable::insert(ImageText::from(image_text))
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    Ok((StatusCode::CREATED, Json(text)))
}

async fn get_image_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(image_text_id): Path<Uuid>,
) -> Result<Json<OutgoingImageText>, MemoryCollectionError> {
    check_access_to_image_text(db.as_ref(), &claims, &image_text_id).await?;
    let text = ImageTextTable::find_by_id(image_text_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .into();
    Ok(Json(text))
}

async fn list_image_texts(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { image_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingImageText>>, MemoryCollectionError> {
    check_access_to_image(db.as_ref(), &claims, &image_id).await?;
    let entities = ImageTextTable::find()
        .filter(ImageId.eq(image_id))
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingImageText::from)
        .collect();
    Ok(Json(entities))
}

async fn update_image_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(image_text_id): Path<Uuid>,
    Json(image_text): Json<UpdateImageText>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_image_text(db.as_ref(), &claims, &image_text_id).await?;
    let image_text = ImageText {
        id: Set(image_text_id),
        ..image_text.into()
    };
    ImageTextTable::update(image_text)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_image_text(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(image_text_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_image_text(db.as_ref(), &claims, &image_text_id).await?;
    ImageTextTable::delete_by_id(image_text_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
