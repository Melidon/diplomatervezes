mod incoming_recording;
mod outgoing_recording;

use {
    self::{
        incoming_recording::{QueryParams, UpdateRecording},
        outgoing_recording::OutgoingRecording,
    },
    crate::{
        entities::recording::{
            ActiveModel as Recording,
            Column::{Id, MemoryId, Name},
            Entity as RecordingTable,
        },
        utils::{
            access_check::{check_access_to_memory, check_access_to_recording},
            claims::Claims,
            exchanges::MemoryCollectionExchanges,
            memory_collection_error::MemoryCollectionError,
        },
    },
    amqprs::{
        channel::{BasicPublishArguments, Channel},
        BasicProperties,
    },
    axum::{
        extract::{DefaultBodyLimit, Multipart, Path, Query},
        http::{
            header::{HeaderName, CONTENT_TYPE},
            HeaderValue, StatusCode,
        },
        routing::{delete, get, post, put},
        Extension, Json, Router,
    },
    file_format::FileFormat::{
        self, Mpeg12AudioLayer3 as MP3, OggVorbis as OGG, WaveformAudio as WAV,
    },
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, Iden, QueryFilter, QuerySelect, Set},
    std::sync::Arc,
    uuid::Uuid,
};

const SUPPORTED_FORMATS: [FileFormat; 3] = [MP3, OGG, WAV];

pub(super) fn router() -> Router {
    Router::new()
        .route("/", post(create_recording))
        .layer(DefaultBodyLimit::max(8 * 1024 * 1024))
        .route("/:recording_id", get(get_recording))
        .route("/", get(list_recordings))
        .route("/:recording_id", put(update_recording))
        .route("/:recording_id", delete(delete_recording))
}

async fn create_recording(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    Extension(mq): Extension<Channel>,
    claims: Claims,
    Query(QueryParams { memory_id }): Query<QueryParams>,
    mut multipart: Multipart,
) -> Result<(StatusCode, Json<OutgoingRecording>), MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let field = multipart
        .next_field()
        .await
        .map_err(MemoryCollectionError::MultipartError)?
        .ok_or(MemoryCollectionError::NoFile)?;
    let name = field
        .name()
        .ok_or(MemoryCollectionError::MissingName)?
        .to_string();
    let bytes = field
        .bytes()
        .await
        .map_err(MemoryCollectionError::MultipartError)?
        .to_vec();
    let file_format = FileFormat::from_bytes(&bytes);
    SUPPORTED_FORMATS
        .contains(&file_format)
        .then_some(())
        .ok_or(MemoryCollectionError::InvalidFormat {
            supported_formats: &SUPPORTED_FORMATS,
        })?;
    let recording = Recording {
        id: Set(Uuid::new_v4()),
        memory_id: Set(memory_id),
        name: Set(name),
        bytes: Set(bytes),
    };
    let recording: OutgoingRecording = RecordingTable::insert(recording)
        .exec_with_returning(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into();
    mq.basic_publish(
        BasicProperties::default(),
        recording.id.into_bytes().to_vec(),
        BasicPublishArguments::new(&MemoryCollectionExchanges::RecordingCreated.to_string(), ""),
    )
    .await
    .map_err(MemoryCollectionError::MessageQueue)?;
    Ok((StatusCode::CREATED, Json(recording)))
}

async fn get_recording(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(recording_id): Path<Uuid>,
) -> Result<([(HeaderName, HeaderValue); 1], Vec<u8>), MemoryCollectionError> {
    check_access_to_recording(db.as_ref(), &claims, &recording_id).await?;
    let recording = RecordingTable::find_by_id(recording_id)
        .one(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .ok_or(MemoryCollectionError::NotFound)?
        .bytes;
    let content_type = FileFormat::from_bytes(&recording)
        .media_type()
        .try_into()
        .expect("Invalid media type");
    Ok(([(CONTENT_TYPE, content_type)], recording))
}

async fn list_recordings(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Query(QueryParams { memory_id }): Query<QueryParams>,
) -> Result<Json<Vec<OutgoingRecording>>, MemoryCollectionError> {
    check_access_to_memory(db.as_ref(), &claims, &memory_id).await?;
    let recordings = RecordingTable::find()
        .filter(MemoryId.eq(memory_id))
        .select_only()
        .columns([Id, Name])
        .into_tuple::<(Uuid, String)>()
        .all(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?
        .into_iter()
        .map(OutgoingRecording::from)
        .collect();
    Ok(Json(recordings))
}

async fn update_recording(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(recording_id): Path<Uuid>,
    Json(recording): Json<UpdateRecording>,
) -> Result<(), MemoryCollectionError> {
    check_access_to_recording(db.as_ref(), &claims, &recording_id).await?;
    let recording = Recording {
        id: Set(recording_id),
        ..recording.into()
    };
    RecordingTable::update(recording)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(())
}

async fn delete_recording(
    Extension(db): Extension<Arc<DatabaseConnection>>,
    claims: Claims,
    Path(recording_id): Path<Uuid>,
) -> Result<StatusCode, MemoryCollectionError> {
    check_access_to_recording(db.as_ref(), &claims, &recording_id).await?;
    RecordingTable::delete_by_id(recording_id)
        .exec(db.as_ref())
        .await
        .map_err(MemoryCollectionError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
