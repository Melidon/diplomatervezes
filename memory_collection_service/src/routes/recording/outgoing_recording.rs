use {crate::entities::recording::Model as Recording, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(super) struct OutgoingRecording {
    pub(super) id: Uuid,
    name: String,
}

impl From<Recording> for OutgoingRecording {
    fn from(recording: Recording) -> Self {
        Self {
            id: recording.id,
            name: recording.name,
        }
    }
}

impl From<(Uuid, String)> for OutgoingRecording {
    fn from((id, name): (Uuid, String)) -> Self {
        Self { id, name }
    }
}
