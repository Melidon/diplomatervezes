use {
    crate::entities::recording::ActiveModel as Recording, sea_orm::Set, serde::Deserialize,
    uuid::Uuid,
};

#[derive(Deserialize)]
pub(super) struct QueryParams {
    pub(super) memory_id: Uuid,
}

#[derive(Deserialize)]
pub(super) struct UpdateRecording {
    pub(super) name: String,
}

impl From<UpdateRecording> for Recording {
    fn from(recording: UpdateRecording) -> Self {
        Self {
            name: Set(recording.name),
            ..Default::default()
        }
    }
}
