use {
    amqprs::{
        callbacks::{DefaultChannelCallback, DefaultConnectionCallback},
        channel::{
            BasicConsumeArguments, Channel, ExchangeDeclareArguments, ExchangeType,
            QueueBindArguments, QueueDeclareArguments,
        },
        connection::Connection,
    },
    memory_collection_service::{
        consumers::UserDeletedConsumer,
        utils::exchanges::{AuthExchanges, MemoryCollectionExchanges},
    },
    migration::{Migrator, MigratorTrait},
    sea_orm::{Database, DatabaseConnection, Iden, Iterable},
    std::{
        env,
        net::{Ipv4Addr, SocketAddrV4},
        sync::Arc,
    },
    tokio::net::TcpListener,
};

async fn connect_to_database() -> DatabaseConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set!");
    let connection = Database::connect(&database_url)
        .await
        .expect("Failed to connect to database!");
    Migrator::up(&connection, None)
        .await
        .expect("Failed to migrate!");
    connection
}

async fn connect_to_message_queue() -> (Connection, Channel, Channel, String) {
    let rabbitmq_uri = env::var("RABBITMQ_URI").expect("RABBITMQ_URI must be set!");
    let connection_arguments = rabbitmq_uri
        .as_str()
        .try_into()
        .expect("RABBITMQ_URI is invalid!");
    let connection = Connection::open(&connection_arguments)
        .await
        .expect("Failed to connect to RabbitMQ!");
    connection
        .register_callback(DefaultConnectionCallback)
        .await
        .expect("Failed to register connection callback!");

    let publish_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    publish_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");
    for memory_collection_exchange in MemoryCollectionExchanges::iter() {
        publish_channel
            .exchange_declare(ExchangeDeclareArguments::of_type(
                &memory_collection_exchange.to_string(),
                ExchangeType::Fanout,
            ))
            .await
            .expect("Failed to declare exchange!");
    }

    let subscribe_channel = connection
        .open_channel(None)
        .await
        .expect("Failed to open channel!");
    subscribe_channel
        .register_callback(DefaultChannelCallback)
        .await
        .expect("Failed to register channel callback!");
    for auth_exchange in AuthExchanges::iter() {
        subscribe_channel
            .exchange_declare(ExchangeDeclareArguments::of_type(
                &auth_exchange.to_string(),
                ExchangeType::Fanout,
            ))
            .await
            .expect("Failed to declare exchange!");
    }
    let (queue_name, _, _) = subscribe_channel
        .queue_declare(QueueDeclareArguments::exclusive_server_named())
        .await
        .expect("Failed to declare queue!")
        .expect("`no_wait` argument should be `false`");
    subscribe_channel
        .queue_bind(QueueBindArguments::new(
            &queue_name,
            &AuthExchanges::UserDeleted.to_string(),
            "",
        ))
        .await
        .expect("Failed to bind queue!");

    (connection, publish_channel, subscribe_channel, queue_name)
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    dotenvy::dotenv().ok();
    let db = Arc::new(connect_to_database().await);
    // DO NOT RENAME `connection` TO `_connection`!!!
    // It will cause the connection to be dropped.
    // Thus it will be closed and channels won't work.
    #[allow(unused_variables)]
    let (connection, publish_channel, subscribe_channel, user_deleted_queue_name) =
        connect_to_message_queue().await;
    subscribe_channel
        .basic_consume(
            UserDeletedConsumer::new(db.clone()),
            BasicConsumeArguments::new(&user_deleted_queue_name, "")
                .auto_ack(true)
                .finish(),
        )
        .await
        .expect("Failed to start consuming!");
    let port = env::var("PORT").map_or(3000, |port| port.parse().expect("PORT is invalid!"));
    let listener = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port))
        .await
        .expect("Could not bind TCP listener!");
    let app = memory_collection_service::get_app(db, publish_channel);
    axum::serve(listener, app).await.expect("Server failed!");
}
