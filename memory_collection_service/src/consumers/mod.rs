use {
    crate::entities::{entity, memory, prelude::*},
    amqprs::{channel::Channel, consumer::AsyncConsumer, BasicProperties, Deliver},
    axum::async_trait,
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter},
    std::sync::Arc,
    uuid::Uuid,
};

pub struct UserDeletedConsumer {
    db: Arc<DatabaseConnection>,
}

impl UserDeletedConsumer {
    pub fn new(db: Arc<DatabaseConnection>) -> Self {
        Self { db }
    }
}

#[async_trait]
impl AsyncConsumer for UserDeletedConsumer {
    async fn consume(
        &mut self,
        _channel: &Channel,
        _deliver: Deliver,
        _basic_properties: BasicProperties,
        content: Vec<u8>,
    ) {
        match Uuid::try_from(content) {
            Ok(id) => delete_all_for_user(self.db.as_ref(), id).await,
            Err(_) => eprintln!("Failed to parse UUID from message!"),
        }
    }
}

async fn delete_all_for_user(db: &DatabaseConnection, id: Uuid) {
    Memory::delete_many()
        .filter(memory::Column::OwnerId.eq(id))
        .exec(db)
        .await
        .unwrap();
    Entity::delete_many()
        .filter(entity::Column::OwnerId.eq(id))
        .exec(db)
        .await
        .unwrap();
}
