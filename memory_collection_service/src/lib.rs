pub mod consumers;
mod entities;
mod routes;
pub mod utils;

use {
    amqprs::channel::Channel,
    axum::{Extension, Router},
    sea_orm::DatabaseConnection,
    std::sync::Arc,
    tower_http::cors::CorsLayer,
};

pub fn get_app(db: Arc<DatabaseConnection>, mq: Channel) -> Router {
    Router::new().nest(
        "/api/memory_collection",
        routes::router()
            .layer(Extension(db))
            .layer(Extension(mq))
            .layer(CorsLayer::very_permissive()), // TODO: Allow less if possible
    )
}
