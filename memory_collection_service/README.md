Steps to recreate the migration:
- `cargo install sea-orm-cli`
- `sea-orm-cli migrate init`
- Edit the **migrations/Cargo.toml**
- write the migration in **migration/src/m20220101_000001_create_table.rs**
- Create a **.env** file and add the database URL
- `sea-orm-cli migrate up`
- `sea-orm-cli generate entity --output-dir src/entities`

Useful links:
- [Setting Up Migration](https://www.sea-ql.org/SeaORM/docs/migration/setting-up-migration/)
- [Writing Migration](https://www.sea-ql.org/SeaORM/docs/migration/writing-migration/)
- [Running Migration](https://www.sea-ql.org/SeaORM/docs/migration/running-migration/)
- [Generating Entities](https://www.sea-ql.org/SeaORM/docs/generate-entity/sea-orm-cli/)